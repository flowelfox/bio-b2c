"""added image to basket translation

Revision ID: 2d2a47b4d00f
Revises: 424d5d36998f
Create Date: 2019-11-06 18:41:54.675686

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2d2a47b4d00f'
down_revision = '424d5d36998f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('baskets_translations', sa.Column('image', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('baskets_translations', 'image')
    # ### end Alembic commands ###
