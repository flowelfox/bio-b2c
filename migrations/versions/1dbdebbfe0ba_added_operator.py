"""added operator

Revision ID: 1dbdebbfe0ba
Revises: ae24b45dcad6
Create Date: 2019-11-06 20:07:51.620091

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1dbdebbfe0ba'
down_revision = 'ae24b45dcad6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('operators',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('join_date', sa.DateTime(), nullable=True),
    sa.Column('delivery_center_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['delivery_center_id'], ['delivery_centers.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('operators')
    # ### end Alembic commands ###
