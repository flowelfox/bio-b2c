from src.models import DBSession, Permission, User, DeliveryCenter, DeliveryCenterTranslation, Category, CategoryTranslation, Product, ProductTranslation, Basket, BasketTranslation, Manufacturer, ManufacturerTranslation

# noinspection SpellCheckingInspection
from src.settings import SUPERUSER_ACCOUNTS


def main():
    """"""
    _ = lambda text: text

    """-----------------User-----------------"""
    Permission.create('start_menu_access', _('Access to "Start" menu'))
    Permission.create('profile_menu_access', _('Access to "Profile" menu'))
    Permission.create('change_language_menu_access', _('Access to "Change language" menu'))
    Permission.create('product_menu_access', _('Access to "Products" menu'))
    Permission.create('scan_qr_menu_access', _('Access to "Scan QR" menu'))
    Permission.create('help_menu_access', _('Access to "Help" menu'))

    Permission.create('user_baskets_menu_access', _('Access to "Baskets" user menu'))
    Permission.create('user_take_basket_menu_access', _('Access to "Take basket" user menu'))
    Permission.create('user_my_baskets_menu_access', _('Access to "My baskets" user menu'))
    Permission.create('user_buy_basket_menu_access', _('Access to "Buy basket" user menu'))
    Permission.create('basket_order_menu_access', _('Access to "Basket products" user menu'))

    Permission.create('allow_buy_basket', _('Allow buy basket'))
    Permission.create('allow_user_take_basket', _('Allow take basket for user'))

    """-----------------Operator-----------------"""
    Permission.create('operator_menu_access', _('Access to "Operator" menu'))
    Permission.create('baskets_to_fill_menu_access', _('Access to "Baskets to fill" menu'))
    Permission.create('operator_baskets_menu_access', _('Access to "Baskets" operator menu'))
    Permission.create('for_carrier_baskets_menu_access', _('Access to "For carrier baskets" operator menu'))
    Permission.create("add_courier_menu_access", _('Access to "Add courier" operator menu'))

    Permission.create('allow_operator_take_basket', _('Allow take basket for operator'))

    """-----------------Courier-----------------"""
    Permission.create('courier_menu_access', _('Access to "Courier" menu'))
    Permission.create('courier_baskets_to_delivery_menu_access', _('Access to "Baskets to delivery" courier menu'))
    Permission.create('allow_courier_take_basket', _('Allow take basket for courier'))

    """-----------------Admin-----------------"""
    # menus
    Permission.create('admin_menu_access', _('Access to "Admin" menu'))
    Permission.create('distribution_menu_access', _('Access to "Distribution" menu'))
    Permission.create('users_info_menu_access', _('Access to "Users info" menu'))
    Permission.create('info_menu_access', _('Access to "Info" menu'))
    Permission.create('admin_formed_baskets_menu_access', _('Access to "Formed baskets" admin menu'))
    Permission.create('ordered_products_menu_access', _('Access to "Ordered products" admin menu'))

    # delivery centers menu
    Permission.create(DeliveryCenter.view_permission, _('Access to "Delivery centers" menu'))
    Permission.create(DeliveryCenter.add_permission, _('Access to "Add delivery center" menu'))
    Permission.create(DeliveryCenterTranslation.view_permission, _('Access to "Delivery center translations" menu'))
    Permission.create(DeliveryCenterTranslation.add_permission, _('Access to "Add delivery center translations" menu'))
    Permission.create(DeliveryCenterTranslation.edit_permission, _('Access to "Edit delivery center translations" menu'))
    Permission.create(DeliveryCenter.delete_permission, _('Allow delete delivery center'))
    Permission.create(DeliveryCenterTranslation.delete_permission, _('Allow delete delivery center translations'))

    # baskets menu
    Permission.create(Basket.view_permission, _('Access to "Basket" menu'))
    Permission.create(Basket.add_permission, _('Access to "Add basket" menu'))
    Permission.create(Basket.edit_permission, _('Access to "Edit basket" menu'))
    Permission.create(BasketTranslation.view_permission, _('Access to "Basket translations" menu'))
    Permission.create(BasketTranslation.add_permission, _('Access to "Add basket translation" menu'))
    Permission.create(BasketTranslation.edit_permission, _('Access to "Edit basket translation" menu'))
    Permission.create(Basket.delete_permission, _('Allow delete basket'))
    Permission.create(BasketTranslation.delete_permission, _('Allow delete basket translations'))
    Permission.create("allow_edit_basket_products", _('Allow edit basket products'))

    # product categories menu
    Permission.create(Category.view_permission, _('Access to "Categories" menu'))
    Permission.create(Category.add_permission, _('Access to "Add category" menu'))
    Permission.create(CategoryTranslation.view_permission, _('Access to "Category translations" menu'))
    Permission.create(CategoryTranslation.add_permission, _('Access to "Add category translation" menu'))
    Permission.create(CategoryTranslation.edit_permission, _('Access to "Edit category translation" menu'))
    Permission.create(Category.delete_permission, _('Allow delete categories'))
    Permission.create(CategoryTranslation.delete_permission, _('Allow delete category translations'))
    Permission.create(Category.import_permission, _('Access to "Import categories" menu'))
    Permission.create("allow_export_qr_codes", _('Allow export QR codes'))

    # products menu
    Permission.create(Product.view_permission, _('Access to "Products" menu'))
    Permission.create(Product.add_permission, _('Access to "Add product" menu'))
    Permission.create(Product.edit_permission, _('Access to "Edit product" menu'))
    Permission.create(ProductTranslation.view_permission, _('Access to "Product translations" menu'))
    Permission.create(ProductTranslation.add_permission, _('Access to "Add product translation" menu'))
    Permission.create(ProductTranslation.edit_permission, _('Access to "Edit product translation" menu'))
    Permission.create(Product.delete_permission, _('Allow delete products'))
    Permission.create(ProductTranslation.delete_permission, _('Allow delete product translations'))
    Permission.create(Product.import_permission, _('Access to "Import products" menu'))

    # manufacturers menu
    Permission.create(Manufacturer.view_permission, _('Access to "Manufacturer" menu'))
    Permission.create(Manufacturer.add_permission, _('Access to "Add manufacturer" menu'))
    Permission.create(ManufacturerTranslation.view_permission, _('Access to "Manufacturer translations" menu'))
    Permission.create(ManufacturerTranslation.add_permission, _('Access to "Add manufacturer translation" menu'))
    Permission.create(ManufacturerTranslation.edit_permission, _('Access to "Edit manufacturer translation" menu'))
    Permission.create(Manufacturer.delete_permission, _('Allow delete manufacturer'))
    Permission.create(ManufacturerTranslation.delete_permission, _('Allow delete manufacturer translations'))
    Permission.create(Manufacturer.import_permission, _('Access to "Import manufacturers" menu'))

    Permission.create("formed_baskets_info_menu_access", _('Access to "Formed baskets info" menu'))
    Permission.create("ordered_products_info_menu_access", _('Access to "Ordered products info" menu'))
    Permission.create("change_help_menu_permission", _('Access to "Edit help" menu'))
    Permission.create("add_help_entries_menu_permission", _('Access to "Add help entry" menu'))
    Permission.create("edit_help_entries_menu_permission", _('Access to "Edit help entry" menu'))
    Permission.create("delete_help_entries_menu_permission", _('Access to "Delete help entry" menu'))

    # permissions menu
    Permission.create(Permission.view_permission, _('Access to "Permissions" menu'))
    Permission.create(Permission.add_permission, _('Allow add permission'))
    Permission.create(Permission.delete_permission, _('Allow remove permission'))

    Permission.create('superuser', _('Superuser'))

    DBSession.commit()

    users = DBSession.query(User).filter(User.chat_id.in_(SUPERUSER_ACCOUNTS)).all()
    permission = DBSession.query(Permission).get('superuser')
    for user in users:
        if not user.has_permission(permission.code):
            user.permissions.append(permission)
        DBSession.add(user)
    DBSession.commit()


if __name__ == '__main__':
    main()
