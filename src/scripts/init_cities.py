from src.models import DBSession, City, CityTranslation

cities = []


def create_city(name_en, name_ru, name_uk):
    existed_city = DBSession.query(CityTranslation).filter(CityTranslation.name == name_en).first()
    if existed_city is None:
        city = City()
        city.translations.append(CityTranslation(lang='en', city=city, name=name_en))
        city.translations.append(CityTranslation(lang='ru', city=city, name=name_ru))
        city.translations.append(CityTranslation(lang='uk', city=city, name=name_uk))
        cities.append(city)


def main():
    create_city("Kharkiv", "Харьков", "Харків")
    create_city("💬 Another City", "💬 Другой город", "💬 Інше місто")

    if cities:
        for city in cities:
            DBSession.add(city)
        DBSession.commit()


if __name__ == '__main__':
    main()
