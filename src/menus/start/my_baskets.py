import datetime
import enum

import pytz
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from src.menus.start.buy_basket import BuyBasketMenu
from src.models import DBSession, BasketOrder, ProductBasketOrder
from src.settings import WEBHOOK_ENABLE


class MyBasketsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2
        DELIVERY_DATES = 3

    menu_name = "user_my_baskets_menu"
    disable_web_page_preview = False

    @require_permission("user_my_baskets_menu_access")
    def entry(self, update, context):
        return super(MyBasketsMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        basket_orders = DBSession.query(BasketOrder) \
            .filter(BasketOrder.user_id == user.id) \
            .filter(BasketOrder.payed == True) \
            .all()

        active_basket_orders = [basket_order for basket_order in basket_orders if basket_order.active()]
        return active_basket_orders

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^my_baskets$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, objs):
        _ = context.user_data['user'].translator
        buttons = []
        if objs:
            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data='basket_more')])
            buttons.append([InlineKeyboardButton(_("Products"), callback_data='basket_products')])
            buttons.append([InlineKeyboardButton(_("Delivery dates"), callback_data='basket_delivery_dates')])
        else:
            buttons.append([InlineKeyboardButton(_("🛎️ Order a cart"), callback_data='buy_basket')])
        return buttons

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_basket')]]
        translation = self.selected_object(context).basket.get_translation(context.user_data['user'].language_code)

        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += '\n'
        message_text += "<i>" + (translation.description if translation.description else _("Unknown")) + "</i>"
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("🛒 My carts") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        if obj:
            translation = obj.basket.get_translation(user.language_code)
            if translation.image is not None:
                message_text += f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += _("Basket") + f": <b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += _("Composition") + "\n"
            for idx, product in enumerate(obj.products):
                product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((product.id, obj.id))
                message_text += f"{idx + 1}. {product.get_translation(user.language_code).name} x {product_basket_order_asoc.quantity}\n"

        else:
            message_text = _("You have no orders yet.") + '\n\n'
            message_text += _("Let start with 🛎️ <b>Order a cart</b>, where you can choose it for your taste.")
        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def delivery_dates(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        basket_order = self.selected_object(context)

        message_text = _("Delivery dates:") + "\n"
        for idx, date in enumerate(basket_order.delivery_dates()):
            adapted_date = pytz.utc.localize(date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
            if date > datetime.datetime.utcnow():
                message_text += f"{idx + 1}. {adapted_date.strftime('%d.%m.%Y')}\n"
            else:
                message_text += f"{idx + 1}. {adapted_date.strftime('%d.%m.%Y')} ✅\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_basket')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)

        return self.States.DELIVERY_DATES

    def back_to_basket(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        basket_order_products_menu = BasketOrderProductsMenu(self)
        buy_basket_menu = BuyBasketMenu(self)
        return {self.States.ACTION: [CallbackQueryHandler(self.more, pattern='^basket_more$'),
                                     CallbackQueryHandler(self.delivery_dates, pattern='^basket_delivery_dates$'),
                                     basket_order_products_menu.handler,
                                     buy_basket_menu.handler],
                self.States.MORE: [CallbackQueryHandler(self.back_to_basket, pattern='^back_to_basket$')],
                self.States.DELIVERY_DATES: [CallbackQueryHandler(self.back_to_basket, pattern='^back_to_basket$')]}


class BasketOrderProductsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2

    menu_name = "basket_order_products_menu"
    disable_web_page_preview = False

    @require_permission("basket_order_menu_access")
    def entry(self, update, context):
        return super(BasketOrderProductsMenu, self).entry(update, context)

    def query_objects(self, context):
        basket_order = self.parent.selected_object(context)
        return basket_order.products

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^basket_products$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        basket_order = self.parent.selected_object(context)

        message_text = ""
        if obj:
            translation = obj.get_translation(user.language_code)
            if translation.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += _("Basket name:") + " " + basket_order.basket.get_translation(user.language_code).name + "\n"
            message_text += _("Product name") + f": <b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("Manufacturer") + f": <b>{(obj.manufacturer.get_translation(user.language_code).name if obj.manufacturer else _('Unknown'))}</b>\n"
            message_text += "\n"
            message_text += (translation.description if translation.description else _("Unknown")) + "\n"
        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰"
            message_text += "\n"
            message_text += "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text
