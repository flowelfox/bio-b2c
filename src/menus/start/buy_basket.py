import datetime
import enum
import re
from io import BytesIO
from os import path

from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus import OneListMenu, BaseMenu
from botmanlib.menus.helpers import require_permission, make_payment, check_payment, add_to_db
from botmanlib.messages import send_or_edit, delete_user_message
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, CallbackContext, ConversationHandler, MessageHandler, Filters

from src.helpers import strtokeycap
from src.models import DBSession, Basket, DeliveryCenter, Product, ProductBasket, BasketOrder, ProductBasketOrder, User, BasketTranslation, database, Operator
from src.settings import WEBHOOK_ENABLE, FONDY_TOKEN, RESOURCES_FOLDER


class BuyBasketMenu(OneListMenu):
    menu_name = 'buy_basket_menu'
    disable_web_page_preview = False

    @require_permission("user_buy_basket_menu_access")
    def entry(self, update, context):
        return super(BuyBasketMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        return DBSession.query(Basket) \
            .join(Basket.delivery_center) \
            .filter(DeliveryCenter.city_id == user.city_id) \
            .all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^buy_basket$', pass_user_data=True)]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if obj:
            translation = obj.get_translation(user.language_code)
            if translation.image is not None:
                message_text += f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + translation.name + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += translation.short_description + "\n\n"
            message_text += _("Cost:") + f" {obj.price:.2f} " + _("UAH") + "\n\n"
        else:
            message_text = _("In your city today there is no possibility to order organic carts.\nAs soon as this opportunity arises, we will notify you.") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("🛎️ Order") + " (" + _("Cart") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")\n"
            message_text += "<i>" + _("Choose the cart with products you want to receive every week.") + "</i>"
        else:
            message_text = ""

        return message_text

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        buttons = []
        if o:
            buttons.append(InlineKeyboardButton(_("✅ Buy"), callback_data="buy_selected_basket"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if obj:
            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data='full_description')])
        return buttons

    def back_to_basket(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def more(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        translation = self.selected_object(context).get_translation(user.language_code)
        if translation is None:
            translation = BasketTranslation(name=_("Unknown"), description=_("Unknown"), short_description=_("Unknown"))

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += translation.short_description + "\n\n"
        message_text += translation.description + "\n\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_basket')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.ACTION

    def additional_states(self):
        buy_basket_menu = BuyBasketProducts(self)

        return {self.States.ACTION: [buy_basket_menu.handler,
                                     CallbackQueryHandler(self.more, pattern="^full_description$"),
                                     CallbackQueryHandler(self.back_to_basket, pattern="^back_to_basket$")]
                }


class BuyBasketProducts(OneListMenu):
    menu_name = "buy_basket_products"
    disable_web_page_preview = False

    @require_permission("allow_buy_basket")
    def entry(self, update, context: CallbackContext):
        self._load(context)
        user = context.user_data['user']

        basket = self.parent.selected_object(context)
        basket_order = BasketOrder(basket_id=basket.id, user_id=user.id)
        DBSession.add(basket_order)
        for product in basket.products:
            product_basket_order_asoc = ProductBasketOrder(product_id=product.id, basket_order=basket_order, quantity=1)
            DBSession.add(product_basket_order_asoc)

        DBSession.commit()
        context.user_data[self.menu_name]['basket_order'] = basket_order

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def query_objects(self, context: CallbackContext):
        basket = self.parent.selected_object(context)
        return DBSession.query(Product) \
            .join(ProductBasket) \
            .filter(ProductBasket.basket_id == basket.id) \
            .all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^buy_selected_basket$")]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        message_text = ""

        if obj:
            translation = obj.get_translation(user.language_code)

            if translation.image is not None:
                message_text += f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + _("List of products") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += strtokeycap(str(context.user_data[self.menu_name]['selected_object'] + 1)) + " " + translation.name + "\n"
            message_text += _("Packing:") + f" {obj.packaging} " + translation.packaging_unit + "\n"

        else:
            message_text = _("This basket does not contain any products") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator
        basket = self.parent.selected_object(context)
        obj = self.selected_object(context)
        product_basket_asoc = DBSession.query(ProductBasket).get((obj.id, basket.id))

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "🛒" + basket.get_translation(user.language_code).name + " (" + _("Product") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        if product_basket_asoc.editable:
            message_text += "\n\n" + _("By the way! You can also increase the amount of this product in the cart (➕)") + "\n"

        return message_text

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def add_quantity(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        product = self.selected_object(context)
        basket_order = context.user_data[self.menu_name]['basket_order']
        product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((product.id, basket_order.id))

        if product_basket_order_asoc.quantity < 10:
            product_basket_order_asoc.quantity += 1
            DBSession.add(product_basket_order_asoc)
        DBSession.commit()

        self.send_message(context)
        update.callback_query.answer()

        return self.States.ACTION

    def remove_quantity(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        product = self.selected_object(context)
        basket_order = context.user_data[self.menu_name]['basket_order']
        product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((product.id, basket_order.id))

        if product_basket_order_asoc.quantity > 1:
            product_basket_order_asoc.quantity -= 1
            DBSession.add(product_basket_order_asoc)
        DBSession.commit()

        self.send_message(context)
        update.callback_query.answer()
        return self.States.ACTION

    def back(self, update, context):
        if not context.user_data[self.menu_name]['basket_order'].payed:
            DBSession.delete(context.user_data[self.menu_name]['basket_order'])
        DBSession.commit()
        return super(BuyBasketProducts, self).back(update, context)

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        if obj:
            basket_order = context.user_data[self.menu_name]['basket_order']
            product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((obj.id, basket_order.id))

            basket = self.parent.selected_object(context)
            product_basket_asoc = DBSession.query(ProductBasket).get((obj.id, basket.id))

            translation = obj.get_translation(user.language_code)

            if product_basket_asoc.editable:
                buttons.append([InlineKeyboardButton("➖", callback_data="remove_quantity"),
                                InlineKeyboardButton(str(product_basket_order_asoc.quantity) + " " + _("pack."), callback_data="nothing"),
                                InlineKeyboardButton("➕", callback_data="add_quantity")])

            buttons.append([InlineKeyboardButton(_("📋 Checkout"), callback_data="confirm_order")])
        return buttons

    def nothing(self, update, context):
        update.callback_query.answer()
        return self.States.ACTION

    def additional_states(self):
        basket_order_menu = BasketOrderMenu(self)
        return {self.States.ACTION: [CallbackQueryHandler(self.add_quantity, pattern="^add_quantity$"),
                                     CallbackQueryHandler(self.remove_quantity, pattern="^remove_quantity$"),
                                     CallbackQueryHandler(self.nothing, pattern="^nothing$"),
                                     basket_order_menu.handler]}


class BasketOrderMenu(BaseMenu):
    menu_name = "basket_order_menu"

    class States(enum.Enum):
        ACTION = 1
        WAIT_PAY = 2

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.ask_deliveries_count(update, context)
        return self.States.ACTION

    def send_message(self, context):
        pass

    def ask_deliveries_count(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        basket_order = context.user_data[self.parent.menu_name]['basket_order']

        message_text = "<b>" + _("📋 Checkout") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("We will collect carts for you with organic products for every week.") + "\n"
        message_text += _("<b>Please select the duration for your order:</b>")
        buttons = [[InlineKeyboardButton(_("🗓️ 1 order ({price:.0f} UAH)").format(price=basket_order.get_price()), callback_data=f"delivers_1")],
                   [InlineKeyboardButton(_("🗓️ 4 weeks ({price:.0f} UAH)").format(price=basket_order.get_price(4)), callback_data=f"delivers_4")],
                   [InlineKeyboardButton(_("🗓️ 12 weeks ({price:.0f} UAH)").format(price=basket_order.get_price(12)), callback_data=f"delivers_12")],
                   [InlineKeyboardButton(_("🗓️ 25 weeks ({price:.0f} UAH)").format(price=basket_order.get_price(25)), callback_data=f"delivers_25")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def set_deliveries_count(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        basket_order = context.user_data[self.parent.menu_name]['basket_order']
        basket_order.deliveries = int(update.callback_query.data.replace("delivers_", ""))

        update.callback_query.answer()
        self.ask_baskets_count(update, context)
        return self.States.ACTION

    def ask_baskets_count(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        basket_order = context.user_data[self.parent.menu_name]['basket_order']
        message_text = "<b>" + _("📋 Checkout") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Please check your order:") + "\n\n"

        message_text += "<b>" + _("Cart name:") + " " + basket_order.basket.get_translation(user.language_code).name + "</b>\n"
        message_text += _("Product List:") + "\n"
        for idx, product in enumerate(basket_order.products):
            product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((product.id, basket_order.id))
            message_text += f"{strtokeycap(str(idx + 1))} {product.get_translation(user.language_code).name}, {product_basket_order_asoc.quantity} {product.get_translation(user.language_code).packaging_unit}\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("🗓️ Duration:") + " " + str(basket_order.deliveries) + " " + _("weeks") + "\n"
        message_text += _("🛒 Quantity:") + " " + str(basket_order.number_of_baskets) + " " + _("cart.") + "\n"
        message_text += _("🏷️ Cost:") + " " + f"{basket_order.get_price(basket_order.deliveries):.2f} " + _("UAH") + "\n\n"
        message_text += _("By the way! You can also order an additional same cart for yourself or friends and relatives (➕)")

        buttons = [[InlineKeyboardButton("➖", callback_data="remove_number_of_baskets"),
                    InlineKeyboardButton(f"🛒 х {basket_order.number_of_baskets}", callback_data="nothing"),
                    InlineKeyboardButton("➕", callback_data="add_number_of_baskets")],
                   [InlineKeyboardButton(_("📝 Confirm"), callback_data="create_order")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_deliveries_count")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def add_basket_count(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        basket_order = context.user_data[self.parent.menu_name]['basket_order']
        if basket_order.number_of_baskets < 10:
            basket_order.number_of_baskets += 1
            DBSession.add(basket_order)
        DBSession.commit()

        self.ask_baskets_count(update, context)
        update.callback_query.answer()

        return self.States.ACTION

    def remove_basket_count(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        basket_order = context.user_data[self.parent.menu_name]['basket_order']
        if basket_order.number_of_baskets > 1:
            basket_order.number_of_baskets -= 1
            DBSession.add(basket_order)
        DBSession.commit()

        self.ask_baskets_count(update, context)
        update.callback_query.answer()
        return self.States.ACTION

    def create_order(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        basket_order = context.user_data[self.parent.menu_name]['basket_order']

        description = _("Pay for basket {name}.").format(name=basket_order.basket.get_translation(user.language_code).name)

        price = basket_order.get_price(basket_order.deliveries)

        response = make_payment(FONDY_TOKEN, int(price * 100), description, response_url=self.bot.link)
        if not response:
            return self.conv_fallback(context)

        for job in context.job_queue.get_jobs_by_name(f"check_pay_{user.chat_id}"):
            job.schedule_removal()

        bot_context = {'order_id': response['order_id'],
                       'user_context': context}

        context.job_queue.run_repeating(self.wait_pay, 5, name=f"checking_pay_{user.chat_id}", context=bot_context)

        message_text = "<b>" + _("📝 Confirm") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You have formed your personal organic cart ✌️") + "\n"
        message_text += _("Remains only to 💳 <b>Pay</b>") + "\n\n"
        message_text += _("After the transaction, funds will be credited automatically within 5-10 seconds") + "\n"

        buttons = [[InlineKeyboardButton(_("💳 Pay ({price:.2f} UAH)").format(price=price), url=response['checkout_url'])],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_number_of_baskets')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.WAIT_PAY

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def wait_pay(self, context):
        order_id = context.job.context['order_id']
        user_context = context.job.context['user_context']
        session = database.sessionmaker()

        try:
            user = session.query(User).get(user_context.user_data['user'].id)
            _ = user_context.user_data['_']

            data = check_payment(FONDY_TOKEN, order_id)
            if not data or data == 'error':
                context.job.schedule_removal()
                context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))
            elif data == "pending":
                pass
            elif data == "success":
                context.job.schedule_removal()

                basket_order = session.query(BasketOrder).get(user_context.user_data[self.parent.menu_name]['basket_order'].id)
                basket_order.payed = True
                basket_order.payment_id = order_id
                if not add_to_db(basket_order, session):
                    context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))
                basket_order.generate_formed_baskets(session)

                message_text = "<b>" + _("🛎️ Order a cart") + "</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("Congratulations! Your organic cart is already looking forward to meeting you 😀") + "\n\n"
                message_text += "<b>" + _("🔔 We will notify you when it is ready at the Extradition Center.") + "</b>"

                buttons = [[InlineKeyboardButton(_("🧾 Receipt"), callback_data='get_receipt')],
                           [InlineKeyboardButton(_("⏪ Main menu"), callback_data='back_to_main_menu')]]

                send_or_edit(user_context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

                # send message to operator
                operators = session.query(User).join(User.operator).filter(Operator.delivery_center_id == basket_order.basket.delivery_center_id).all()
                for operator in operators:
                    _ = operator.translator
                    operator_message_text = _("User created order for basket {basket_name}").format(basket_name=basket_order.basket.get_translation(operator.language_code).name)
                    operator_buttons = [[InlineKeyboardButton(_("Close message"), callback_data='close_message')]]
                    context.bot.send_message(chat_id=operator.chat_id, text=operator_message_text, reply_markup=InlineKeyboardMarkup(operator_buttons), parse_mode="HTML")


        finally:
            session.close()

    def get_receipt(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("🛎️ Order a cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Congratulations! Your organic cart is already looking forward to meeting you 😀") + "\n\n"
        message_text += "<b>" + _("🔔 We will notify you when it is ready at the Extradition Center.") + "</b>"

        buttons = [[InlineKeyboardButton(_("⏪ Main menu"), callback_data='back_to_main_menu')]]

        result_pdf = self.create_pdf_receipt(context)
        with BytesIO() as receipt:
            result_pdf.write(receipt)
            receipt.seek(0)

            filename = _("Receipt") + '.pdf'
            send_or_edit(context, chat_id=user.chat_id, document=receipt, filename=filename, caption=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.WAIT_PAY

    def create_pdf_receipt(self, context):
        user = context.user_data['user']
        basket_order = DBSession.query(BasketOrder).get(context.user_data[self.parent.menu_name]['basket_order'].id)

        overlay = BytesIO()
        can = canvas.Canvas(overlay, pagesize=A4, bottomup=0)

        # order id and date
        can.setFont("Arial-Bold", 14)
        date = re.sub(':.*?:', "", f"ОЦ1-{basket_order.id:07} від {datetime.date.today().strftime('%d.%m.%Y')} р.")
        can.drawString(155, 107, date)

        # buyer
        can.setFont("Arial", 9)
        buyer_name = re.sub(':.*?:', "", basket_order.user.desired_name if basket_order.user.desired_name else basket_order.user.get_name())
        can.drawString(107, 192, buyer_name)

        # phone
        can.setFont("Arial", 9)
        phone = re.sub(':.*?:', "", basket_order.user.phone)
        can.drawString(125, 203, phone)

        # baskets
        can.setFont("Arial", 8)
        total_price = f"{basket_order.get_price(basket_order.deliveries):.2f} ".replace(".", ",")
        basket_y = 275
        can.drawString(37, basket_y, "1")
        # can.drawString(52, basket_y, f"{basket_order.basket.id}")
        name = basket_order.basket.get_translation('uk').name + f" ({basket_order.deliveries} тиждня)"
        if len(name) > 69:
            name = name[:69] + "..."
        can.drawString(73, basket_y, re.sub(':.*?:', "", name))
        can.drawString(361, basket_y, f"{basket_order.number_of_baskets} шт")
        can.drawString(433, basket_y, f"{basket_order.get_price(basket_order.deliveries) / basket_order.number_of_baskets:.2f}".replace(".", ","))
        can.drawString(505, basket_y, total_price)
        basket_y += 13
        can.drawString(73, basket_y, f"Склад одного набору:")
        basket_y += 15

        for idx, product in enumerate(basket_order.products):
            translation = product.get_translation('uk')
            product_basket_order_asoc = DBSession.query(ProductBasketOrder).get((product.id, basket_order.id))
            can.drawString(37, basket_y, f"{idx + 1}")
            can.setFont("Arial-Italic", 8)
            can.drawString(52, basket_y, f"{product.id}")
            can.setFont("Arial", 8)
            can.drawString(73, basket_y, translation.name)
            can.drawString(361, basket_y, f"{product.packaging * product_basket_order_asoc.quantity} {translation.packaging_unit}")
            can.drawString(433, basket_y, "-")
            can.drawString(505, basket_y, "-")
            basket_y += 13
        basket_y += 2

        can.setFont("Arial-Bold", 8)
        can.drawString(433, basket_y, "Всього:")
        can.drawString(505, basket_y, total_price)
        basket_y += 13

        can.drawString(433, basket_y, "у т.ч. ПДВ:")
        can.drawString(505, basket_y, "0,00")
        basket_y += 15

        uah, coins = total_price.split(",")
        can.drawString(37, basket_y, "Всього найменувань {0}, на суму {1} грн {2} коп.".format("1", uah, coins.strip()))
        basket_y += 13
        can.drawString(37, basket_y, "без ПДВ")

        can.save()
        overlay.seek(0)

        overlay_pdf = PdfFileReader(overlay, strict=False)
        # read template PDF
        template_pdf = PdfFileReader(open(path.join(RESOURCES_FOLDER, 'receipt.pdf'), 'rb'), strict=False)

        result_pdf = PdfFileWriter()
        template_page = template_pdf.getPage(0)
        template_page.mergePage(overlay_pdf.getPage(0))
        result_pdf.addPage(template_page)

        return result_pdf

    def back(self, update, context):
        update.callback_query.answer()
        self.parent.send_message(context)
        return ConversationHandler.END

    def to_main_menu(self, update, context):
        user = context.user_data['user']
        self.fake_callback_update(user, "start", callback_query_id=update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^confirm_order$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                               CallbackQueryHandler(self.set_deliveries_count, pattern=r"^delivers_\d+$"),
                                                               CallbackQueryHandler(self.ask_deliveries_count, pattern=r"^back_to_deliveries_count$"),
                                                               CallbackQueryHandler(self.remove_basket_count, pattern=r"^remove_number_of_baskets$"),
                                                               CallbackQueryHandler(self.add_basket_count, pattern=r"^add_number_of_baskets$"),
                                                               CallbackQueryHandler(self.create_order, pattern=r"^create_order$"),
                                                               ],
                                          self.States.WAIT_PAY: [CallbackQueryHandler(self.ask_baskets_count, pattern="^back_to_number_of_baskets$"),
                                                                 CallbackQueryHandler(self.get_receipt, pattern="^get_receipt$"),
                                                                 CallbackQueryHandler(self.to_main_menu, pattern="^back_to_main_menu$")],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
