import datetime
import enum
import re
from os import path
from textwrap import wrap

from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, prepare_user, require_permission
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from six import BytesIO
from src.settings import RESOURCES_FOLDER
from telegram import TelegramError, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler, PrefixHandler

from src.menus.start.baskets import BasketsMenu
from src.menus.start.help import HelpMenu
from src.menus.start.products import CategoriesMenu
from src.menus.start.profile import ProfileMenu
from src.menus.start.scan_qr import ScanQRMenu
from src.models import User, DBSession, BasketOrder, ProductBasketOrder


class StartMenu(BaseMenu):
    menu_name = 'start_menu'

    class States(enum.Enum):
        ACTION = 1
        LANGUAGE = 2

    def entry(self, update, context):
        if DBSession.query(User).filter(User.chat_id == update.effective_user.id).first() is None:
            user_is_new = True
        else:
            user_is_new = False

        user = prepare_user(User, update, context)

        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.effective_message and update.effective_message.text and update.effective_message.text.startswith("/"):
            delete_interface(context)

        if update.callback_query and update.callback_query.data == 'start':
            update.callback_query.answer()
            try:
                update.effective_message.edit_reply_markup()
            except (TelegramError, AttributeError):
                pass

        if not user.has_permission('start_menu_access'):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted from using this bot"))
            return self.States.ACTION

        self.clear_all_states(context, user.chat_id)

        if user_is_new:
            return self.start_ask_language(update, context)

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("🏠 Main menu") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("Select section:")

        buttons = [[InlineKeyboardButton(_("🛒 Organic Cart"), callback_data='baskets')],
                   [InlineKeyboardButton(_("🛍️ All Goods"), callback_data='categories')]]
        if user.has_permission('scan_qr_menu_access'):
            buttons.append([InlineKeyboardButton(_("📸 Product QR Scanner"), callback_data="scan_qr")])
        buttons.append([InlineKeyboardButton(_("👤 My profile"), callback_data='profile')])
        buttons.append([InlineKeyboardButton(_("💭 Help"), callback_data='help')])
        buttons.append([InlineKeyboardButton(_("🙌 Share to friends"), switch_inline_query=_(" is an organic bot, which contains the maximum range of organic products with prices. It also helps to order all this online 📲 Just try it 🤗"))])
        buttons.append([InlineKeyboardButton("🇺🇦🇷🇺🇬🇧", callback_data='language')])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    @require_permission("change_language_menu_access")
    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton("🇬🇧ENG", callback_data='language_en')],
                   [InlineKeyboardButton("🇺🇦UKR", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺RUS", callback_data='language_ru')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Select a language:"),
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def start_ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "Якою мовою бажаєш спілкуватися?\n" \
                       "In what language do you want to communicate?\n" \
                       "На каком языке ты хочешь общаться?"
        buttons = [[InlineKeyboardButton("🇺🇦Українська", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺Русский", callback_data='language_ru')],
                   [InlineKeyboardButton("🇬🇧English", callback_data='language_en')],
                   ]

        send_or_edit(context, chat_id=user.chat_id, text=message_text,
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        value = update.callback_query.data.replace("language_", "")

        if value == 'en':
            user.language_code = 'en'
        elif value == 'ru':
            user.language_code = 'ru'
        elif value == 'uk':
            user.language_code = 'uk'
        else:
            user.language_code = 'en'

        add_to_db(user)
        context.user_data['_'] = _ = user.translator

        self.send_message(context)

        return self.States.ACTION

    def back_to_menu(self, update, context):
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        help_menu = HelpMenu(self)
        profile_menu = ProfileMenu(self)
        categories_menu = CategoriesMenu(self)
        baskets_menu = BasketsMenu(self)
        scan_qr_menu = ScanQRMenu(self)
        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'start', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern='^start$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.ask_language, pattern="^language$"),
                                                               profile_menu.handler,
                                                               categories_menu.handler,
                                                               help_menu.handler,
                                                               baskets_menu.handler,
                                                               scan_qr_menu.handler],
                                          self.States.LANGUAGE: [CallbackQueryHandler(self.set_language, pattern="^language_\w\w$")],
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/admin$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/operator$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/courier$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
