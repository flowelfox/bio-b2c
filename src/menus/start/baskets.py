import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, require_permission, add_to_db, generate_regex_handlers
from botmanlib.messages import send_or_edit, delete_user_message, remove_interface, remove_interface_markup
from botmanlib.validators import PhoneNumber
from formencode import Invalid
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler

from src.menus.start.buy_basket import BuyBasketMenu
from src.menus.start.my_baskets import MyBasketsMenu
from src.menus.take_basket import TakeBasketMenu
from src.models import DBSession, City, BasketOrder


class BasketsMenu(BaseMenu):
    menu_name = 'user_baskets_menu'

    class States(enum.Enum):
        ACTION = 1
        CITY = 2
        ASK_PHONE = 3
        OTHER_CITY = 4

    @require_permission("user_baskets_menu_access")
    def entry(self, update, context):
        user = context.user_data['user']

        if user.city is None:
            return self.ask_city(update, context)

        elif user.phone is None:
            return self.ask_phone(update, context)

        self.send_message(context)
        return self.States.ACTION

    def ask_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        user.city = None
        add_to_db(user)

        message_text = "🛒 <b>" + _("Organic Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("Choose your city from the list of available") + "</b>\n\n"
        message_text += _("⚠️ Not found among available? Select 💬 <b>Another city</b> and enter a name.")

        buttons = []
        all_cities = DBSession.query(City).all()
        for city in all_cities:
            buttons.append([InlineKeyboardButton(city.get_translation(user.language_code).name, callback_data=f"city_{city.id}")])

        buttons.append([InlineKeyboardButton(_("⏪ Main menu"), callback_data=f"back_{self.menu_name}")])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.CITY

    def set_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        city = DBSession.query(City).get(int(update.callback_query.data.replace("city_", "")))
        user.city = city
        add_to_db(user)

        if city.get_translation(user.language_code).name == _("💬 Another City"):
            return self.ask_other_city(update, context)
        else:
            remove_interface_markup(context)
            if user.phone is None:
                return self.ask_phone(update, context)
            else:
                self.send_message(context)
                return self.States.ACTION

    def ask_other_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "💬 <b>" + _("Another city") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("Enter and send me the name of your city.") + "\n\n"
        message_text += "<i>" + _("We will notify you when it becomes available.") + "</i>\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_city_select")]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.OTHER_CITY

    def set_other_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        user.other_city = update.effective_message.text
        add_to_db(user)
        send_or_edit(context, chat_id=user.chat_id, text=_("Your city is saved"))

        if user.phone is None:
            return self.ask_phone(update, context)
        else:
            self.send_message(context)
            return self.States.ACTION

    def phone_needed_answer(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("📲 <b>Phone number is used in two cases:</b>") + "\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("1. To identify the client when contacting support.\n"
                          "2. For feedback when completing an order.") + "\n\n"
        message_text += _("You can read more in the <a href=\"{link}\">terms of use</a>.").format(link="https://buyorganic.in.ua/organicisbot-terms") + "\n\n"
        message_text += _("📨 If you have questions:\n"
                          "@BuyBio ")

        context.bot.send_message(chat_id=user.chat_id, text=message_text, parse_mode="HTML")
        return self.States.ASK_PHONE

    def ask_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        message_text = "🛒 <b>" + _("Organic Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("To form your Cart - we also need your phone number 📲\nBy submitting a phone number, you agree to the <a href=\"{link}\">terms of use</a>.").format(link="https:/google.com") + "\n\n"
        message_text += _("You can send it by <b>clicking the button below 👇</b>")

        buttons = [[KeyboardButton(_("📲 Send phone number"), request_contact=True)],
                   [KeyboardButton(_("🤔 Why we need phone number"))]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True), parse_mode="HTML")
        return self.States.ASK_PHONE

    def set_phone_contact(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        phone = update.message.contact.phone_number
        user.phone = phone
        add_to_db(user)
        send_or_edit(context, chat_id=user.chat_id, text=_("Your phone number is saved"), reply_markup=ReplyKeyboardRemove())
        remove_interface(context)
        return self.entry(update, context)

    def set_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        try:
            validator = PhoneNumber()
            phone = validator.to_python(text)
            user.phone = phone
            add_to_db(user)
            send_or_edit(context, chat_id=user.chat_id, text=_("Your phone number is saved"), reply_markup=ReplyKeyboardRemove())
            remove_interface(context)
            return self.entry(update, context)
        except Invalid:
            remove_interface(context, 'phone_invalid')
            send_or_edit(context, 'phone_invalid', chat_id=user.chat_id, text=_("Please enter phone number in +XXYYYZZZZZZZ format"))
            return self.ask_phone(update, context)

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        basket_orders = DBSession.query(BasketOrder) \
            .filter(BasketOrder.user_id == user.id) \
            .filter(BasketOrder.payed == True) \
            .all()

        active_basket_orders = [basket_order for basket_order in basket_orders if basket_order.active()]
        baskets_ordered = len(active_basket_orders)

        message_text = "🛒 <b>" + _("Organic Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        if len(user.basket_orders) > 0:
            message_text += _("Select the required section")
        else:
            message_text += _("You have no orders yet.") + "\n\n"
            message_text += _("Let start with 🛎️ <b>Order a cart</b>, where you can choose it for your taste")

        buttons = []
        if user.has_permission("user_buy_basket_menu_access"):
            buttons.append(InlineKeyboardButton(_("🛎️️ Order a cart"), callback_data='buy_basket'))
        if user.has_permission("user_my_baskets_menu_access"):
            buttons.append(InlineKeyboardButton(_("🗂️ My carts") + f" ({baskets_ordered})", callback_data='my_baskets'))
        if user.has_permission("user_take_basket_menu_access"):
            buttons.append(InlineKeyboardButton(_("🛍️ Get a cart"), callback_data='user_take_basket'))

        buttons.append(InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}'))

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(group_buttons(buttons, 1)), parse_mode="HTML")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        buy_basket_menu = BuyBasketMenu(self)
        my_baskets_menu = MyBasketsMenu(self)
        take_basket_menu = TakeBasketMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^baskets$")],
                                      states={
                                          self.States.ACTION: [
                                              CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                              my_baskets_menu.handler,
                                              buy_basket_menu.handler,
                                              take_basket_menu.handler,
                                          ],
                                          self.States.CITY: [CallbackQueryHandler(self.set_city, pattern=r"^city_\d+$"),
                                                             CallbackQueryHandler(self.ask_other_city, pattern=r"^other_city$"),
                                                             CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$')],
                                          self.States.ASK_PHONE: generate_regex_handlers("🤔 Why we need phone number", self.phone_needed_answer) +
                                                                 [MessageHandler(Filters.contact, self.set_phone_contact),
                                                                  MessageHandler(Filters.text, self.set_phone)],
                                          self.States.OTHER_CITY: [CallbackQueryHandler(self.ask_city, pattern="^back_to_city_select$"),
                                                                   MessageHandler(Filters.text, self.set_other_city)]

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
