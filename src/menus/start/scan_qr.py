import enum
import logging
from io import BytesIO

from PIL import Image
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import require_permission, check_payment, add_to_db, make_payment
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface
from formencode import validators, Invalid
from pyzbar.pyzbar import decode
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.error import TimedOut
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.helpers import strtokeycap
from src.models import Product, DBSession, database, User, ProductOrder, ProductsOrder, ProductTranslation
from src.settings import FONDY_TOKEN, WEBHOOK_ENABLE

logger = logging.getLogger(__name__)


class ScanQRMenu(BaseMenu):
    menu_name = 'scan_qr_menu'

    class States(enum.Enum):
        ACTION = 1
        ADD_PRODUCT = 2
        CART = 3
        EXIT = 4
        WAIT_PAY = 5
        MORE = 6

    @require_permission('scan_qr_menu_access')
    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['cart'] = []

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📸 Product QR Scanner") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("📸 To buy a product, take a photo of its QR code and send it to me")

        buttons = []
        if context.user_data[self.menu_name]['cart']:
            buttons.append([InlineKeyboardButton(_("🛒 Cart"), callback_data="qr_cart")])
            buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_ask")])
        else:
            buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")])

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def scan_qr_code(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        with BytesIO() as file:
            photo = update.effective_message.photo[-1]
            tries = 0
            while tries < 3:
                try:
                    tries += 1
                    photo.get_file().download(out=file)
                    tries = 5
                except TimedOut:
                    pass
            if tries != 5:
                raise TimedOut

            file.seek(0)
            decoded_data = decode(Image.open(file))
            delete_user_message(update)
            if decoded_data:
                product = DBSession.query(Product).get(decoded_data[0].data.decode('utf-8'))
                if product:
                    context.user_data[self.menu_name]['item'] = {"product": product,
                                                                 "quantity": 1}
                    return self.product_info(update, context)
                else:
                    message_text = "<b>" + _("📸 Product QR Scanner") + "</b>\n"
                    message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                    message_text += _("Product not found.")
                    buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
                    send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
                    return self.States.ACTION
            else:
                _ = context.user_data['user'].translator
                message_text = "<b>" + _("📸 Product QR Scanner") + "</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("Can't recognize QR code.") + "\n\n"
                message_text += _("📸 To buy a product, take a photo of its QR code and send it to me")

                buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
                send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
                return self.States.ACTION

    def manually_entered_id(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        delete_interface(context)

        try:
            val = validators.Int(min=1)
            product_id = val.to_python(update.effective_message.text)

            product = DBSession.query(Product).get(product_id)
            if product:
                context.user_data[self.menu_name]['item'] = {"product": product,
                                                             "quantity": 1}
                return self.product_info(update, context)
            else:
                message_text = "<b>" + _("📸 Product QR Scanner") + "</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("Product not found.")
                buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
                send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
                return self.States.ACTION

        except Invalid:
            message_text = "<b>" + _("📸 Product QR Scanner") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("Wrong product ID.")
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
            return self.States.ACTION

    def product_info(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        item = context.user_data[self.menu_name]['item']
        price = f"{item['product'].price:.2f}" if item['product'].price else "??"

        translation = item['product'].get_translation(user.language_code)
        message_text = ""
        if translation.image is not None:
            message_text += f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
        message_text += "<b>" + _("🛒 Your Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += "<b>" + _("You added to the cart:") + "</b>\n"
        message_text += f"1️⃣ {(translation.name if translation.name else _('Unknown'))}\n"
        message_text += _("Packing:") + f" {item['product'].packaging} {translation.packaging_unit}\n"
        message_text += _("Cost:") + f" {price} " + _("UAH") + "\n\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("You can increase the amount of each product in the cart (➕)")

        buttons = [[InlineKeyboardButton("➖", callback_data="qr_quantity_remove"),
                    InlineKeyboardButton(f"{item['quantity']}", callback_data="nothing"),
                    InlineKeyboardButton("➕", callback_data="qr_quantity_add")],
                   [InlineKeyboardButton(_("👀 Peek under the wrap"), callback_data=f"more_{self.menu_name}")],
                   [InlineKeyboardButton(_("📋 Checkout"), callback_data="add_to_cart")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        if update.callback_query:
            update.callback_query.answer()
        return self.States.ADD_PRODUCT

    def more(self, update, context):
        _ = context.user_data['user'].translator
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]
        translation = context.user_data[self.menu_name]['item']['product'].get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = ProductTranslation(name=_("Unknown"), description=_("Unknown"), short_description=_("Unknown"))
        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.MORE

    def add_to_cart(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        item = context.user_data[self.menu_name]['item']

        if item['product'].price is None:
            message_text = _("Can't add product to cart. Product has no price.")
            buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
            return self.States.ADD_PRODUCT
        for cart_item in context.user_data[self.menu_name]['cart']:
            if item['product'].id == cart_item['product'].id:
                message_text = _("Can't add product to cart. Product already added to cart.")
                buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data="back_to_menu")]]
                send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
                return self.States.ADD_PRODUCT

        context.user_data[self.menu_name]['cart'].append(item)
        return self.qr_cart(update, context)

    def quantity_add(self, update, context):
        item = context.user_data[self.menu_name]['item']
        if item['quantity'] < 10:
            item['quantity'] += 1

        return self.product_info(update, context)

    def quantity_remove(self, update, context):
        item = context.user_data[self.menu_name]['item']
        if item['quantity'] > 1:
            item['quantity'] -= 1

        return self.product_info(update, context)

    def qr_cart(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📋 Checkout") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += "<b>" + _("Product List:") + "</b>\n"
        price = 0
        for idx, item in enumerate(context.user_data[self.menu_name]['cart']):
            translation = item['product'].get_translation(user.language_code)
            message_text += f"{strtokeycap(str(idx + 1))} {translation.name} {item['quantity']} " + _("pack.")
            message_text += f" x {item['product'].price} {_('UAH')}"
            message_text += f" = {item['product'].price * item['quantity']} {_('UAH')}\n"
            price += item['product'].price * item['quantity']
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("🏷️ TOTAL:") + f" {price:.2f} {_('UAH')}</b>"

        buttons = [[InlineKeyboardButton(_("📸 Add Product"), callback_data="back_to_menu")],
                   [InlineKeyboardButton(_("📝 Confirm"), callback_data="confirm_order")],
                   [InlineKeyboardButton(_("🚫 Cancel the order"), callback_data=f"back_{self.menu_name}")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        if update.callback_query:
            update.callback_query.answer()
        return self.States.CART

    def confirm_order(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        product_order = ProductOrder(user_id=user.id)
        price = 0
        for item in context.user_data[self.menu_name]['cart']:
            price += item['product'].price * item['quantity']
            product_basket_order_asoc = ProductsOrder(product_id=item['product'].id, product_order=product_order, quantity=item['quantity'])
            DBSession.add(product_basket_order_asoc)

        product_order.price = price
        add_to_db(product_order, DBSession)
        context.user_data[self.parent.menu_name]['product_order'] = product_order

        description = _("Ordering products from shop")
        response = make_payment(FONDY_TOKEN, int(price * 100), description, response_url=self.bot.link)
        if not response:
            return self.conv_fallback(context)

        for job in context.job_queue.get_jobs_by_name(f"check_pay_{user.chat_id}"):
            job.schedule_removal()

        bot_context = {'order_id': response['order_id'],
                       'user_context': context}

        context.job_queue.run_repeating(self.wait_pay, 5, name=f"checking_pay_{user.chat_id}", context=bot_context)

        message_text = "<b>" + _("📝 Confirm") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You have formed your personal organic cart ✌️") + "\n"
        message_text += _("Remains only to 💳 <b>Pay</b>") + "\n\n"
        message_text += _("After the transaction, funds will be credited automatically within 5-10 seconds") + "\n"

        buttons = [[InlineKeyboardButton(_("💳 Pay ({price:.2f} UAH)").format(price=price), url=response['checkout_url'])],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_cart')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.WAIT_PAY

    def wait_pay(self, context):
        order_id = context.job.context['order_id']
        user_context = context.job.context['user_context']
        session = database.sessionmaker()

        try:
            user = session.query(User).get(user_context.user_data['user'].id)
            _ = user_context.user_data['_']

            data = check_payment(FONDY_TOKEN, order_id)
            if not data or data == 'error':
                context.job.schedule_removal()
                context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))
            elif data == "pending":
                pass
            elif data == "success":
                context.job.schedule_removal()

                product_order = session.query(ProductOrder).get(user_context.user_data[self.parent.menu_name]['product_order'].id)
                product_order.payed = True
                product_order.payment_id = order_id

                if not add_to_db(product_order, session):
                    context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))

                message_text = "<b>" + _("🔔 Product paid") + "</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("Congratulations! Your Payment was successful 😀") + "\n"
                message_text += _("Order ID:") + f" {product_order.id}\n\n"
                message_text += "<b>" + _("⚠️ Please wait for the operator to confirm your order") + "</b>"

                buttons = [[InlineKeyboardButton(_("✅ ОК"), callback_data=f'back_{self.menu_name}')]]

                send_or_edit(user_context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

                # send message to operator
                try:
                    operators = product_order.products[0].category.delivery_center.operators
                    if not operators:
                        raise IndexError
                    for operator in operators:
                        _ = operator.user.translator

                        operator_buttons = [[InlineKeyboardButton(_("✔ Confirm"), callback_data=f'qr_buy_confirm_{product_order.id}')],
                                            [InlineKeyboardButton(_("🚫 Cancel"), callback_data=f'qr_buy_decline_{product_order.id}')]]

                        operator_message_text = "<b>" + _("🔔 Product paid") + "</b>\n"
                        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
                        operator_message_text += "<b>" + _("Order ID:") + f" {product_order.id}</b>\n\n"
                        operator_message_text += "<b>" + _("Product List:") + "</b>\n"
                        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
                        price = 0
                        for idx, item in enumerate(user_context.user_data[self.menu_name]['cart']):
                            translation = item['product'].get_translation(operator.user.language_code)
                            operator_message_text += f"{strtokeycap(str(idx + 1))} {translation.name} {item['quantity']} " + _("pack.")
                            operator_message_text += f" x {item['product'].price} {_('UAH')}"
                            operator_message_text += f" = {item['product'].price * item['quantity']} {_('UAH')}\n"
                            price += item['product'].price * item['quantity']
                        operator_message_text += "<b>" + _("🏷️ TOTAL:") + f" {price:.2f} {_('UAH')}</b>\n\n"
                        operator_message_text += "<b>" + _("👤 Customer:") + "</b>\n"
                        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
                        operator_message_text += _("ID:") + f" {user.chat_id}\n"
                        operator_message_text += _("Name:") + f" <a href=\"{user.mention_url}\">{user.get_name()}</a>\n"
                        operator_message_text += _("Phone:") + f" {user.phone if user.phone else _('Unknown')}\n\n"
                        operator_message_text += "<b>" + _("⚠️ Carefully check the contents and compliance with your order") + "</b>"

                        send_or_edit(user_context,
                                     dispatcher=self.dispatcher,
                                     user_id=operator.user.chat_id,
                                     chat_id=operator.user.chat_id,
                                     interface_name='qr_buy_products_order',
                                     text=operator_message_text,
                                     reply_markup=InlineKeyboardMarkup(operator_buttons),
                                     parse_mode="HTML")

                except IndexError:
                    logger.error("Can't find operator for delivery center")

        finally:
            session.close()

    def back(self, update, context):
        self.parent.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def back_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = _("You added some products to your cart, if you exit your cart will be cleared.\nContinue?")
        buttons = [[InlineKeyboardButton(_("⏪ Yes"), callback_data=f"back_{self.menu_name}"),
                    InlineKeyboardButton(_("❌ No, go back"), callback_data=f"back_to_menu")]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.EXIT

    def nothing(self, update, context):
        update.callback_query.answer()
        return self.States.ADD_PRODUCT

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^scan_qr$')],
                                      states={
                                          self.States.ACTION: [MessageHandler(Filters.photo, self.scan_qr_code),
                                                               MessageHandler(Filters.text, self.manually_entered_id),
                                                               CallbackQueryHandler(self.qr_cart, pattern=f"^qr_cart$"),
                                                               CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                               CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                               CallbackQueryHandler(self.back_ask, pattern=f"^back_ask$")],
                                          self.States.ADD_PRODUCT: [CallbackQueryHandler(self.add_to_cart, pattern="^add_to_cart$"),
                                                                    CallbackQueryHandler(self.quantity_remove, pattern="^qr_quantity_remove$"),
                                                                    CallbackQueryHandler(self.quantity_add, pattern="^qr_quantity_add$"),
                                                                    CallbackQueryHandler(self.more, pattern=f"^more_{self.menu_name}$"),
                                                                    CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                                                    CallbackQueryHandler(self.nothing, pattern="^nothing$")],
                                          self.States.CART: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                             CallbackQueryHandler(self.confirm_order, pattern=f"^confirm_order$"),
                                                             CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$")],
                                          self.States.EXIT: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                             CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$")],

                                          self.States.WAIT_PAY: [CallbackQueryHandler(self.qr_cart, pattern="^back_to_cart$"),
                                                                 CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$")],

                                          self.States.MORE: [CallbackQueryHandler(self.product_info, pattern="^back_to_menu$")]

                                      },
                                      fallbacks=[MessageHandler(Filters.all,
                                                                lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)
        return handler
