import enum

from botmanlib.menus import ArrowAddEditMenu
from botmanlib.menus.helpers import generate_regex_handlers, require_permission
from botmanlib.messages import delete_interface
from botmanlib.validators import PhoneNumber
from formencode import validators
from telegram import InlineKeyboardButton, TelegramError
from telegram.ext import ConversationHandler, CallbackQueryHandler

from src.models import DBSession, User, City, CityTranslation


class ProfileMenu(ArrowAddEditMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = 'profile_menu'
    model = User
    show_reset_field = True
    reset_to_default = True
    use_html_text = True
    force_action = ArrowAddEditMenu.Action.EDIT
    variants_per_page = 10
    resend_after_each_field = True
    show_field_selectors = False

    @require_permission('profile_menu_access')
    def entry(self, update, context):
        return super(ProfileMenu, self).entry(update, context)

    def load(self, context):
        user = context.user_data['user']
        _ = user.translator

        super(ProfileMenu, self).load(context)

        context.user_data[self.menu_name]['city_name'] = user.city.get_translation(user.language_code).name if user.city else None

        if not context.user_data[self.menu_name]['desired_name']:
            context.user_data[self.menu_name]['desired_name'] = user.name
            context.user_data[self.menu_name]['active_field'] = 1

    def query_object(self, context):
        return DBSession.query(User).get(context.user_data['user'].id)

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_cities = DBSession.query(City).all()
        all_cities_translation_names = [city.get_translation(user.language_code).name for city in all_cities]

        return [self.Field('desired_name', _("1.Name*"), validator=validators.String(), required=True),
                self.Field('city_name', _("2.City*"), validator=validators.String(), required=True, variants=all_cities_translation_names),
                self.Field('other_city', _("2.1 City name*"), validator=validators.String(), required=True, depend_field='city_name', depend_value=_("💬 Another City")),
                self.Field('phone', _("3.Phone*"), validator=PhoneNumber(), required=True, before_validate=lambda data: data if data.startswith('+') else '+' + data, invalid_message=_("Invalid phone number 🙁\nPlease enter it in format:\n38XXXXXXXXXX") + '\n\n<i>' + _("Example: ") + "380500000000</i>"),
                self.Field('email', _("4.Email"), validator=validators.Email(), invalid_message=_("Invalid Email 🙁\nPlease try again")),
                ]

    def variant_center_buttons(self, context):
        return []

    def entry_points(self):
        return generate_regex_handlers("My profile", self.entry, left_text='📋') + \
               [CallbackQueryHandler(self.entry, pattern='profile')]

    def message_top_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "<b>" + _("👤 My profile") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'
        return message_text

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator

        active_field = self.active_field(context)
        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("<i>* Required fields</i>") + "\n\n"
        if active_field.param == 'desired_name':
            message_text += _("✍ <b>Please enter your Name and Surname and send me</b>") + "\n"
        if active_field.param == 'phone':
            message_text += _("✍ <b>Please enter your phone number in the XXXXXXXXXXXX format and send it to me</b>") + "\n"
        if active_field.param == 'email':
            message_text += _("✍ <b>Please enter your Email and send me.</b>") + "\n"
        if active_field.param == 'city_name':
            message_text += _("✅ <b>Please select your city</b>\n\n⚠️ Not found among available? Select 💬 <b>Another city</b> and enter a name.") + "\n"
        if active_field.param == 'other_city':
            message_text += _("✍ Enter and send me the name of your city.\nWe will notify you when it becomes available.") + "\n"

        return message_text

    def save_button(self, context):
        return None

    def bottom_buttons(self, context):
        _ = context.user_data['user'].translator
        menu_data = context.user_data[self.menu_name]
        if menu_data['desired_name'] and \
                menu_data['phone']:
            return [[InlineKeyboardButton(_("✅ Save"), callback_data=f"save_{self.menu_name}")]]

    def reset_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("🧹 Reset field"), callback_data="reset_field")

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def save(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if not self.check_fields(context):
            delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        delete_interface(context, 'variations_interface')

        self.update_object(context)
        try:
            self.bot.answer_callback_query(update.callback_query.id, text=_("Your information saved."), show_alert=True)
        except TelegramError:
            self.bot.send_message(chat_id=user.chat_id, text=_("Your information saved."))

        return self.back(update, context)

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        _ = user_data['_']

        city_translation = DBSession.query(CityTranslation).filter(CityTranslation.name == user_data[self.menu_name]['city_name']).first()
        obj.city_id = city_translation.city_id
        if user_data[self.menu_name]['city_name'] != _("💬 Another City"):
            obj.other_city = None

        DBSession.add(obj)
        DBSession.commit()
