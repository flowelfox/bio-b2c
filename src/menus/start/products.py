import enum

from botmanlib.menus import BunchListMenu, OneListMenu
from botmanlib.menus.helpers import to_state, require_permission
from botmanlib.messages import send_or_edit, delete_interface
from sqlalchemy.sql.functions import count
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import Product, ProductTranslation, Category, DBSession
from src.settings import WEBHOOK_ENABLE


class CategoriesMenu(BunchListMenu):
    menu_name = "category_products_menu"
    model = Category
    auto_hide_arrows = True

    @require_permission('product_menu_access')
    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = user.translator
        delete_interface(context, interface_name='main')
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Category).filter(Category.parent_category_id == None).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^categories$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator
        if obj:
            message_text = "<b>" + _("🛍️ All Goods") + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("Please select category") + "\n"
        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def object_buttons(self, context, categories):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        for category in categories:
            categories_size = DBSession.query(count(Category.id)).filter(Category.parent_category_id == category.id).scalar()
            trans = category.get_translation(user.language_code)
            if trans:
                buttons.append([InlineKeyboardButton(trans.name + " (" + f"{categories_size}" + ") ", callback_data=f"subgroup_{category.id}")])
        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        subcategories_menu = SubCategoriesMenu(self)
        return {self.States.ACTION: [subcategories_menu.handler]}


class SubCategoriesMenu(BunchListMenu):
    menu_name = "subcategory_products_menu"
    model = Category
    auto_hide_arrows = True

    @require_permission('product_menu_access')
    def entry(self, update, context):
        self._load(context)
        context.user_data[self.menu_name]['parent_category_id'] = int(update.callback_query.data.replace("subgroup_", ""))

        user = context.user_data['user']
        _ = user.translator
        delete_interface(context, interface_name='main')
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Category).join(Category.products).filter(Category.parent_category_id == context.user_data[self.menu_name]['parent_category_id']).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^subgroup_\d+$')]

    def message_text(self, context, category):
        user = context.user_data['user']
        _ = user.translator
        parent_category = DBSession.query(Category).get(context.user_data[self.menu_name]['parent_category_id'])

        if category:
            message_text = "<b>" + parent_category.get_translation(user.language_code).name + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("Please select category") + "\n"
        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def object_buttons(self, context, objects):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        for obj in objects:
            products_size = DBSession.query(count(Product.id)).filter(Product.category_id == obj.id).first()
            trans = obj.get_translation(user.language_code)
            buttons.append([InlineKeyboardButton(trans.name + " (" + f"{products_size[0]}" + ") ", callback_data=f"egroup_{obj.id}")])
        return buttons

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        product_menu = ProductsMenu(self)
        return {self.States.ACTION: [product_menu.handler]}


class ProductsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2

    menu_name = "products_menu"
    model = Product
    disable_web_page_preview = False
    auto_hide_arrows = True

    def entry(self, update, context):
        _ = context.user_data['user'].translator

        data = update.callback_query.data
        self._load(context)

        if 'egroup_' in data:
            context.user_data[self.menu_name]['category_id'] = int(data.replace("egroup_", ""))
            context.user_data[self.menu_name]['product_id'] = None

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        product = DBSession.query(Product).filter(Product.category_id == context.user_data[self.menu_name]['category_id']).all()
        return product

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^egroup_\d+$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        context.user_data[self.menu_name]['product_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        self.parent.send_message(context)
        return ConversationHandler.END

    def object_buttons(self, context, objects):
        _ = context.user_data['user'].translator
        buttons = []
        if objects:
            buttons.append([InlineKeyboardButton(_("👀 Peek under the wrap"), callback_data='product_more')])
        return buttons

    def more(self, update, context):
        _ = context.user_data['user'].translator
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_product')]]
        translation = self.selected_object(context).get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = ProductTranslation(name=_("Unknown"), description=_("Unknown"), short_description=_("Unknown"))
        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if obj:
            translation = obj.get_translation(user.language_code)
            manufacturer_translation = obj.manufacturer.get_translation(user.language_code) if obj.manufacturer else None
            if translation.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += f"<b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            if translation.short_description:
                message_text += translation.short_description + "\n\n"
            elif translation.description:
                message_text += translation.description[:100] + "...\n\n"

            if manufacturer_translation:
                message_text += _("Producer") + f": {manufacturer_translation.name}\n"
            if obj.packaging:
                message_text += _("Package") + f": {obj.packaging} {translation.packaging_unit if translation.packaging_unit else ''}\n"
            if obj.price:
                message_text += _("Price") + f": {obj.price:.2f} {_('UAH')}\n"

        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        obj = self.selected_object(context)

        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += obj.category.get_translation(user.language_code).name + " (" + _("Product") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def back_to_product(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.more, pattern='^product_more$')],
                self.States.MORE: [CallbackQueryHandler(self.back_to_product, pattern='^back_to_product$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))]}
