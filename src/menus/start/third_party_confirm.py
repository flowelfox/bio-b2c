import enum
import re

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import prepare_user
from botmanlib.messages import delete_user_message, delete_interface, send_or_edit
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.models import User, FormedBasket, DBSession


class ThirdPartyConfirmMenu(BaseMenu):
    menu_name = "third_party_confirm"

    class States(enum.Enum):
        ACTION = 1
        DECLINE = 2

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        _ = context.user_data['user'].translator
        data = update.callback_query.data

        context.user_data[self.menu_name]['owner_transfer_confirmation'] = update.effective_message

        if "owner_transfer_confirm_" in data:
            m = re.match(r"owner_transfer_confirm_(\d+)_tu(\d+)", data)
            fm_id, tu_id = m.groups()
            context.user_data[self.menu_name]['formed_basket_id'] = int(fm_id)
            context.user_data[self.menu_name]['transfer_user_id'] = int(tu_id)
            return self.confirm_transfer(update, context)

        elif "owner_transfer_decline_" in data:
            m = re.match(r"owner_transfer_decline_(\d+)_tu(\d+)", data)
            fm_id, tu_id = m.groups()
            context.user_data[self.menu_name]['formed_basket_id'] = int(fm_id)
            context.user_data[self.menu_name]['transfer_user_id'] = int(tu_id)
            return self.ask_decline(update, context)

        else:
            return ConversationHandler.END

    def back_to_owner_notification(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        center_closes_message = context.user_data[self.menu_name]['owner_transfer_confirmation']
        send_or_edit(context, "owner_transfer_confirmation", chat_id=user.chat_id, text=center_closes_message.text, reply_markup=center_closes_message.reply_markup)
        del context.user_data[self.menu_name]['owner_transfer_confirmation']
        return self.States.ACTION

    def confirm_transfer(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = context.user_data[self.menu_name]['formed_basket_id']
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)

        transfer_user_id = context.user_data[self.menu_name]['transfer_user_id']
        transfer_user = DBSession.query(User).get(transfer_user_id)

        # send fake update as operator
        self.fake_callback_update(formed_basket.holder, 'confirm_as_owner_transfer')

        message_text = "<b>" + _("🛍️ Get a Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        transfer_user_link = f"<a href=\"{transfer_user.mention_url}\">{transfer_user.get_name()}</a>"
        message_text += _("You successfully confirmed transfer of your cart to {user}.").format(user=transfer_user_link) + "\n"

        buttons = [[InlineKeyboardButton(_("⏪ Main menu"), callback_data=f"back_to_main_menu")]]
        send_or_edit(context, "owner_transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def ask_decline(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = context.user_data[self.menu_name]['formed_basket_id']
        context.user_data[self.menu_name]['declined_basket_id'] = formed_basket_id

        message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("✍️ Please write the reason for the refusal and send it to me")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_confirm")]]
        send_or_edit(context, "owner_transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.DECLINE

    def decline_confirm(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['decline_reason'] = update.effective_message.text

        if "declined_basket_id" not in context.user_data[self.menu_name]:
            return self.States.FINISH

        self.decline_details(update, context)
        delete_user_message(update)

        return self.States.DECLINE

    def decline_details(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You canceled the receipt of the cart with the following comment:") + '\n'
        message_text += "💬 <b>" + context.user_data[self.menu_name]['decline_reason'] + "</b>\n\n"

        buttons.append([InlineKeyboardButton(_("✅ Confirm decline"), callback_data=f"confirm_basket_decline")])
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_confirm")])

        send_or_edit(context, "owner_transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.DECLINE

    def confirm_basket_decline(self, update, context):
        user = context.user_data['user']
        formed_basket_id = context.user_data[self.menu_name]['declined_basket_id']
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)

        formed_basket.decline_pass(context.user_data[self.menu_name]['decline_reason'], None, DBSession)

        # send message to operator/courier
        _ = formed_basket.holder.translator
        translation = formed_basket.basket_order.basket.get_translation(formed_basket.holder.language_code)
        operator_message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        operator_message_text += "<b>" + _("{name} canceled the transfer of the cart:").format(name=user.get_name()) + "</b>\n"
        operator_message_text += _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}\n"
        operator_message_text += _("ID:") + f" {formed_basket.id}\n\n"
        operator_message_text += "<b>" + _("💬 Indicated cause:") + "</b>\n"
        operator_message_text += context.user_data[self.menu_name]['decline_reason']

        operator_buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'to_decline_confirm')]]
        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=formed_basket.holder.chat_id,
                     chat_id=formed_basket.holder.chat_id,
                     interface_name='transfer_basket',
                     text=operator_message_text,
                     reply_markup=InlineKeyboardMarkup(operator_buttons),
                     parse_mode="HTML")

        return self.back_to_main_menu(update, context)

    def back_to_confirm(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        transfer_interface_message = context.user_data[self.menu_name]['owner_transfer_confirmation']
        send_or_edit(context, 'owner_transfer_confirmation', chat_id=user.chat_id, text=transfer_interface_message.text, reply_markup=transfer_interface_message.reply_markup)
        if 'declined_basket_id' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['declined_basket_id']
        if 'owner_transfer_confirmation' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['owner_transfer_confirmation']

        return self.States.ACTION

    def back_to_main_menu(self, update, context):
        user = context.user_data['user']

        delete_interface(context, "owner_transfer_confirmation")
        self.fake_callback_update(user, 'start', update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'^owner_transfer_confirm_\d+_tu\d+$'),
                                                    CallbackQueryHandler(self.entry, pattern=r'^owner_transfer_decline_\d+_tu\d+$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back_to_main_menu, pattern="^back_to_main_menu$")],
                                          self.States.DECLINE: [MessageHandler(Filters.text, self.decline_confirm),
                                                                CallbackQueryHandler(self.back_to_confirm, pattern="^back_to_confirm$"),
                                                                CallbackQueryHandler(self.confirm_basket_decline, pattern="^confirm_basket_decline$"),
                                                                CallbackQueryHandler(self.decline_details, pattern="^to_decline_confirm$")]
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)
        return handler
