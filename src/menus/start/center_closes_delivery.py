import datetime
import enum

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import prepare_user, add_to_db, make_payment, check_payment
from botmanlib.messages import delete_user_message, delete_interface, send_or_edit
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.models import User, FormedBasket, database, Operator, DBSession
from src.settings import FONDY_TOKEN


class CenterClosesDeliveryMenu(BaseMenu):
    menu_name = "center_closes_delivery"

    class States(enum.Enum):
        ACTION = 1
        DELIVERY_ADDRESS = 2
        DELIVERY_DETAILS = 3
        WAIT_PAY = 4

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        _ = context.user_data['user'].translator
        data = update.callback_query.data

        context.user_data[self.menu_name]['center_closes_message'] = update.effective_message

        if "close_order_delivery_" in data:
            context.user_data[self.menu_name]['formed_basket_id'] = int(data.replace("close_order_delivery_", ""))
            return self.ask_delivery_address(update, context)
        else:
            return ConversationHandler.END

    def back_to_closed_notification(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        center_closes_message = context.user_data[self.menu_name]['center_closes_message']
        send_or_edit(context, f"close_center_notification_{context.user_data[self.menu_name]['formed_basket_id']}", chat_id=user.chat_id, text=center_closes_message.text, reply_markup=center_closes_message.reply_markup)
        del context.user_data[self.menu_name]['center_closes_message']

        return self.States.ACTION

    def ask_delivery_address(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        message_text = "<b>" + _("🚚 Order Delivery") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += "<b>" + _("✍️ Enter and send me your address") + "</b>\n\n"
        message_text += _("Example:\nst. August 23, building 1, apt. 1, intercom code 137")
        buttons = [[InlineKeyboardButton(_("🚫 Cancel delivery"), callback_data='back_to_closed_notification')]]
        send_or_edit(context, f"close_center_notification_{context.user_data[self.menu_name]['formed_basket_id']}", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.DELIVERY_ADDRESS

    def set_delivery_address(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.message.text
        formed_basket = DBSession.query(FormedBasket).get(context.user_data[self.menu_name]['formed_basket_id'])
        delivery_center = formed_basket.basket_order.basket.delivery_center

        context.user_data[self.menu_name]['address'] = text
        context.user_data[self.menu_name]['delivery_start_time'] = datetime.time(hour=delivery_center.close_time.hour - 2, minute=delivery_center.close_time.minute)
        context.user_data[self.menu_name]['delivery_end_time'] = delivery_center.close_time
        delete_user_message(update)

        return self.delivery_details(update, context)

    def delivery_details(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        address = context.user_data[self.menu_name]['address']
        cost = context.user_data[self.menu_name]['cost'] = 99.0
        delivery_time = f"{context.user_data[self.menu_name]['delivery_start_time'].strftime('%H:%M')}-{context.user_data[self.menu_name]['delivery_end_time'].strftime('%H:%M')}"

        message_text = "<b>" + _("🚚 Order Delivery") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Please carefully check your address and the selected delivery time:") + "\n\n"
        message_text += "<b>" + _("📍 Your Address:") + f"</b> {address}\n"
        message_text += f"🕐 <b>{delivery_time}</b>\n"
        message_text += "<b>" + _("🏷️ Cost:") + f"</b> {cost:.2f} " + _("UAH") + "\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("🔔 We will notify you when the courier will leave to you")

        buttons = [[InlineKeyboardButton(_("📝 Confirm"), callback_data='confirm_delivery')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_delivery_time')]]

        send_or_edit(context, f"close_center_notification_{context.user_data[self.menu_name]['formed_basket_id']}", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.DELIVERY_DETAILS

    def confirm_delivery(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        cost = context.user_data[self.menu_name]['cost']

        # create invoice
        order_description = _("Delivery for cart {cart_id}").format(cart_id=context.user_data[self.menu_name]['formed_basket_id'])
        response = make_payment(FONDY_TOKEN, int(cost * 100), order_description, response_url=self.bot.link)
        if not response:
            return self.conv_fallback(context)

        for job in context.job_queue.get_jobs_by_name(f"check_pay_{user.chat_id}"):
            job.schedule_removal()

        bot_context = {'order_id': response['order_id'],
                       'user_context': context}

        context.job_queue.run_repeating(self.wait_pay, 5, name=f"checking_pay_{user.chat_id}", context=bot_context)

        # send message to user
        message_text = "<b>" + _("📝 Confirm") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You have ordered a cart delivery ✌️") + "\n"
        message_text += _("Remains only to 💳 <b>Pay</b>") + "\n\n"
        message_text += _("After the transaction, funds will be credited automatically within 5-10 seconds") + "\n"

        buttons = [[InlineKeyboardButton(_("💳 Pay ({price:.2f} UAH)").format(price=cost), url=response['checkout_url'])],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_delivery_details')]]

        send_or_edit(context, f"close_center_notification_{context.user_data[self.menu_name]['formed_basket_id']}", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.WAIT_PAY

    def wait_pay(self, context):
        order_id = context.job.context['order_id']
        user_context = context.job.context['user_context']
        session = database.sessionmaker()

        try:
            user = session.query(User).get(user_context.user_data['user'].id)
            _ = user_context.user_data['_']

            data = check_payment(FONDY_TOKEN, order_id)
            if not data or data == 'error':
                context.job.schedule_removal()
                context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))
            elif data == "pending":
                pass
            elif data == "success":
                context.job.schedule_removal()
                delivery_time = f"{user_context.user_data[self.menu_name]['delivery_start_time'].strftime('%H:%M')}-{user_context.user_data[self.menu_name]['delivery_end_time'].strftime('%H:%M')}"

                formed_basket = session.query(FormedBasket).get(user_context.user_data[self.menu_name]['formed_basket_id'])
                formed_basket.address = user_context.user_data[self.menu_name]['address']
                formed_basket.delivery_price = user_context.user_data[self.menu_name]['cost']
                formed_basket.delivery_start_time = user_context.user_data[self.menu_name]['delivery_start_time']
                formed_basket.delivery_end_time = user_context.user_data[self.menu_name]['delivery_end_time']

                if not add_to_db(formed_basket, session):
                    context.bot.send_message(chat_id=user.chat_id, text=_("Error while processing payment."))

                message_text = "<b>" + _("🚚 Order Delivery") + "</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += _("Congratulations! You have successfully ordered delivery. Wait for a courier 😀") + "\n\n"
                message_text += "<b>" + _("📍 Your Address:") + f"</b> {formed_basket.address}\n"
                message_text += f"🕐 <b>{delivery_time}</b>\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
                message_text += "<b>" + _("🔔 We will inform you when the courier receives your cart.") + "</b>"

                buttons = [[InlineKeyboardButton(_("⏪ Main menu"), callback_data='back_to_main_menu')]]
                send_or_edit(user_context, f"close_center_notification_{user_context.user_data[self.menu_name]['formed_basket_id']}", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

                operators = session.query(User).join(User.operator).filter(Operator.delivery_center_id == formed_basket.basket_order.basket.delivery_center_id).all()

                for operator in operators:
                    _ = operator.translator
                    operator_message_text = _("User ordered delivery to for basket #{basket_id}").format(basket_id=formed_basket.id)
                    operator_buttons = [[InlineKeyboardButton(_("Close message"), callback_data='close_message')]]
                    context.bot.send_message(chat_id=operator.chat_id, text=operator_message_text, reply_markup=InlineKeyboardMarkup(operator_buttons), parse_mode="HTML")

        finally:
            session.close()

    def back_to_main_menu(self, update, context):
        user = context.user_data['user']

        delete_interface(context, f"close_center_notification_{context.user_data[self.menu_name]['formed_basket_id']}")
        self.fake_callback_update(user, 'start', update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'^close_order_delivery_\d+$')],
                                      states={
                                          self.States.ACTION: [],
                                          self.States.DELIVERY_ADDRESS: [MessageHandler(Filters.text, self.set_delivery_address),
                                                                         CallbackQueryHandler(self.back_to_closed_notification, pattern=r"^back_to_closed_notification$")],
                                          self.States.DELIVERY_DETAILS: [CallbackQueryHandler(self.confirm_delivery, pattern="^confirm_delivery$")],
                                          self.States.WAIT_PAY: [CallbackQueryHandler(self.delivery_details, pattern="^back_to_delivery_details$"),
                                                                 CallbackQueryHandler(self.back_to_main_menu, pattern="^back_to_main_menu$")]
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)
        return handler
