import enum
import math

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import get_settings, group_buttons, require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.settings import SETTINGS_FILE, WEBHOOK_ENABLE


class HelpMenu(BaseMenu):
    menu_name = 'help_menu'

    class States(enum.Enum):
        ACTION = 1
        CHOOSED_QUESTION = 2

    @require_permission('help_menu_access')
    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):

        user = context.user_data['user']
        _ = user.translator
        settings = get_settings(SETTINGS_FILE)

        questions_list = []
        message_text = "<b>" + _("💭 Help") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        for help_element in sorted(settings['help'], key=lambda x: x.get('priority', 0), reverse=True):
            questions_list.append(help_element['question'][user.language_code])

        flat_buttons = []
        for idx, question in enumerate(questions_list):
            message_text += f"{idx + 1}.  {question}\n"
            flat_buttons.append(InlineKeyboardButton(f"{idx + 1}. {question}", callback_data=f'question_{idx + 1}'))

        buttons = group_buttons(flat_buttons, math.ceil(len(flat_buttons) / 10))
        buttons.append([InlineKeyboardButton(_("💭 Online Consultant"), url='t.me/BuyBio')])
        buttons.append([InlineKeyboardButton(_("⏪ Main Menu"), callback_data=f'back_{self.menu_name}')])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def set_question(self, update, context):
        settings = get_settings(SETTINGS_FILE)
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        data = update.callback_query.data
        quertion_id = int(data.replace("question_", ""))

        message_text = ""
        for idx, help_element in enumerate(sorted(settings['help'], key=lambda x: x.get('priority', 0), reverse=True)):
            if quertion_id == idx + 1:
                image = help_element.get("image", None)
                trans_image = image.get(user.language_code, None) if image else None
                message_text = f'<a href="{self.bot.get_image_url(trans_image)}">\u200B</a>' if trans_image and WEBHOOK_ENABLE else ""
                message_text += f"{idx + 1}. {help_element['question'][user.language_code]}\n"
                message_text += "〰〰〰〰〰〰〰〰〰〰\n"
                message_text += help_element['answer'][user.language_code]

        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data='back_from_help')])
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def back_from_help(self, update, context):
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^help$')],
                                      states={
                                          self.States.ACTION: [
                                              CallbackQueryHandler(self.back_from_help, pattern="^back_from_help$"),
                                              CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                              CallbackQueryHandler(self.set_question, pattern="^question_\d+$")]

                                      },
                                      fallbacks=[MessageHandler(Filters.all,
                                                                lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)
        return handler
