import datetime
import enum
import re
from io import BytesIO
from os import path

from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db
from botmanlib.menus.helpers import prepare_user
from botmanlib.messages import delete_interface
from botmanlib.messages import send_or_edit, delete_user_message
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.models import DBSession, User, ProductOrder, ProductsOrder
from src.settings import RESOURCES_FOLDER


class QRBuyConfirmMenu(BaseMenu):
    menu_name = "qr_buy_confirm_menu"

    class States(enum.Enum):
        ACTION = 1
        DELIVERY_ADDRESS = 2
        DELIVERY_TIME = 3
        DELIVERY_DETAILS = 4
        WAIT_PAY = 5

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        _ = context.user_data['user'].translator
        data = update.callback_query.data

        context.user_data[self.menu_name]['qr_buy_products_order'] = context.user_data['interfaces']['qr_buy_products_order'].message
        if "qr_buy_decline_" in data:
            context.user_data[self.menu_name]['order_id'] = int(data.replace("qr_buy_decline_", ""))
            return self.decline_order(update, context)
        elif "qr_buy_confirm_" in data:
            context.user_data[self.menu_name]['order_id'] = int(data.replace("qr_buy_confirm_", ""))
            return self.confirm_order(update, context)
        else:
            delete_interface(context, "qr_buy_products_order")
            return ConversationHandler.END

    def back_to_notification(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        qr_buy_products_order_message = context.user_data[self.menu_name]['qr_buy_products_order']
        send_or_edit(context, 'qr_buy_products_order', chat_id=user.chat_id, text=qr_buy_products_order_message.text, reply_markup=qr_buy_products_order_message.reply_markup)
        del context.user_data[self.menu_name]['qr_buy_products_order']
        return self.States.ACTION

    def confirm_order(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        order = DBSession.query(ProductOrder).get(context.user_data[self.menu_name]['order_id'])
        if order:
            order.confirmed = True
            add_to_db(order, DBSession)

        message_text = "<b>" + _("🔔 Product paid") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("Order ID:") + f" {order.id}</b>\n\n"
        message_text += _("Confirmed.")

        buttons = [[InlineKeyboardButton(_("⏪ Operator menu"), callback_data='back_to_operator')]]
        send_or_edit(context, "qr_buy_products_order", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        _ = order.user.translator

        user_buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_scan_qr_menu')]]

        user_message_text = "<b>" + _("🛍️ Get a Cart") + "</b>\n"
        user_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        user_message_text += "<b>" + _("Congratulations! The cart found its owner 🥳") + "</b>\n\n"
        user_message_text += _("Bon appetit and see you!")

        result_pdf = self.create_pdf_receipt(order)
        with BytesIO() as receipt:
            result_pdf.write(receipt)
            receipt.seek(0)

            filename = _("Receipt") + '.pdf'
            send_or_edit(context,
                         dispatcher=self.dispatcher,
                         user_id=order.user.chat_id,
                         chat_id=order.user.chat_id,
                         document=receipt,
                         filename=filename,
                         caption=message_text,
                         reply_markup=InlineKeyboardMarkup(user_buttons),
                         parse_mode="HTML")

        return self.States.ACTION

    def decline_order(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        order = DBSession.query(ProductOrder).get(context.user_data[self.menu_name]['order_id'])
        if order:
            order.confirmed = False
            add_to_db(order, DBSession)

        message_text = "<b>" + _("🔔 Product paid") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("Order ID:") + f" {order.id}</b>\n\n"
        message_text += _("Declined.")

        buttons = [[InlineKeyboardButton(_("⏪ Operator menu"), callback_data='back_to_operator')]]
        send_or_edit(context, "qr_buy_products_order", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def create_pdf_receipt(self, order):
        _ = order.user.translator

        overlay = BytesIO()
        can = canvas.Canvas(overlay, pagesize=A4, bottomup=0)

        # order id and date
        can.setFont("Arial-Bold", 14)
        date = re.sub(':.*?:', "", f"ОЦ1-{order.id:07} від {datetime.date.today().strftime('%d.%m.%Y')} р.")
        can.drawString(155, 107, date)

        # buyer
        can.setFont("Arial", 9)
        buyer_name = re.sub(':.*?:', "", order.user.desired_name if order.user.desired_name else order.user.get_name())
        can.drawString(107, 192, buyer_name)

        # phone
        can.setFont("Arial", 9)
        phone = re.sub(':.*?:', "", order.user.phone if order.user.phone else " - ")
        can.drawString(125, 203, phone)

        # baskets
        can.setFont("Arial", 8)
        basket_y = 275

        total_price = 0
        for idx, product in enumerate(order.products):
            translation = product.get_translation('uk')
            product_order_asoc = DBSession.query(ProductsOrder).get((product.id, order.id))
            can.drawString(37, basket_y, f"{idx + 1}")
            can.setFont("Arial-Italic", 8)
            can.drawString(52, basket_y, f"{product.id}")
            can.setFont("Arial", 8)
            name = translation.name + f" ({product.packaging} {translation.packaging_unit})"
            if len(name) > 69:
                name = name[:69] + "..."
            can.drawString(73, basket_y, name)
            can.drawString(361, basket_y, f"{product_order_asoc.quantity} шт")
            can.drawString(433, basket_y, f"{product.price:.2f}".replace(".", ","))
            can.drawString(505, basket_y, f"{product_order_asoc.quantity * product.price:.2f}".replace(".", ","))
            total_price += product.price * product_order_asoc.quantity
            basket_y += 13
        basket_y += 2
        total_price = f"{total_price:.2f}".replace(".", ",")

        can.setFont("Arial-Bold", 8)
        can.drawString(433, basket_y, "Всього:")
        can.drawString(505, basket_y, total_price)
        basket_y += 13

        can.drawString(433, basket_y, "у т.ч. ПДВ:")
        can.drawString(505, basket_y, "0,00")
        basket_y += 15

        uah, coins = total_price.split(",")
        can.drawString(37, basket_y, "Всього найменувань {0}, на суму {1} грн {2} коп.".format(len(order.products), uah, coins))
        basket_y += 13
        can.drawString(37, basket_y, "без ПДВ")

        can.save()
        overlay.seek(0)

        overlay_pdf = PdfFileReader(overlay, strict=False)
        # read template PDF
        template_pdf = PdfFileReader(open(path.join(RESOURCES_FOLDER, 'receipt.pdf'), 'rb'), strict=False)

        result_pdf = PdfFileWriter()
        template_page = template_pdf.getPage(0)
        template_page.mergePage(overlay_pdf.getPage(0))
        result_pdf.addPage(template_page)

        return result_pdf

    def back_to_operator_menu(self, update, context):
        user = context.user_data['user']

        delete_interface(context, 'qr_buy_products_order')
        self.fake_callback_update(user, 'operator', update.callback_query.id)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern=r'^qr_buy_decline_\d+$'),
                                                    CallbackQueryHandler(self.entry, pattern=r'^qr_buy_confirm_\d+$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back_to_operator_menu, pattern="^back_to_operator$")],
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)
        return handler
