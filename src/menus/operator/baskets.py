import datetime
import enum

import pytz
from botmanlib.menus import OneListMenu
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import require_permission, inline_placeholder
from botmanlib.messages import send_or_edit, delete_user_message
from sqlalchemy import func, Date
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.basket_products_info import BasketProductsMenu
from src.menus.operator.add_carrier import AddCourierMenu
from src.menus.operator.carrier_baskets import ForACarrierMenu
from src.menus.transfer_menu import TransferMenu
from src.models import DBSession
from src.models import FormedBasket, FormedBasketStatus


class CartsForDeliveryMenu(BaseMenu):
    menu_name = 'carts_for_delivery_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        all_carts = DBSession.query(func.count(FormedBasket.id)).filter(FormedBasket.holder_id == user.id).filter(FormedBasket.status == FormedBasketStatus.filled).scalar()
        courier_carts = DBSession.query(func.count(FormedBasket.id)) \
            .filter(FormedBasket.delivery_date.cast(Date) == datetime.date.today()) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.has_delivery == True).scalar()

        message_text = "<b>" + _("⏏️ Carts for Delivery") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Select the required section:")

        buttons = [[InlineKeyboardButton(_("⏏️ All carts ({all_carts})").format(all_carts=all_carts), callback_data='all_baskets')],
                   [InlineKeyboardButton(_("🚚 For a Courier ({courier_carts})").format(courier_carts=courier_carts), callback_data='baskets_for_courier')],
                   [InlineKeyboardButton(_("📟 Add Courier"), callback_data='add_courier')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        baskets_menu = BasketsMenu(self)
        add_courier_menu = AddCourierMenu(self)
        for_a_courier_menu = ForACarrierMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^baskets$")],
                                      states={
                                          self.States.ACTION: [baskets_menu.handler,
                                                               add_courier_menu.handler,
                                                               for_a_courier_menu.handler,
                                                               CallbackQueryHandler(inline_placeholder(self.States.ACTION, "Menu under development"), pattern=f"^baskets_for_courier$"),
                                                               CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$")],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class BasketsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = "operator_baskets_menu"
    disable_web_page_preview = False

    @require_permission("operator_baskets_menu_access")
    def entry(self, update, context):
        self._load(context)
        context.user_data[self.menu_name]['filter'] = "all"

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']

        query = DBSession.query(FormedBasket) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.status == FormedBasketStatus.filled)

        if context.user_data[self.menu_name]['filter'] == 'without_delivery':
            query = query.filter(FormedBasket.has_delivery == False)

        return query.all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^all_baskets$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, obj):
        _ = context.user_data['user'].translator
        buttons = []
        if obj:
            if context.user_data[self.menu_name]['filter'] == 'all':
                buttons.append([InlineKeyboardButton(_("🛅 Without delivery"), callback_data='change_filter')])
            elif context.user_data[self.menu_name]['filter'] == 'without_delivery':
                buttons.append([InlineKeyboardButton(_("⏏️ All"), callback_data='change_filter')])

            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'more_{obj.basket_order.basket.id}')])
            buttons.append([InlineKeyboardButton(_("ℹ Cart details"), callback_data=f'details_{obj.id}')])
            buttons.append([InlineKeyboardButton(_("✅ Give out the cart"), callback_data='operator_transfer_basket')])
        return buttons

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if obj:
            translation = obj.basket_order.basket.get_translation(user.language_code)
            message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("ID:") + f" {obj.id}</b>\n\n"

            adapted_date = pytz.utc.localize(obj.delivery_date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
            message_text += _("🗓️ Date of issue:") + " " + adapted_date.strftime("%d.%m.%Y") + "\n\n"

            message_text += "<b>" + _("👤 Customer:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("ID:") + f" {obj.basket_order.user.chat_id}\n"
            message_text += _("Name:") + f" <a href=\"{obj.basket_order.user.mention_url}\">{obj.basket_order.user.get_name()}</a>\n"
            message_text += _("Phone:") + f" {obj.basket_order.user.phone if obj.basket_order.user.phone else _('Unknown')}\n"
            city = obj.basket_order.user.city.get_translation(user.language_code).name if obj.basket_order.user.city else _('Unknown')
            if obj.basket_order.user.city.get_translation(user.language_code).name == _("💬 Another City"):
                city += f" ({obj.basket_order.user.other_city})"
            message_text += _("City:") + f" {city}\n\n"

        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def change_filter(self, update, context):
        if context.user_data[self.menu_name]['filter'] == 'all':
            context.user_data[self.menu_name]['filter'] = 'without_delivery'
        else:
            context.user_data[self.menu_name]['filter'] = 'all'

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("details_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if context.user_data[self.menu_name]['filter'] == 'without_delivery':
            message_text += _("🛅 Carts without delivery") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        elif context.user_data[self.menu_name]['filter'] == 'all':
            message_text += _("⏏️ All carts") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def additional_states(self):
        transfer_menu = TransferMenu(self)
        basket_more = BasketProductsMenu(self)

        return {self.States.ACTION: [transfer_menu.handler,
                                     basket_more.handler,
                                     CallbackQueryHandler(self.details, pattern=r"^details_\d+$"),
                                     CallbackQueryHandler(self.change_filter, pattern="^change_filter$"),
                                     CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$")]}
