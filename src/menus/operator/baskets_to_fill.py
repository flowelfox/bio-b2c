import enum

import pytz
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.helpers import strtokeycap
from src.menus.basket_products_info import BasketProductsMenu
from src.models import DBSession, ProductFormedBasket, FormedBasket, FormedBasketStatus, BasketOrder
from src.settings import WEBHOOK_ENABLE


class BasketToFillMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2

    menu_name = "baskets_to_fill_menu"
    disable_web_page_preview = False

    @require_permission("baskets_to_fill_menu_access")
    def entry(self, update, context):
        return super(BasketToFillMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        formed_baskets = DBSession.query(FormedBasket) \
            .join(FormedBasket.basket_order) \
            .filter(BasketOrder.payed == True) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.status == FormedBasketStatus.new) \
            .order_by(FormedBasket.delivery_date.asc()) \
            .all()

        return formed_baskets

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^baskets_to_fill$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, formed_basket):
        _ = context.user_data['user'].translator
        buttons = []
        if formed_basket:
            buttons.append([InlineKeyboardButton(_("✅ Confirm and notify"), callback_data='basket_filled')])
            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'more_{formed_basket.basket_order.basket.id}')])
            buttons.append([InlineKeyboardButton(_("ℹ Cart details"), callback_data=f'details_{formed_basket.id}')])
        return buttons

    def message_text(self, context, formed_basket):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if formed_basket:
            translation = formed_basket.basket_order.basket.get_translation(user.language_code)
            if translation.image is not None:
                message_text += f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += "<b>" + _("Cart name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            adapted_date = pytz.utc.localize(formed_basket.delivery_date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
            message_text += _("🗓️ Date of issue:") + " " + adapted_date.strftime("%d.%m.%Y") + "\n\n"

            message_text += "<b>" + _("👤 Customer:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("ID:") + f" {formed_basket.basket_order.user.chat_id}\n"
            message_text += _("Name:") + f" <a href=\"{formed_basket.basket_order.user.mention_url}\">{formed_basket.basket_order.user.get_name()}</a>\n"
            message_text += _("Phone:") + f" {formed_basket.basket_order.user.phone if formed_basket.basket_order.user.phone else _('Unknown')}\n"
            city = formed_basket.basket_order.user.city.get_translation(user.language_code).name if formed_basket.basket_order.user.city else _('Unknown')
            if formed_basket.basket_order.user.city.get_translation(user.language_code).name == _("💬 Another City"):
                city += f" ({formed_basket.basket_order.user.other_city})"
            message_text += _("City:") + f" {city}\n\n"

            message_text += "<b>" + _("Product List:") + "</b>\n"
            for idx, product in enumerate(formed_basket.products):
                product_formed_basket_asoc = DBSession.query(ProductFormedBasket).get((product.id, formed_basket.id))
                message_text += f"{strtokeycap(str(idx + 1))} {product.get_translation(user.language_code).name}, {product_formed_basket_asoc.quantity} " + _("pack.") + "\n"
            message_text += "\n"

        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("☑️ Form carts") + " (" + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("details_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def fill_basket(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket = self.selected_object(context)
        formed_basket.fill(DBSession)

        update.callback_query.answer()
        message_text = "<b>" + _("✅ Cart Formed") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("ID: {id}").format(id=formed_basket.id) + "</b>\n\n"
        message_text += "<b>" + _("⚠️ Do not forget to indicate this ID on the package.") + "</b>\n\n"
        message_text += _("🔔 An automatic alert about the readiness of the cart was sent to the client.")

        buttons = [[InlineKeyboardButton(_("☑️ Form next one"), callback_data="back_to_baskets_to_fill")],
                   [InlineKeyboardButton(_("⏪ Main menu"), callback_data="to_main_menu")]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        # send message to user
        _ = formed_basket.basket_order.user.translator
        client_buttons = [[InlineKeyboardButton(_("🚚 Delivery (99 UAH)"), callback_data=f'order_delivery_{formed_basket.id}')],
                          [InlineKeyboardButton(_("👌 I manage on my own (0 UAH)"), callback_data=f'order_take_{formed_basket.id}')]]
        delivery_center = formed_basket.basket_order.basket.delivery_center
        delivery_center_translation = delivery_center.get_translation(formed_basket.basket_order.user.language_code)
        address = delivery_center_translation.address if delivery_center_translation else _("Unknown")
        if delivery_center.open_time and delivery_center.close_time:
            work_time = f"{delivery_center.open_time.strftime('%H:%M')}-{delivery_center.close_time.strftime('%H:%M')}"
        else:
            work_time = _("Unknown")

        push_text = "<b>" + _("🔔 Organics day has come!") + "</b>\n"
        push_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        push_text += _("Your organic cart has already been formed and is waiting today at the Issue Center:") + "\n\n"
        push_text += _("📍 Address: {address}").format(address=address) + "\n"
        push_text += _("🕰️ Work time: {work_time}").format(work_time=work_time) + "\n\n"
        push_text += _("Do not have time to get to the issuing center - order delivery! 🚚")

        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=formed_basket.basket_order.user.chat_id,
                     chat_id=formed_basket.basket_order.user.chat_id,
                     interface_name='filled_basket_notification',
                     text=push_text,
                     reply_markup=InlineKeyboardMarkup(client_buttons),
                     parse_mode="HTML")

        self.update_objects(context)
        return self.States.ACTION

    def to_main_menu(self, update, context):
        user = context.user_data['user']
        self.fake_callback_update(user, "operator", callback_query_id=update.callback_query.id)
        return ConversationHandler.END

    def back_to_baskets_to_fill(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        basket_more = BasketProductsMenu(self)
        return {self.States.ACTION: [CallbackQueryHandler(self.fill_basket, pattern="^basket_filled$"),
                                     CallbackQueryHandler(self.back_to_baskets_to_fill, pattern="^back_to_baskets_to_fill$"),
                                     CallbackQueryHandler(self.to_main_menu, pattern="^to_main_menu$"),
                                     CallbackQueryHandler(self.back_to_menu, pattern=r"^back_to_menu$"),
                                     CallbackQueryHandler(self.details, pattern=r"^details_\d+$"),
                                     basket_more.handler],
                self.States.MORE: [CallbackQueryHandler(self.back_to_baskets_to_fill, pattern='^back_to_baskets_to_fill$')]}
