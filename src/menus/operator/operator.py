import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, prepare_user, require_permission
from botmanlib.messages import send_or_edit, delete_user_message, remove_interface, delete_interface
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler, PrefixHandler

from src.menus.mixins.welcome_menu import WelcomeMenuMixin
from src.menus.operator.baskets import CartsForDeliveryMenu
from src.menus.operator.baskets_to_fill import BasketToFillMenu
from src.menus.start.profile import ProfileMenu
from src.menus.take_basket import TakeBasketMenu
from src.models import User, Operator, DBSession, DeliveryCenter


class OperatorMenu(BaseMenu, WelcomeMenuMixin):
    menu_name = 'operator_menu'

    class States(enum.Enum):
        ACTION = 1
        LANGUAGE = 2
        CITY = 3
        ASK_PHONE = 4

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        _ = context.user_data['user'].translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if not user.has_permission('operator_menu_access'):
            delete_user_message(update)
            return ConversationHandler.END

        if update.effective_message.text and update.effective_message.text.startswith("/"):
            delete_interface(context)

        if user.city is None:
            return self.ask_city(update, context)

        elif user.phone is None:
            return self.ask_phone(update, context)

        if user.operator is None:
            operator = Operator(user=user)
            add_to_db(operator)
            user = DBSession.query(User).get(user.id)

        if user.operator.delivery_center is None:
            return self.ask_delivery_center(update, context)

        self.send_message(context)
        return self.States.ACTION

    def ask_delivery_center(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        delivery_centers = DBSession.query(DeliveryCenter).filter(DeliveryCenter.city == user.city).all()

        buttons = []
        if delivery_centers:
            for delivery_center in delivery_centers:
                buttons.append([InlineKeyboardButton(delivery_center.get_translation(user.language_code).name, callback_data=f'delivery_center_{delivery_center.id}')])

            send_or_edit(context, chat_id=user.chat_id, text=_("Please select your delivery center:"), reply_markup=InlineKeyboardMarkup(buttons))
        else:
            send_or_edit(context, chat_id=user.chat_id, text=_("There are currently no Organic Issuing Centers available in your city."))

        return self.States.ACTION

    def set_delivery_center(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        delivery_center_id = int(update.callback_query.data.replace("delivery_center_", ""))
        user.operator.delivery_center_id = delivery_center_id
        add_to_db(user.operator)

        send_or_edit(context, chat_id=user.chat_id, text=_("Delivery center assigned to you"))
        remove_interface(context)
        return self.send_message(context)

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        dc_translation = user.operator.delivery_center.get_translation(user.language_code)

        message_text = "<b>" + _("👩‍💻 Operator Center") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You work in the \"{delivery_center}\" at \"{address}\"").format(delivery_center=dc_translation.name, address=dc_translation.address) + "\n"
        message_text += _("Select the required section:")

        buttons = [[InlineKeyboardButton(_("⏏️ Carts for delivery"), callback_data='baskets')],
                   [InlineKeyboardButton(_("🔄 Get a cart"), callback_data='operator_take_basket')],
                   [InlineKeyboardButton(_("☑️ Form carts"), callback_data='baskets_to_fill')],
                   [InlineKeyboardButton(_("👤 My profile"), callback_data='profile')],
                   [InlineKeyboardButton("🇺🇦🇷🇺🇬🇧", callback_data='language')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    @require_permission("change_language_menu_access")
    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton("🇬🇧ENG", callback_data='language_en')],
                   [InlineKeyboardButton("🇺🇦UKR", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺RUS", callback_data='language_ru')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Select a language:"),
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        value = update.callback_query.data.replace("language_", "")

        if value == 'en':
            user.language_code = 'en'
        elif value == 'ru':
            user.language_code = 'ru'
        elif value == 'uk':
            user.language_code = 'uk'
        else:
            user.language_code = 'en'

        add_to_db(user)
        context.user_data['_'] = _ = user.translator

        self.send_message(context)

        return self.States.ACTION

    def back_to_menu(self, update, context):
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        profile_menu = ProfileMenu(self)
        baskets_to_fill_menu = BasketToFillMenu(self)
        carts_for_delivery_menu = CartsForDeliveryMenu(self)
        take_basket_menu = TakeBasketMenu(self)
        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'operator', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern="^operator$")],
                                      states={
                                          self.States.ACTION: [baskets_to_fill_menu.handler,
                                                               carts_for_delivery_menu.handler,
                                                               take_basket_menu.handler,
                                                               CallbackQueryHandler(self.ask_language, pattern="^language$"),
                                                               CallbackQueryHandler(self.set_delivery_center, pattern=r"^delivery_center_\d+$"),
                                                               profile_menu.handler],
                                          self.States.LANGUAGE: [CallbackQueryHandler(self.set_language, pattern=r"^language_\w\w$")],
                                          self.States.CITY: [MessageHandler(Filters.text, self.find_city),
                                                             CallbackQueryHandler(self.set_city)],
                                          self.States.ASK_PHONE: [MessageHandler(Filters.contact, self.set_phone_contact),
                                                                  MessageHandler(Filters.text, self.set_phone)]
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/admin$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/start$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/courier$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
