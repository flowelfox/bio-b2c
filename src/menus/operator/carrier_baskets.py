import datetime
import enum

import pytz
from botmanlib.menus import OneListMenu
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from sqlalchemy import Date, func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.basket_products_info import BasketProductsMenu
from src.models import DBSession
from src.models import FormedBasket


class ForACarrierMenu(BaseMenu):
    menu_name = 'for_a_carrier_menu'

    class States(enum.Enum):
        ACTION = 1

    @require_permission("for_carrier_baskets_menu_access")
    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        query = DBSession.query(func.count(FormedBasket.id)) \
            .filter(FormedBasket.delivery_date.cast(Date) == datetime.date.today()) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.has_delivery == True)
        all_for_today = query.scalar()

        time1 = query.filter(FormedBasket.delivery_start_time >= datetime.time(10)) \
            .filter(FormedBasket.delivery_end_time <= datetime.time(12)).scalar()

        time2 = query.filter(FormedBasket.delivery_start_time >= datetime.time(12)) \
            .filter(FormedBasket.delivery_end_time <= datetime.time(14)).scalar()

        time3 = query.filter(FormedBasket.delivery_start_time >= datetime.time(14)) \
            .filter(FormedBasket.delivery_end_time <= datetime.time(16)).scalar()

        time4 = query.filter(FormedBasket.delivery_start_time >= datetime.time(16)) \
            .filter(FormedBasket.delivery_end_time <= datetime.time(18)).scalar()

        time5 = query.filter(FormedBasket.delivery_start_time >= datetime.time(18)) \
            .filter(FormedBasket.delivery_end_time <= datetime.time(20)).scalar()

        message_text = "<b>" + _("🚚 Carts for a Courier") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Select a time interval:")

        buttons = [[InlineKeyboardButton(_("🔢 All for today") + f" ({all_for_today})", callback_data="carts_time_0000_2359")],
                   [InlineKeyboardButton("🕐 10:00-12:00" + f" ({time1})", callback_data="carts_time_1000_1200")],
                   [InlineKeyboardButton("🕐 12:00-14:00" + f" ({time2})", callback_data="carts_time_1200_1400")],
                   [InlineKeyboardButton("🕐 14:00-16:00" + f" ({time3})", callback_data="carts_time_1400_1600")],
                   [InlineKeyboardButton("🕐 16:00-18:00" + f" ({time4})", callback_data="carts_time_1600_1800")],
                   [InlineKeyboardButton("🕐 18:00-20:00" + f" ({time5})", callback_data="carts_time_1800_2000")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        carrier_baskets_menu = CarrierBasketsMenu(self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^baskets_for_courier$")],
                                      states={
                                          self.States.ACTION: [carrier_baskets_menu.handler,
                                                               CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$")],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class CarrierBasketsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = "carrier_baskets_menu"
    disable_web_page_preview = False

    @require_permission("for_carrier_baskets_menu_access")
    def entry(self, update, context):
        self._load(context)

        data = update.callback_query.data
        times = data.replace("carts_time_", "").split("_")
        context.user_data[self.menu_name]['delivery_start_time'] = datetime.time(hour=int(times[0][:2]), minute=int(times[0][2:]))
        context.user_data[self.menu_name]['delivery_end_time'] = datetime.time(hour=int(times[1][:2]), minute=int(times[1][2:]))

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']

        query = DBSession.query(FormedBasket) \
            .filter(FormedBasket.delivery_date.cast(Date) == datetime.date.today()) \
            .filter(FormedBasket.delivery_start_time >= context.user_data[self.menu_name]['delivery_start_time']) \
            .filter(FormedBasket.delivery_end_time <= context.user_data[self.menu_name]['delivery_end_time']) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.has_delivery == True)

        return query.all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r"^carts_time_\d{4}_\d{4}$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, obj):
        _ = context.user_data['user'].translator
        buttons = []
        if obj:
            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'more_{obj.basket_order.basket.id}')])
            buttons.append([InlineKeyboardButton(_("ℹ Cart details"), callback_data=f'details_{obj.id}')])
            if obj.basket_order.user.username:
                buttons.append([InlineKeyboardButton(_("💭 Chat with customer"), url=f"t.me/{obj.basket_order.user.username}")])

        return buttons

    def message_text(self, context, formed_basket):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if formed_basket:
            translation = formed_basket.basket_order.basket.get_translation(user.language_code)
            message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("ID:") + f" {formed_basket.id}</b>\n\n"

            adapted_date = pytz.utc.localize(formed_basket.delivery_date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
            message_text += _("🗓️ Date of issue:") + " " + adapted_date.strftime("%d.%m.%Y") + "\n\n"

            message_text += "<b>" + _("Delivery Data:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("📍 Address:") + f" {formed_basket.address}\n"
            delivery_time = f"{formed_basket.delivery_start_time.strftime('%H:%M')}-{formed_basket.delivery_end_time.strftime('%H:%M')}"
            message_text += f"🕐 {delivery_time}\n\n"

            message_text += "<b>" + _("👤 Customer:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("ID:") + f" {formed_basket.basket_order.user.chat_id}\n"
            message_text += _("Name:") + f" <a href=\"{formed_basket.basket_order.user.mention_url}\">{formed_basket.basket_order.user.get_name()}</a>\n"
            message_text += _("Phone:") + f" {formed_basket.basket_order.user.phone if formed_basket.basket_order.user.phone else _('Unknown')}\n"
            city = formed_basket.basket_order.user.city.get_translation(user.language_code).name if formed_basket.basket_order.user.city else _('Unknown')
            if formed_basket.basket_order.user.city.get_translation(user.language_code).name == _("💬 Another City"):
                city += f" ({formed_basket.basket_order.user.other_city})"
            message_text += _("City:") + f" {city}\n\n"

        else:
            message_text = "<b>" + _("🚚 Carts for a Courier") + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("There is no carts for selected time interval")

        return message_text

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("details_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("🚚 Carts for a courier") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def additional_states(self):
        basket_products_menu = BasketProductsMenu(self)
        basket_more = BasketProductsMenu(self)
        return {self.States.ACTION: [basket_more.handler,
                                     CallbackQueryHandler(self.details, pattern=r"^details_\d+$"),
                                     CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     basket_products_menu.handler]}
