import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import prepare_user, add_to_db, remove_from_db
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface
from sqlalchemy import or_
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from src.menus.mixins.welcome_menu import WelcomeMenuMixin
from src.models import User, DBSession, Courier, Permission


class AddCourierMenu(BaseMenu, WelcomeMenuMixin):
    menu_name = 'add_courier_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        _ = context.user_data['user'].translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if update.effective_message.text and update.effective_message.text.startswith("/"):
            delete_interface(context)

        if not user.has_permission('add_courier_menu_access'):
            delete_user_message(update)
            return ConversationHandler.END

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📟 Add a Courier") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Enter and send me a Name or ID or Username") + "\n\n"
        message_text += _("⚠️ The user must have a Telegram account and be authorized in {bot_name}").format(bot_name=f"@{context.bot.username}")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        add_carrier_search_menu = AddCarrierSearchMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^add_courier$")],
                                      states={
                                          self.States.ACTION: [add_carrier_search_menu.handler,
                                                               CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$")],
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class AddCarrierSearchMenu(OneListMenu):
    menu_name = 'add_carrier_search_menu'
    parse_mode = ParseMode.HTML
    allow_reentry = False

    def entry(self, update, context):
        self._load(context)
        text = update.effective_message.text
        if text.isdigit():
            context.user_data[self.menu_name]['users'] = DBSession.query(User).filter(or_(User.id == int(text), User.chat_id == int(text))).all()
        else:
            context.user_data[self.menu_name]['users'] = DBSession.query(User).filter(or_(User.first_name.ilike(f"%{text}%"), User.last_name.ilike(f"%{text}%"), User.username == text)).all()
        delete_interface(context)

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return context.user_data[self.menu_name]['users']

    def entry_points(self):
        return [MessageHandler(Filters.text, self.entry)]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("📟 Add a Courier") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        if obj:
            message_text += _("ID: ") + str(obj.id) + '\n'
            message_text += _("Name: ") + f"<a href=\"tg://user?id={obj.chat_id}\">{obj.get_name()}</a>" + '\n'
            message_text += _("Username: ") + (obj.username if obj.username else _("Unknown")) + '\n'
            message_text += _("Courier: ") + (_("Yes") if obj.courier else _("No")) + '\n'

        else:
            message_text = _("Nobody found") + '\n'

        return message_text

    def add_courier(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        selected_user = self.selected_object(context)

        courier = Courier(user=selected_user, delivery_center=user.operator.delivery_center)
        for permission in ['courier_menu_access',
                           'courier_baskets_to_delivery_menu_access',
                           'allow_courier_take_basket']:

            perm = DBSession.query(Permission).get(permission)
            if perm not in selected_user.permissions:
                selected_user.permissions.append(perm)

        add_to_db([selected_user, courier])

        update.callback_query.answer(text=_("User added to bot as courier"), show_alert=True)
        self.update_objects(context)
        self.send_message(context)

        return self.States.ACTION

    def delete_courier(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        selected_user = self.selected_object(context)
        for permission in ['courier_menu_access',
                           'courier_baskets_to_delivery_menu_access',
                           'allow_courier_take_basket']:

            perm = DBSession.query(Permission).get(permission)
            if perm in selected_user.permissions:
                selected_user.permissions.remove(perm)

        remove_from_db(selected_user.courier)

        update.callback_query.answer(text=_("Courier deleted"), show_alert=True)
        self.update_objects(context)
        self.send_message(context)

        return self.States.ACTION

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        if len(context.user_data[self.menu_name]['users']) > 0:
            message_text = "\n〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<i>" + _("👤 User") + f" ({current_page} " + _("of") + f" {max_page})" + "</i>"
            return message_text
        return ""

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []
        if obj:
            buttons.append([InlineKeyboardButton(_("📗 Add"), callback_data='add_as_carrier')])
            if obj.courier:
                buttons.append([InlineKeyboardButton(_("📕 Delete"), callback_data='delete_carrier')])

        return buttons

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.add_courier, pattern="^add_as_carrier$"),
                                     CallbackQueryHandler(self.delete_courier, pattern="^delete_carrier$")]}
