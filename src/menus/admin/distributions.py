import enum

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command, get_settings, group_buttons
from botmanlib.menus.ready_to_use.instant_distribution import InstantDistributionMenu
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import User, DBSession, City
from src.settings import SETTINGS_FILE


class DistributionsMenu(BaseMenu):
    menu_name = 'distribution_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        if not user.has_permission('distribution_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return self.States.ACTION

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("Instant distribution"), callback_data='instant_distribution')],
                   [InlineKeyboardButton(_("Distribution by city"), callback_data='distribution_by_city')],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]

        send_or_edit(context, chat_id=user.chat_id, text=_("Choose distribution type:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        instant_distribution_menu = BioInstantDistributionMenu(User, self)
        city_distribution_menu = BioInstantDistributionByCityMenu(User, self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^distribution$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               instant_distribution_menu.handler,
                                                               city_distribution_menu.handler,
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler


class BioInstantDistributionMenu(InstantDistributionMenu):
    menu_name = 'instant_distribution'

    def available_buttons(self, context):
        _ = context.user_data['user'].translator
        buttons = [{"data": "start", "name": _("Return user to start menu")}, ]
        return buttons


class BioInstantDistributionByCityMenu(BioInstantDistributionMenu):
    menu_name = 'distribution_by_city'

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        SELECT_CITY = 6

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^distribution_by_city$', pass_user_data=True)]

    def entry(self, update, context):
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []

        self.ask_city(context)
        return self.States.SELECT_CITY

    def ask_city(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        settings = get_settings(SETTINGS_FILE)
        cities = DBSession.query(City).all()

        for city in cities:
            city_name = city.get_translation(user.language_code).name
            if city_name != _("💬 Another City"):
                flat_buttons.append(InlineKeyboardButton(city.get_translation(user.language_code).name, callback_data=f"set_city_{city.id}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select city:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_city(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['city_filter_id'] = data.replace("set_city_", "")
        self.send_message(context)
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Messages will be sent to users who live in selected city. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Message was sent to all bot users who live in selected city except you")

    def users_to_send(self, context):
        return DBSession.query(User) \
            .filter(User.chat_id != context.user_data["user"].chat_id) \
            .filter(User.active == True) \
            .filter(User.city_id == context.user_data[self.menu_name]['city_filter_id']) \
            .all()

    def additional_states(self):
        return {self.States.SELECT_CITY: [CallbackQueryHandler(self.set_city, pattern=r'^set_city_\d+$'),
                                          CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                          MessageHandler(Filters.all, to_state(self.States.SELECT_CITY))]}
