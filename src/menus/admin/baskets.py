from botmanlib.menus import OneListMenu, ArrowAddEditMenu, BunchListMenu
from botmanlib.menus.helpers import add_to_db, group_buttons, require_permission
from botmanlib.messages import delete_interface, send_or_edit, remove_interface
from botmanlib.models import MessageType
from formencode import validators
from sqlalchemy import func
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, CallbackContext

from src.models import DBSession, DeliveryCenter, DeliveryCenterTranslation, Basket, BasketTranslation, Product, ProductBasket


class BasketsMenu(OneListMenu):
    menu_name = 'baskets_menu'

    @require_permission(Basket.view_permission)
    def entry(self, update, context):
        return super(BasketsMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(Basket).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^baskets$')]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        if obj:
            message_text = f"____{_('Baskets: ')}____\n"
            en_translation = obj.get_translation('en')
            message_text += (_("Name") + f" {en_translation.lang.upper()}: {en_translation.name}" + '\n') if en_translation else ""
            message_text += _("Delivery center") + f": {obj.delivery_center.get_translation(user.language_code).name}" + '\n'
            message_text += _("Products: ")
            if obj.products:
                message_text += "\n"
                for idx, product in enumerate(obj.products):
                    message_text += f"{idx + 1}. {product.get_translation(user.language_code).name}\n"
                message_text += "\n"
            else:
                message_text += _("Empty") + "\n"

            message_text += _("Price: ") + f"{obj.price:.2f} " + _("UAH") + "\n"
            available_translation = ','.join([trans.lang.upper() for trans in obj.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"
            if en_translation is None:
                message_text += '  ' + _("There is no translations yet") + '\n'
        else:
            message_text = _("There is no baskets yet") + '\n'

        return message_text

    @require_permission(Basket.delete_permission)
    def delete_ask(self, update, context):
        return super(BasketsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        if user.has_permission(Basket.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_basket"))
        if user.has_permission(Basket.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_basket"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if obj:
            if user.has_permission(BasketTranslation.view_permission):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='basket_translations'))
            if user.has_permission("allow_edit_basket_products"):
                buttons.append(InlineKeyboardButton(_("Products"), callback_data='basket_products'))
            if obj and user.has_permission(Basket.delete_permission):
                buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return group_buttons(buttons, 1)

    def additional_states(self):
        add_basket_menu = BasketAddMenu(self)
        edit_basket_menu = BasketEditMenu(self)
        basket_translations_menu = BasketTranslationMenu(self)
        basket_products = BasketProductsMenu(self)
        return {self.States.ACTION: [add_basket_menu.handler,
                                     edit_basket_menu.handler,
                                     basket_translations_menu.handler,
                                     basket_products.handler,
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Basket deleted")


class BasketAddMenu(ArrowAddEditMenu):
    menu_name = 'basket_add_menu'
    model = dict

    @require_permission(Basket.add_permission)
    def entry(self, update, context):
        return super(BasketAddMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'

        available_delivery_centers = DBSession.query(func.count(DeliveryCenter.id)).scalar()
        if available_delivery_centers <= 0:
            message_text += _("There is no delivery centers yet. Please add one in Admin menu/Delivery centers menu.")
        return message_text

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        delivery_center_translations = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.lang == user.language_code).all()
        delivery_centers_names = [dct.name for dct in delivery_center_translations]

        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('description', _("*Description EN"), validators.String(), required=True),
                  self.Field('short_description', _("*Short description EN"), validators.String(), required=True),
                  self.Field('delivery_center_name', _("*Delivery center"), validators.String(), variants=delivery_centers_names, required=True, switchable=bool(delivery_centers_names)),
                  self.Field('price', _("*Price"), validators.Number(min=0), units=" " + _("UAH"), default=0)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_basket$")]

    def save_object(self, obj, context, session=None):

        basket = Basket()
        basket.price = obj['price']

        translation = BasketTranslation(lang='en')
        translation.name = obj['name']
        translation.description = obj['description']
        translation.short_description = obj['short_description']

        delivery_center_translation = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.name == context.user_data[self.menu_name]['delivery_center_name']).first()
        basket.delivery_center_id = delivery_center_translation.delivery_center_id
        basket.translations.append(translation)

        if not add_to_db([translation, basket], session):
            return self.conv_fallback(context)


class BasketEditMenu(ArrowAddEditMenu):
    menu_name = 'basket_edit_menu'
    model = Basket

    @require_permission(Basket.add_permission)
    def entry(self, update, context):
        return super(BasketEditMenu, self).entry(update, context)

    def query_object(self, context):
        return self.parent.selected_object(context)

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'
        return message_text

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('price', _("*Price"), validators.Number(min=0), units=" " + _("UAH"), default=0)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_basket$")]


class BasketTranslationMenu(OneListMenu):
    menu_name = 'basket_translations_menu'

    @require_permission(BasketTranslation.view_permission)
    def entry(self, update, context):
        return super(BasketTranslationMenu, self).entry(update, context)

    def query_objects(self, context):
        basket = self.parent.selected_object(context)
        if basket:
            return DBSession.query(BasketTranslation).filter(BasketTranslation.basket_id == basket.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^basket_translations$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        if obj:
            message_text = f"____{_('Basket translations')}____\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
            message_text += _("Short description") + f": {obj.short_description}\n"
            message_text += _("Description") + f": {obj.description}\n"
            message_text += _("Image: ") + (_("Loaded") if obj.image else _("Not loaded")) + "\n"
        else:
            message_text = _("There is no translations yet") + '\n'

        return message_text

    @require_permission(BasketTranslation.delete_permission)
    def delete_ask(self, update, context):
        return super(BasketTranslationMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(BasketTranslation.lang).filter(BasketTranslation.basket == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if len(exist_translations) <= 2 and user.has_permission(BasketTranslation.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_basket_trans"))
        if user.has_permission(BasketTranslation.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_basket_trans"))
        if user.has_permission(BasketTranslation.delete_permission) and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        basket_translation_add_edit_menu = BasketTranslationAddEditMenu(self)
        return {self.States.ACTION: [basket_translation_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Translation deleted")


class BasketTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = 'basket_translations_add_edit_menu'
    model = BasketTranslation

    @require_permission(BasketTranslation.add_permission)
    def category_add(self, update, context):
        return super(BasketTranslationAddEditMenu, self).entry(update, context)

    @require_permission(BasketTranslation.edit_permission)
    def category_edit(self, update, context):
        return super(BasketTranslationAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        basket_trans = self.parent.selected_object(context)
        if basket_trans and self.action(context) == self.Action.EDIT:
            return basket_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['user'].translator
        exist_translations = [trans[0].upper() for trans in DBSession.query(BasketTranslation.lang).filter(BasketTranslation.basket == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True),
                   self.Field('short_description', _("*Short description"), validators.String(), required=True),
                   self.Field('description', _("*Description"), validators.String(), required=True),
                   self.Field('image', _("Image"), validators.String(), required=False, message_type=MessageType.photo)]
        return fields

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.basket = self.parent.parent.selected_object(context)
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.category_add, pattern="^add_basket_trans$"),
                CallbackQueryHandler(self.category_edit, pattern="^edit_basket_trans$")]


class BasketProductsMenu(BunchListMenu):
    menu_name = 'basket_products_menu'
    auto_hide_arrows = True
    objects_per_page = 6
    search_key_param = 'name'
    model = Product

    @require_permission("allow_edit_basket_products")
    def entry(self, update, context):
        return super(BasketProductsMenu, self).entry(update, context)

    def query_objects(self, context: CallbackContext):
        return DBSession.query(Product).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^basket_products$")]

    def message_text(self, context, page_objects):
        user = context.user_data['user']
        _ = user.translator
        basket = self.parent.selected_object(context)

        message_text = _("Basket ") + basket.get_translation(user.language_code).name + "\n"
        message_text += _("Added products:") + "\n"
        for idx, product in enumerate(basket.products):
            product_basket_asoc = DBSession.query(ProductBasket).get((product.id, basket.id))
            message_text += str(idx + 1) + ". " + product.get_translation(user.language_code).name + " " + ("(" + _("Editable") + ")" if product_basket_asoc.editable else "") + "\n"

        if not basket.products:
            message_text += _("No products added to basket") + "\n"

        if page_objects is None:
            message_text += "\n<i>" + _("There is no products in bot yet, please add one in Admin menu/Categories & products menu") + "</i>\n"

        message_text += "\n"
        return message_text

    def object_buttons(self, context, objs):
        user = context.user_data['user']
        basket = self.parent.selected_object(context)

        buttons = []
        for product in objs:
            if product in basket.products:
                added_product_buttons = [InlineKeyboardButton(f"✅ {product.id} " + product.get_translation(user.language_code).name, callback_data=f"remove_product_{product.id}")]
                product_basket_asoc = DBSession.query(ProductBasket).get((product.id, basket.id))
                if product_basket_asoc.editable:
                    added_product_buttons.append(InlineKeyboardButton("🔏", callback_data=f"disable_editing_{product.id}"))
                else:
                    added_product_buttons.append(InlineKeyboardButton("🖊", callback_data=f"enable_editing_{product.id}"))

                buttons.append(added_product_buttons)
            else:
                buttons.append([InlineKeyboardButton(f"{product.id} " + product.get_translation(user.language_code).name, callback_data=f"add_product_{product.id}")])

        return buttons

    def add_product(self, update, context):
        basket = self.parent.selected_object(context)
        product_id = int(update.callback_query.data.replace("add_product_", ""))

        product_basket_asoc = ProductBasket(product_id=product_id, basket_id=basket.id)
        if not add_to_db(product_basket_asoc):
            return self.conv_fallback(context)

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def remove_product(self, update, context):
        basket = self.parent.selected_object(context)
        product_id = int(update.callback_query.data.replace("remove_product_", ""))

        product_basket_asoc = DBSession.query(ProductBasket).get((product_id, basket.id))
        DBSession.delete(product_basket_asoc)
        DBSession.commit()

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def disable_editing(self, update, context):
        basket = self.parent.selected_object(context)
        product_id = int(update.callback_query.data.replace("disable_editing_", ""))

        product_basket_asoc = DBSession.query(ProductBasket).get((product_id, basket.id))
        if product_basket_asoc:
            product_basket_asoc.editable = False
            if not add_to_db(product_basket_asoc):
                return self.conv_fallback(context)

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def enable_editing(self, update, context):
        basket = self.parent.selected_object(context)
        product_id = int(update.callback_query.data.replace("enable_editing_", ""))

        product_basket_asoc = DBSession.query(ProductBasket).get((product_id, basket.id))
        if product_basket_asoc:
            product_basket_asoc.editable = True
            if not add_to_db(product_basket_asoc):
                return self.conv_fallback(context)

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def search(self, update, context):
        user_data = context.user_data
        user = user_data['user']
        text = update.message.text

        objects = self.query_objects(context)

        filtered_objects = []
        for obj in objects:
            obj_translation = obj.get_translation(user.language_code)
            if not obj_translation:
                continue

            value = getattr(obj_translation, self.search_key_param, False)
            if value and text.lower() in value.lower():
                filtered_objects.append(obj)

        if filtered_objects:
            user_data[self.menu_name]['filtered_objects'] = filtered_objects
            user_data[self.menu_name]['page'] = 0
            delete_interface(context)
            self.send_message(context)
        else:
            delete_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=self.empty_search_text(context))
            remove_interface(context)
            self.send_message(context)

        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.remove_product, pattern=r"^remove_product_\d+$"),
                                     CallbackQueryHandler(self.add_product, pattern=r"^add_product_\d+$"),
                                     CallbackQueryHandler(self.disable_editing, pattern=r"^disable_editing_\d+$"),
                                     CallbackQueryHandler(self.enable_editing, pattern=r"^enable_editing_\d+$")
                                     ]}
