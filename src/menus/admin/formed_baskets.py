import enum

import pytz
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.menus.basket_products_info import BasketProductsMenu
from src.menus.transfer_menu import TransferMenu
from src.models import DBSession
from src.models import FormedBasket, FormedBasketStatus


class FormedBasketsMenu(OneListMenu):
    menu_name = "admin_formed_baskets_menu"
    disable_web_page_preview = False

    class States(enum.Enum):
        ACTION = 1

    @require_permission("admin_formed_baskets_menu_access")
    def entry(self, update, context):
        self._load(context)
        context.user_data[self.menu_name]['filter'] = "all"

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']

        query = DBSession.query(FormedBasket).filter(FormedBasket.status == FormedBasketStatus.filled)

        if context.user_data[self.menu_name]['filter'] == 'without_delivery':
            query = query.filter(FormedBasket.has_delivery == False)

        return query.all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^formed_baskets$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, formed_basket):
        _ = context.user_data['user'].translator
        buttons = []
        if formed_basket:
            if context.user_data[self.menu_name]['filter'] == 'all':
                buttons.append([InlineKeyboardButton(_("🛅 Without delivery"), callback_data='change_filter')])
            elif context.user_data[self.menu_name]['filter'] == 'without_delivery':
                buttons.append([InlineKeyboardButton(_("⏏️ All"), callback_data='change_filter')])

            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'more_{formed_basket.basket_order.basket.id}')])
        return buttons

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if obj:
            translation = obj.basket_order.basket.get_translation(user.language_code)
            message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("ID:") + f" {obj.id}</b>\n\n"

            adapted_date = pytz.utc.localize(obj.delivery_date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
            message_text += _("🗓️ Date of issue:") + " " + adapted_date.strftime("%d.%m.%Y") + "\n\n"

            message_text += "<b>" + _("👤 Customer:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("ID:") + f" {obj.basket_order.user.chat_id}\n"
            message_text += _("Name:") + f" <a href=\"{obj.basket_order.user.mention_url}\">{obj.basket_order.user.get_name()}</a>\n"
            message_text += _("Phone:") + f" {obj.basket_order.user.phone if obj.basket_order.user.phone else _('Unknown')}\n"
            city = obj.basket_order.user.city.get_translation(user.language_code).name if obj.basket_order.user.city else _('Unknown')
            if obj.basket_order.user.city.get_translation(user.language_code).name == _("💬 Another City"):
                city += f" ({obj.basket_order.user.other_city})"
            message_text += _("City:") + f" {city}\n\n"

        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def change_filter(self, update, context):
        if context.user_data[self.menu_name]['filter'] == 'all':
            context.user_data[self.menu_name]['filter'] = 'without_delivery'
        else:
            context.user_data[self.menu_name]['filter'] = 'all'

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator

        message_text = "〰〰〰〰〰〰〰〰〰〰\n"
        if context.user_data[self.menu_name]['filter'] == 'without_delivery':
            message_text += _("🛅 Carts without delivery") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        elif context.user_data[self.menu_name]['filter'] == 'all':
            message_text += _("⏏️ All carts") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

        return message_text

    def additional_states(self):
        transfer_menu = TransferMenu(self)
        basket_products_menu = BasketProductsMenu(self)
        return {self.States.ACTION: [transfer_menu.handler,
                                     basket_products_menu.handler,
                                     CallbackQueryHandler(self.change_filter, pattern="^change_filter$")]}