from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler, MessageHandler, Filters

from src.models import DBSession, FormedBasket, FormedBasketStatus, ProductFormedBasket


class OrderedProductsMenu(BaseMenu):
    menu_name = "ordered_products_menu"

    @require_permission("ordered_products_menu_access")
    def entry(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        """----------------FILLED-------------------"""
        formed_baskets = DBSession.query(FormedBasket) \
            .filter(FormedBasket.status == FormedBasketStatus.filled) \
            .all()

        products = {}
        for basket in formed_baskets:
            for product in basket.products:
                product_formed_basket_asoc = DBSession.query(ProductFormedBasket).get((product.id, basket.id))
                product_name = product.get_translation(user.language_code).name
                if product_name not in products:
                    products.update({product_name: product_formed_basket_asoc.quantity})
                else:
                    products[product_name] += product_formed_basket_asoc.quantity

        message_text = _("Filled products:") + "\n"
        for product_name, quantity in products.items():
            message_text += f"- {product_name} {quantity}\n"

        """----------------TO FILL-------------------"""
        formed_baskets = DBSession.query(FormedBasket) \
            .filter(FormedBasket.status == FormedBasketStatus.new) \
            .all()

        products = {}
        for basket in formed_baskets:
            for product in basket.products:
                product_formed_basket_asoc = DBSession.query(ProductFormedBasket).get((product.id, basket.id))
                product_name = product.get_translation(user.language_code).name
                if product_name not in products:
                    products.update({product_name: product_formed_basket_asoc.quantity})
                else:
                    products[product_name] += product_formed_basket_asoc.quantity

        message_text += "\n"
        message_text += _("To fill products:") + "\n"
        for product_name, quantity in products.items():
            message_text += f"- {product_name} {quantity}\n"



        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}')]]
        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def back(self, update, context):
        context.user_data[self.menu_name]['basket_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^ordered_products$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'), ],
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
