from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons, require_permission
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.jobs import start_close_delivery_center_job
from src.models import DBSession, DeliveryCenter, DeliveryCenterTranslation, City, CityTranslation


class DeliveryCentersMenu(OneListMenu):
    menu_name = 'delivery_centers_menu'

    @require_permission(DeliveryCenter.view_permission)
    def entry(self, update, context):
        return super(DeliveryCentersMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(DeliveryCenter).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^delivery_centers$')]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        if obj:
            message_text = f"____{_('Delivery centers: ')}____\n"
            en_translation = obj.get_translation('en')
            message_text += _("Name") + (f" {en_translation.lang.upper()}: {en_translation.name}" + '\n' if en_translation else ": " + _("Unknown")) + "\n"
            message_text += _("City") + f": {obj.city.get_translation(user.language_code).name}" + '\n'
            message_text += _("Address") + (f" {en_translation.lang.upper()}: {en_translation.address}" if en_translation else ": " + _("Unknown")) + "\n"
            message_text += _("Work time") + (f": {obj.open_time.strftime('%H:%M')}-{obj.close_time.strftime('%H:%M')}" if obj.open_time and obj.close_time else ": " + _("Unknown")) + "\n"
            available_translation = ','.join([trans.lang.upper() for trans in obj.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"
            if en_translation is None:
                message_text += '  ' + _("There is no translations yet") + '\n'
        else:
            message_text = _("There is no delivery centers yet") + '\n'

        return message_text

    @require_permission(DeliveryCenter.delete_permission)
    def delete_ask(self, update, context):
        return super(DeliveryCentersMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []

        if o and user.has_permission(DeliveryCenter.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_delivery_center"))

        if o and user.has_permission(DeliveryCenter.delete_permission):
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if user.has_permission(DeliveryCenter.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_delivery_center"))
        if obj:
            if user.has_permission(DeliveryCenterTranslation.view_permission):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='delivery_center_translations'))
        return group_buttons(buttons, 1)

    def additional_states(self):
        add_delivery_centers = DeliveryCenterAddMenu(self)
        edit_delivery_centers = DeliveryCenterEditMenu(self)

        delivery_center_translations_menu = DeliveryCenterTranslationsMenu(self)
        return {self.States.ACTION: [add_delivery_centers.handler,
                                     edit_delivery_centers.handler,
                                     delivery_center_translations_menu.handler,
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Category deleted")


class DeliveryCenterAddMenu(ArrowAddEditMenu):
    menu_name = 'delivery_center_add_menu'
    model = DeliveryCenter

    @require_permission(DeliveryCenter.add_permission)
    def entry(self, update, context):
        return super(DeliveryCenterAddMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_cities = DBSession.query(City).all()
        all_cities_translation_names = [city.get_translation(user.language_code).name for city in all_cities]

        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('city_name', _("*City"), validator=validators.String(), required=True, variants=all_cities_translation_names),
                  self.Field('address', _("Address EN"), validators.String()),
                  self.Field('open_time', _("Open time"), validators.TimeConverter(use_datetime=True)),
                  self.Field('close_time', _("Close time"), validators.TimeConverter(use_datetime=True))]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_delivery_center$")]

    def save_object(self, obj, context, session=None):
        translation = DeliveryCenterTranslation(lang='en')
        translation.name = context.user_data[self.menu_name]['name']
        translation.address = context.user_data[self.menu_name]['address']

        city_translation = DBSession.query(CityTranslation).filter(CityTranslation.name == context.user_data[self.menu_name]['city_name']).first()
        obj.city_id = city_translation.city_id

        obj.translations.append(translation)

        if not add_to_db([translation, obj], session):
            return self.conv_fallback(context)
        start_close_delivery_center_job(context.job_queue)


class DeliveryCenterEditMenu(ArrowAddEditMenu):
    menu_name = 'delivery_center_edit_menu'
    model = DeliveryCenter
    force_action = ArrowAddEditMenu.Action.EDIT

    @require_permission(DeliveryCenter.add_permission)
    def entry(self, update, context):
        return super(DeliveryCenterEditMenu, self).entry(update, context)

    def query_object(self, context):
        return self.parent.selected_object(context)

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('open_time', _("Open time"), validators.TimeConverter(use_datetime=True)),
                  self.Field('close_time', _("Close time"), validators.TimeConverter(use_datetime=True))]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_delivery_center$")]

    def save_object(self, obj, context, session=None):
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

        start_close_delivery_center_job(context.job_queue)


class DeliveryCenterTranslationsMenu(OneListMenu):
    menu_name = 'delivery_center_translations_menu'

    @require_permission(DeliveryCenterTranslation.view_permission)
    def entry(self, update, context):
        return super(DeliveryCenterTranslationsMenu, self).entry(update, context)

    def query_objects(self, context):
        delivery_center = self.parent.selected_object(context)
        if delivery_center:
            return DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.delivery_center_id == delivery_center.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^delivery_center_translations$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        if obj:
            message_text = f"____{_('Delivery center translations')}____\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
            message_text += _("Address") + f": {obj.address}\n"
        else:
            message_text = _("There is no translations yet") + '\n'

        return message_text

    @require_permission(DeliveryCenterTranslation.delete_permission)
    def delete_ask(self, update, context):
        return super(DeliveryCenterTranslationsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(DeliveryCenterTranslation.lang).filter(DeliveryCenterTranslation.delivery_center == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if len(exist_translations) <= 2 and user.has_permission(DeliveryCenterTranslation.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_delivery_center_trans"))
        if user.has_permission(DeliveryCenterTranslation.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_delivery_center_trans"))
        if user.has_permission(DeliveryCenterTranslation.delete_permission) and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        delivery_center_translation_add_edit_menu = DeliveryCenterTranslationAddEditMenu(self)
        return {self.States.ACTION: [delivery_center_translation_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Translation deleted")


class DeliveryCenterTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = 'delivery_center_translations_add_edit_menu'
    model = DeliveryCenterTranslation

    @require_permission(DeliveryCenterTranslation.add_permission)
    def category_add(self, update, context):
        return super(DeliveryCenterTranslationAddEditMenu, self).entry(update, context)

    @require_permission(DeliveryCenterTranslation.edit_permission)
    def category_edit(self, update, context):
        return super(DeliveryCenterTranslationAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        ct_trans = self.parent.selected_object(context)
        if ct_trans and self.action(context) == self.Action.EDIT:
            return ct_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['user'].translator
        exist_translations = [trans[0].upper() for trans in DBSession.query(DeliveryCenterTranslation.lang).filter(DeliveryCenterTranslation.delivery_center == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True),
                   self.Field('address', _("Address"), validators.String())]
        return fields

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.delivery_center = self.parent.parent.selected_object(context)
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.category_add, pattern="^add_delivery_center_trans$"),
                CallbackQueryHandler(self.category_edit, pattern="^edit_delivery_center_trans$")]
