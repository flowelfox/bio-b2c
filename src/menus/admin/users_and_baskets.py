from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission
from telegram.ext import CallbackQueryHandler

from src.models import User, DBSession


class UsersInfoMenu(OneListMenu):
    menu_name = 'users_info_menu'
    model = User
    search_key_param = 'desired_name'

    @require_permission("users_info_menu_access")
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator

        context.user_data[self.menu_name]['only_with_queries'] = False
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        query = DBSession.query(User).filter(User.is_active == True)
        if context.user_data[self.menu_name]['only_with_queries']:
            query = query.join(User.queries)
        return query.order_by(User.join_date.desc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^users_info$")]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        if obj:
            message_text = f"____{_('Users')}____\n"
            message_text += _("User Telegram ID") + f": {obj.chat_id}\n"
            message_text += _("First Name") + f": {obj.first_name if obj.first_name else _('Unknown')}" + '\n'
            message_text += _("Last Name") + f": {obj.last_name if obj.last_name else _('Unknown')}" + '\n'
            message_text += _("Desired name") + f": {obj.desired_name if obj.desired_name else _('Unknown')}" + '\n'
            message_text += _("Username") + f": {'@' + obj.username if obj.username else _('Unknown')}" + '\n'
            message_text += _("Phone") + f": {obj.phone if obj.phone else _('Unknown')}" + '\n'
            message_text += _("Email") + f": {obj.email if obj.email else _('Unknown')}" + '\n'
            message_text += _("City") + f": {obj.city.get_translation(user.language_code).name if obj.city else _('Unknown')}\n"
            message_text += _("Other city") + f": {obj.other_city if obj.other_city else _('Not set')}\n"
            message_text += _("Came from") + f": {obj.came_from if obj.came_from else _('Unknown')}" + '\n'
            message_text += _("User language") + f": {obj.language_code.upper() if obj.language_code else _('Unknown')}" + '\n'
        else:
            message_text = _("There is no users yet") + '\n'

        return message_text

    def center_buttons(self, context, o=None):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        # if obj:
        #     if user.has_permission('user_tickets_menu_access'):
        #         buttons.append(InlineKeyboardButton(_("Queries"), callback_data="user_queries_info"))
        return buttons

    # def object_buttons(self, context, obj):
    #     user = context.user_data['user']
    #     _ = user.translator
    #     buttons = []
    #
    #     if context.user_data[self.menu_name]['only_with_queries']:
    #         buttons.append(InlineKeyboardButton(_("Show without queries"), callback_data='only_with_queries_switch'))
    #     else:
    #         buttons.append(InlineKeyboardButton(_("Show only with queries"), callback_data='only_with_queries_switch'))
    #
    #     return group_buttons(buttons, 1)

    def switch_only_with_tickets(self, update, context):
        context.user_data[self.menu_name]['only_with_queries'] = not context.user_data[self.menu_name]['only_with_queries']
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        queries_info_menu = QueriesInfoMenu(self)
        return {self.States.ACTION: [queries_info_menu.handler,
                                     CallbackQueryHandler(self.switch_only_with_tickets, pattern='only_with_queries_switch'),
                                     ]}

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        return "_______" + _("User") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"


class QueriesInfoMenu(OneListMenu):
    menu_name = 'queries_info_menu'

    @require_permission("user_queries_menu_access")
    def entry(self, update, context):
        return super(QueriesInfoMenu, self).entry(update, context)

    def query_objects(self, context):
        query = DBSession.query(DataQuery).join(DataQuery.user)
        query = query.filter(DataQuery.user == self.parent.selected_object(context))
        return query.order_by(DataQuery.create_date.desc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^user_queries_info$")]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        if obj:
            message_text = full_details_text(user,
                                             obj.subcategory,
                                             obj.product,
                                             obj.country,
                                             obj.region,
                                             obj.currency)
        else:
            message_text = _("User has no queries") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        return "_______" + _("Query") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"
