from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons, require_permission
from botmanlib.models import MessageType
from formencode import validators
from sqlalchemy import func
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.menus.admin.products_csv_loader import ProductCSVLoaderMenu
from src.models import Product, ProductTranslation, DBSession, Manufacturer, ManufacturerTranslation


class ProductsMenu(OneListMenu):
    menu_name = 'product_menu'

    @require_permission(Product.view_permission)
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Product).filter(Product.category_id == self.parent.selected_object(context).id).order_by(Product.id.asc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^products$')]

    def message_text(self, context, product):
        user = context.user_data['user']
        _ = user.translator

        if product:
            message_text = f"____{_('Products: ')}____\n"
            translation = product.get_translation(user.language_code)
            manufacturer_translation = product.manufacturer.get_translation(user.language_code) if product.manufacturer else None
            message_text += (_("Name") + f" {translation.lang.upper()}: {translation.name}" + '\n') if translation else "Unknown"
            message_text += _("ID") + f": {product.id}\n"
            message_text += _("Description") + f" {translation.lang.upper()}: {translation.description if translation.description else _('Unknown')}" + '\n'
            message_text += _("Short description") + f" {translation.lang.upper()}: {translation.short_description if translation.short_description else _('Unknown')}" + '\n'
            message_text += _("Manufacturer") + f": {manufacturer_translation.name if manufacturer_translation else _('Unknown')}" + '\n'
            message_text += _("Package") + f": {product.packaging if product.packaging else _('Unknown')} {translation.packaging_unit if translation.packaging_unit else ''}\n"
            message_text += _("Price") + f": {str(product.price) + ' ' + _('UAH') if product.price else _('Unknown')}" + '\n'
            message_text += (_("Image") + f" {translation.lang.upper()}: {_('Loaded') if translation.image else _('Not loaded')}" + '\n') if translation else ""
            available_translation = ','.join([trans.lang.upper() for trans in product.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"
            if translation is None:
                message_text += '  ' + _("There is no translations yet") + '\n'
        else:
            message_text = _("There is no products yet") + '\n'
        return message_text

    @require_permission(Product.delete_permission)
    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        return super(ProductsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        buttons = []
        user = context.user_data['user']
        if user.has_permission(Product.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_product"))
        if o and user.has_permission(Product.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_product"))
        if o and user.has_permission(Product.delete_permission):
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def object_buttons(self, context, obj):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        if obj:
            if user.has_permission(ProductTranslation.view_permission):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='product_translations'))
        if user.has_permission(Product.import_permission):
            buttons.append(InlineKeyboardButton(_("Import from CSV"), callback_data='import_csv'))

        return group_buttons(buttons, 1)

    def additional_states(self):
        product_add_menu = ProductAddMenu(self)
        product_edit_menu = ProductEditMenu(self)
        product_translation_menu = ProductTranslationMenu(self)
        import_csv = ProductCSVLoaderMenu(self)
        return {self.States.ACTION: [product_add_menu.handler,
                                     product_edit_menu.handler,
                                     product_translation_menu.handler,
                                     import_csv.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Product deleted")

    def back(self, update, context):
        if update.callback_query:
            update.callback_query.answer()

        self.parent.send_message(context)
        return ConversationHandler.END


class ProductAddMenu(ArrowAddEditMenu):
    menu_name = 'product_add_menu'
    model = Product

    @require_permission(Product.add_permission)
    def entry(self, update, context):
        self._entry(update, context)
        _ = context.user_data['user'].translator
        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'

        available_manufacturers = DBSession.query(func.count(Manufacturer.id)).scalar()
        if available_manufacturers <= 0:
            message_text += _("There is no manufacturers yet. Please add one in Admin menu/Manufacturers menu.")
        return message_text

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_manufacturers = DBSession.query(Manufacturer).all()
        all_manufacturers_translation_names = [manufacturer.get_translation(user.language_code).name for manufacturer in all_manufacturers]

        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('description', _("Description EN"), validators.String()),
                  self.Field('short_description', _("Short description EN"), validators.String()),
                  self.Field('manufacturer_name', _("Manufacturer"), validators.String(), variants=all_manufacturers_translation_names, switchable=bool(all_manufacturers_translation_names)),
                  self.Field('packaging', _("Packaging"), validators.String()),
                  self.Field('packaging_unit', _("Packaging unit EN"), validators.String()),
                  self.Field('price', _("*Price"), validators.Number(), required=True, default=0, units=" " + _("UAH")),
                  self.Field('image', _("Image EN"), validators.String(), message_type=MessageType.photo)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_product$")]

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        translation = ProductTranslation(lang='en')
        translation.name = user_data[self.menu_name]['name']
        translation.description = user_data[self.menu_name]['description']
        translation.short_description = user_data[self.menu_name]['short_description']
        translation.packaging_unit = user_data[self.menu_name]['packaging_unit']

        obj.price = user_data[self.menu_name]['price']
        obj.packaging = user_data[self.menu_name]['packaging']
        translation.image = user_data[self.menu_name]['image']
        obj.translations.append(translation)
        obj.category = self.parent.parent.selected_object(context)

        manufacturer_translation = DBSession.query(ManufacturerTranslation).filter(ManufacturerTranslation.name == user_data[self.menu_name]['manufacturer_name']).first()
        if manufacturer_translation:
            obj.manufacturer_id = manufacturer_translation.manufacturer_id

        if not add_to_db([translation, obj], session):
            return self.conv_fallback(context)


class ProductEditMenu(ArrowAddEditMenu):
    menu_name = 'product_edit_menu'
    model = Product

    @require_permission(Product.edit_permission)
    def entry(self, update, context):
        self._entry(update, context)
        _ = context.user_data['user'].translator
        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):

        product = self.parent.selected_object(context)
        if product:
            return DBSession.query(Product).filter(Product.id == product.id).first()
        else:
            self.parent.update_objects(context)
            self.parent.send_message(context)
            return ConversationHandler.END

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'

        available_manufacturers = DBSession.query(func.count(Manufacturer.id)).scalar()
        if available_manufacturers <= 0:
            message_text += _("There is no manufacturers yet. Please add one in Admin menu/Manufacturers menu.")
        return message_text

    def load(self, context):
        user = context.user_data['user']
        _ = user.translator

        super(ProductEditMenu, self).load(context)
        product = self.query_object(context)

        context.user_data[self.menu_name]['manufacturer_name'] = product.manufacturer.get_translation(user.language_code).name if product.manufacturer else None

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        all_manufacturers = DBSession.query(Manufacturer).all()
        all_manufacturers_translation_names = [manufacturer.get_translation(user.language_code).name for manufacturer in all_manufacturers]

        fields = [self.Field('price', _("Price"), validators.Number(), required=True, default=0, units=" " + _("UAH")),
                  self.Field('manufacturer_name', _("Manufacturer"), validators.String(), variants=all_manufacturers_translation_names, switchable=bool(all_manufacturers_translation_names)),
                  self.Field('packaging', _("Packaging"), validators.String(), required=False)]
        return fields

    def save_object(self, obj, context, session=None):

        manufacturer_translation = DBSession.query(ManufacturerTranslation).filter(ManufacturerTranslation.name == context.user_data[self.menu_name]['manufacturer_name']).first()
        obj.manufacturer_id = manufacturer_translation.manufacturer_id

        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^edit_product$')]


class ProductTranslationMenu(OneListMenu):
    menu_name = 'product_translation_menu'

    @require_permission(ProductTranslation.view_permission)
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        product = self.parent.selected_object(context)
        if product:
            return DBSession.query(ProductTranslation).filter(ProductTranslation.product_id == product.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^product_translations$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator
        if obj:
            message_text = f"____{_('Product Translations: ')}____\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name if obj.name else _('Unknown')}\n"
            message_text += _("Description") + f": {obj.description if obj.description else _('Unknown')}\n"
            message_text += _("Short description") + f": {obj.short_description if obj.short_description else _('Unknown')}\n"
            message_text += _("Package") + f": {obj.packaging_unit if obj.packaging_unit else _('Unknown')}\n"
            message_text += _("Image") + f": {_('Loaded') if obj.image else _('Not loaded')}" + '\n'

        else:
            message_text = _("There is no translations yet") + '\n'

        return message_text

    @require_permission(ProductTranslation.delete_permission)
    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        return super(ProductTranslationMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(ProductTranslation.lang).filter(ProductTranslation.product == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if len(exist_translations) <= 2 and user.has_permission(ProductTranslation.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_product_trans"))
        if user.has_permission(ProductTranslation.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_product_trans"))
        if user.has_permission(ProductTranslation.delete_permission) and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        product_translation_add_edit_menu = ProductTranslationAddEditMenu(self)
        return {self.States.ACTION: [product_translation_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Translation deleted")


class ProductTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = 'product_translation_add_edit_menu'
    model = ProductTranslation

    @require_permission(ProductTranslation.add_permission)
    def translation_add(self, update, context):
        return super(ProductTranslationAddEditMenu, self).entry(update, context)

    @require_permission(ProductTranslation.edit_permission)
    def translation_edit(self, update, context):
        return super(ProductTranslationAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        product_trans = self.parent.selected_object(context)
        if product_trans and self.action(context) == self.Action.EDIT:
            return product_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['user'].translator
        exist_translations = [trans[0].upper() for trans in DBSession.query(ProductTranslation.lang).filter(ProductTranslation.product == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True),
                   self.Field('description', _("Description"), validators.String()),
                   self.Field('short_description', _("Short description"), validators.String()),
                   self.Field('manufacturer', _("Manufacturer"), validators.String()),
                   self.Field('packaging_unit', _("Packaging unit"), validators.String()),
                   self.Field('image', _("Image "), validators.String(), message_type=MessageType.photo)
                   ]
        return fields

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.product = self.parent.parent.selected_object(context)
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.translation_add, pattern="^add_product_trans$"),
                CallbackQueryHandler(self.translation_edit, pattern="^edit_product_trans$")]
