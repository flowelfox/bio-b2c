from botmanlib.menus.ready_to_use.csv_loader import CSVLoaderMenu
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.models import ImportSession, Category, CategoryTranslation, DeliveryCenterTranslation


def delivery_center_validate(column, line, data):
    errors = []
    warnings = []

    delivery_centers = ImportSession.query(DeliveryCenterTranslation).all()

    for dc in delivery_centers:
        if dc.name == data:
            return errors, warnings, dc.delivery_center_id

    errors.append(CSVLoaderMenu.ColumnError(line, column.param, "Delivery center does not exists"))
    return errors, warnings, None


def parent_category_validate(column, line, data):
    errors = []
    warnings = []

    category_translations = ImportSession.query(CategoryTranslation).all()

    for c in category_translations:
        if c.name == data:
            return errors, warnings, c.category_id

    warnings.append(CSVLoaderMenu.ColumnError(line, column.param, "Category does not exists"))
    return errors, warnings, None


class CategoryCSVLoaderMenu(CSVLoaderMenu):
    menu_name = 'category_csv_loader_menu'
    model = Category
    translation_model = CategoryTranslation
    translation_model_foreign_key = 'category_id'
    session = ImportSession

    def columns(self):
        return [CSVLoaderMenu.Column("code", ["Code", "Код", "#"], validators.String(), required=True),
                CSVLoaderMenu.Column("name", ["Name", "Name UA", "Name RU", "Name EN"], validators.String(), required=True, multilang=True),
                CSVLoaderMenu.Column("delivery_center_id", ["Delivery center", "Центр доставки"], validators.String(), required=True, after_validate=delivery_center_validate),
                CSVLoaderMenu.Column("parent_category_id", ["Parent category", "Родительская категория"], validators.String(), required=False, after_validate=parent_category_validate),
                ]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^import_csv$')]

    def query_object(self, row_data):
        return self.session.query(Category).filter(Category.code == row_data['code']).first()

    def query_translation(self, obj, lang):
        return self.session.query(CategoryTranslation).filter(CategoryTranslation.category_id == obj.id).filter(CategoryTranslation.lang == lang.lower()).first()

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data='back_from_csv')