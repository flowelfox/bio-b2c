from botmanlib.menus.ready_to_use.csv_loader import CSVLoaderMenu
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.models import ImportSession, Manufacturer, ManufacturerTranslation


class ManufacturerCSVLoaderMenu(CSVLoaderMenu):
    menu_name = 'manufacturer_csv_loader_menu'
    model = Manufacturer
    translation_model = ManufacturerTranslation
    translation_model_foreign_key = 'category_id'
    session = ImportSession

    def columns(self):
        return [CSVLoaderMenu.Column("id", ["№", "ид", "#"], validators.String(), required=True),
                CSVLoaderMenu.Column("name", ["Name", "Компанія UA", "Компанія RU", "Компанія EN"], validators.String(), required=True, multilang=True),
                CSVLoaderMenu.Column("site", ["Site", "Сайт"], validators.String(), required=False),
                CSVLoaderMenu.Column("tm", ["TM", "ТМ"], validators.String(), required=False)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^import_csv$')]

    def query_object(self, row_data):
        return self.session.query(Manufacturer).get(row_data['id'])

    def query_translation(self, obj, lang):
        return self.session.query(ManufacturerTranslation).filter(ManufacturerTranslation.manufacturer_id == obj.id).filter(ManufacturerTranslation.lang == lang.lower()).first()

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data='back_from_csv')