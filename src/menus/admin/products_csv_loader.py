from botmanlib.menus.ready_to_use.csv_loader import CSVLoaderMenu
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.models import ImportSession, ProductTranslation, Product, CategoryTranslation, ManufacturerTranslation


def category_validate(column, line, data):
    errors = []
    warnings = []
    categories = ImportSession.query(CategoryTranslation).all()

    for ct in categories:
        if ct.name == data:
            return errors, warnings, ct.category_id

    errors.append(CSVLoaderMenu.ColumnError(line, column.param, "Category does not exists"))
    return errors, warnings, None


def manufacturer_validate(column, line, data):
    errors = []
    warnings = []

    manufacturers = ImportSession.query(ManufacturerTranslation).all()

    for mf in manufacturers:
        if mf.name == data:
            return errors, warnings, mf.manufacturer_id

    return errors, warnings, None


class ProductCSVLoaderMenu(CSVLoaderMenu):
    menu_name = 'product_csv_loader_menu'
    model = Product
    translation_model = ProductTranslation
    translation_model_foreign_key = 'product_id'
    session = ImportSession

    def columns(self):
        return [CSVLoaderMenu.Column("id", ["id", "ид", "#"], validators.Int(min=1), required=True),
                CSVLoaderMenu.Column("category_id", ["Category", "Категорія"], validators.String(), required=True, after_validate=category_validate),
                CSVLoaderMenu.Column("price", ["Price", "Роздрібне Вартість", "Вартість"], validators.Int(min=1)),
                CSVLoaderMenu.Column("name", ["Name", "Назва продукту УКР", "Назва продукту РУС", "Назва продукту ENG", "Назва продукту УКР (Origin)", "Назва продукту РУС (Origin)", "Назва продукту ЕNG (Origin"], validators.String(), required=True, multilang=True),
                CSVLoaderMenu.Column("description", ["Description", "Опис продукту УКР", "Опис продукту РУС", "Опис продукту ENG"], validators.String(), multilang=True),
                CSVLoaderMenu.Column("short_description", ["Short description", "Коротий опис продукту УКР", "Коротий опис продукту РУС", "Коротий опис продукту ENG"], validators.String(), multilang=True),
                CSVLoaderMenu.Column("manufacturer_id", ["Manufacturer", "Виробник"], validators.String(), required=False, after_validate=manufacturer_validate),
                CSVLoaderMenu.Column("packaging", ["Weight", "Роздрібне Пакування (вага)"], validators.Number(min=0)),
                CSVLoaderMenu.Column("packaging_unit", ["Weight Unit", "Роздрібне Од. виміру РУС", "Роздрібне Од. виміру УКР", "Роздрібне Од. виміру ENG"], validators.String(), multilang=True),
                CSVLoaderMenu.Column("image", ["Image", "Фото (лінк) ENG", "Фото (лінк) РУС", "Фото (лінк) УКР"], validators.URL(), multilang=True, is_image=True)]

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^import_csv$')]

    def query_object(self, row_data):
        return self.session.query(Product).get(row_data['id'])

    def query_translation(self, obj, lang):
        return self.session.query(ProductTranslation).filter(ProductTranslation.product_id == obj.id).filter(ProductTranslation.lang == lang.lower()).first()

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data='back_from_csv')
