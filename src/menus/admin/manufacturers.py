from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons, require_permission
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.menus.admin.manufacturers_csv_loader import ManufacturerCSVLoaderMenu
from src.models import DBSession, Manufacturer, ManufacturerTranslation


class ManufacturersMenu(OneListMenu):
    menu_name = 'manufacturers_menu'

    @require_permission(Manufacturer.view_permission)
    def entry(self, update, context):
        return super(ManufacturersMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(Manufacturer).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^manufacturers$')]

    def message_text(self, context, manufacturer):
        user = context.user_data['user']
        _ = user.translator

        if manufacturer:
            message_text = f"____{_('Manufacturers: ')}____\n"
            en_translation = manufacturer.get_translation('en')
            message_text += _("Name") + f" {en_translation.lang.upper()}: {en_translation.name if en_translation.name else _('Unknown')}\n"
            message_text += _("Site") + f": {manufacturer.site if manufacturer.site else _('Unknown')}\n"
            message_text += _("TM") + f": {manufacturer.tm if manufacturer.tm else _('Unknown')}\n"
            available_translation = ','.join([trans.lang.upper() for trans in manufacturer.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"
            if en_translation is None:
                message_text += '  ' + _("There is no translations yet") + '\n'
        else:
            message_text = _("There is no manufacturers yet") + '\n'

        return message_text

    @require_permission(Manufacturer.delete_permission)
    def delete_ask(self, update, context):
        return super(ManufacturersMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        if user.has_permission(Manufacturer.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_manufacturer"))
        if user.has_permission(Manufacturer.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_manufacturer"))
        if o and user.has_permission(Manufacturer.delete_permission):
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if obj:
            if user.has_permission(ManufacturerTranslation.view_permission):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='manufacturer_translations'))
        if user.has_permission(Manufacturer.import_permission):
            buttons.append(InlineKeyboardButton(_("Import from CSV"), callback_data='import_csv'))
        return group_buttons(buttons, 1)

    def additional_states(self):
        add_manufacturer = ManufacturerAddMenu(self)
        edit_manufacturer = ManufacturerEditMenu(self)
        manufacturer_translations_menu = ManufacturerTranslationsMenu(self)
        manufacturer_csv_load_menu = ManufacturerCSVLoaderMenu(self)
        return {self.States.ACTION: [add_manufacturer.handler,
                                     edit_manufacturer.handler,
                                     manufacturer_translations_menu.handler,
                                     manufacturer_csv_load_menu.handler,
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Manufacturer deleted")


class ManufacturerAddMenu(ArrowAddEditMenu):
    menu_name = 'manufacturer_add_menu'
    model = dict

    @require_permission(Manufacturer.add_permission)
    def entry(self, update, context):
        return super(ManufacturerAddMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('site', _("Site"), validators.String(), required=False),
                  self.Field('tm', _("TM"), validators.String(), required=False)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_manufacturer$")]

    def save_object(self, obj, context, session=None):
        manufacturer = Manufacturer()
        manufacturer.site = obj['site']
        manufacturer.tm = obj['tm']

        translation = ManufacturerTranslation(lang='en')
        translation.name = obj['name']

        manufacturer.translations.append(translation)

        if not add_to_db([translation, manufacturer], session):
            return self.conv_fallback(context)


class ManufacturerEditMenu(ArrowAddEditMenu):
    menu_name = 'manufacturer_edit_menu'
    model = Manufacturer

    @require_permission(Manufacturer.edit_permission)
    def entry(self, update, context):
        return super(ManufacturerEditMenu, self).entry(update, context)

    def query_object(self, context):
        return self.parent.selected_object(context)

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('site', _("Site"), validators.String(), required=False),
                  self.Field('tm', _("TM"), validators.String(), required=False)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_manufacturer$")]

    def save_object(self, obj, context, session=None):
        if not add_to_db([obj], session):
            return self.conv_fallback(context)


class ManufacturerTranslationsMenu(OneListMenu):
    menu_name = 'manufacturer_translations_menu'

    @require_permission(ManufacturerTranslation.view_permission)
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        manufacturer = self.parent.selected_object(context)
        if manufacturer:
            return DBSession.query(ManufacturerTranslation).filter(ManufacturerTranslation.manufacturer_id == manufacturer.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^manufacturer_translations$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        if obj:
            message_text = f"____{_('Manufacturers translations')}____\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
        else:
            message_text = _("There is no translations yet") + '\n'

        return message_text

    @require_permission(ManufacturerTranslation.delete_permission)
    def delete_ask(self, update, context):
        _ = context.user_data['user'].translator
        return super(ManufacturerTranslationsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(ManufacturerTranslation.lang).filter(ManufacturerTranslation.manufacturer == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if len(exist_translations) <= 2 and user.has_permission(ManufacturerTranslation.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_manufacturer_trans"))
        if user.has_permission(ManufacturerTranslation.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_manufacturer_trans"))
        if user.has_permission(ManufacturerTranslation.delete_permission) and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        manufacturer_translation_add_edit_menu = ManufacturerTranslationAddEditMenu(self)
        return {self.States.ACTION: [manufacturer_translation_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Translation deleted")


class ManufacturerTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = 'manufacturer_translation_add_edit_menu'
    model = ManufacturerTranslation

    @require_permission(ManufacturerTranslation.add_permission)
    def manufacturer_add(self, update, context):
        return super(ManufacturerTranslationAddEditMenu, self).entry(update, context)

    @require_permission(ManufacturerTranslation.edit_permission)
    def manufacturer_edit(self, update, context):
        return super(ManufacturerTranslationAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        ct_trans = self.parent.selected_object(context)
        if ct_trans and self.action(context) == self.Action.EDIT:
            return ct_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['user'].translator
        exist_translations = [trans[0].upper() for trans in DBSession.query(ManufacturerTranslation.lang).filter(ManufacturerTranslation.manufacturer == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True)]
        return fields

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.manufacturer = self.parent.parent.selected_object(context)
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.manufacturer_add, pattern="^add_manufacturer_trans$"),
                CallbackQueryHandler(self.manufacturer_edit, pattern="^edit_manufacturer_trans$")]
