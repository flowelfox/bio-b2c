import datetime
from io import BytesIO

import pytz
import qrcode
from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons, require_permission
from botmanlib.messages import send_or_edit
from formencode import validators
from openpyxl import Workbook
from openpyxl.drawing.image import Image
from openpyxl.styles import Alignment
from sqlalchemy import func
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from src.menus.admin.categories_csv_loader import CategoryCSVLoaderMenu
from src.menus.admin.products import ProductsMenu
from src.models import Category, CategoryTranslation, DBSession, Product, DeliveryCenterTranslation, DeliveryCenter


class ProductCategoriesMenu(OneListMenu):
    menu_name = 'categories_menu'

    @require_permission(Category.view_permission)
    def entry(self, update, context):
        return super(ProductCategoriesMenu, self).entry(update, context)

    def query_objects(self, context):
        return DBSession.query(Category).order_by(Category.id.asc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^categories$')]

    def message_text(self, context, category):
        user = context.user_data['user']
        _ = user.translator

        if category:
            message_text = f"____{_('Product categories: ')}____\n"
            en_translation = category.get_translation('en')
            message_text += (_("Name") + f" {en_translation.lang.upper()}: {en_translation.name}" + '\n') if en_translation else ""
            if category.parent_category:
                message_text += _("Parent category:") + category.parent_category.get_translation(user.language_code).name + "\n"
            if category.categories:
                message_text += _("Subcategories:") + "\n"
                for cat in category.categories:
                    message_text += f"- {cat.get_translation(user.language_code).name}\n"
            message_text += "\n"
            message_text += _("Products in category:") + f" {len(category.products)}\n"

            message_text += (_("Delivery center") + f": {category.delivery_center.get_translation(user.language_code).name}" + '\n')
            available_translation = ','.join([trans.lang.upper() for trans in category.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"
            if en_translation is None:
                message_text += '  ' + _("There is no translations yet") + '\n'
        else:
            message_text = _("There is no categories yet") + '\n'

        return message_text

    @require_permission(Category.delete_permission)
    def delete_ask(self, update, context):
        return super(ProductCategoriesMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        if user.has_permission(Category.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_category"))
        if user.has_permission(Category.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_category"))

        if o and user.has_permission(Category.delete_permission):
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        if obj:
            if user.has_permission(CategoryTranslation.view_permission):
                buttons.append([InlineKeyboardButton(_("Translations"), callback_data='category_translations')])
            if user.has_permission(Product.view_permission):
                buttons.append([InlineKeyboardButton(_("Products"), callback_data='products')])
            if user.has_permission("allow_export_qr_codes"):
                buttons.append([InlineKeyboardButton(_("Export QR codes"), callback_data='export_qrs')])
        if user.has_permission(Category.import_permission):
            buttons.append([InlineKeyboardButton(_("Import from CSV"), callback_data='import_csv')])
        return buttons

    @require_permission("allow_export_qr_codes")
    def export_qrs(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        products = DBSession.query(Product).order_by(Product.id.asc()).all()
        images = []

        qr_codes = BytesIO()
        wb = Workbook()
        ws = wb.active
        ws.title = _("QR Codes")
        ws["A1"] = _("ID")
        ws["B1"] = _("Name EN")
        ws["C1"] = _("Name RU")
        ws["D1"] = _("Name UK")
        ws["E1"] = _("Price")
        ws["F1"] = _("Packaging")
        ws["G1"] = _("Packaging unit")
        ws["H1"] = _("QR")
        ws.column_dimensions[f"A"].width = 5
        ws.column_dimensions[f"B"].width = 20
        ws.column_dimensions[f"C"].width = 20
        ws.column_dimensions[f"D"].width = 20
        ws.column_dimensions[f"E"].width = 8
        ws.column_dimensions[f"F"].width = 8
        ws.column_dimensions[f"G"].width = 5
        ws.column_dimensions[f"H"].width = 30

        for idx, product in enumerate(products):
            ws.cell(idx + 2, 1).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 2).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 3).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 4).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 5).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 6).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 7).alignment = Alignment(horizontal='center', vertical="center")
            ws.cell(idx + 2, 8).alignment = Alignment(horizontal='center', vertical="center")

            ws[f"A{idx + 2}"] = product.id
            ws[f"B{idx + 2}"] = product.get_translation("en").name
            ws[f"C{idx + 2}"] = product.get_translation("ru").name
            ws[f"D{idx + 2}"] = product.get_translation("uk").name
            ws[f"E{idx + 2}"] = product.price
            ws[f"F{idx + 2}"] = product.packaging
            ws[f"G{idx + 2}"] = product.get_translation("uk").packaging_unit
            img = BytesIO()
            data = f"{product.id}"

            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.constants.ERROR_CORRECT_L,
                box_size=10,
                border=2,
            )
            qr.add_data(data)
            qr.make(fit=True)

            image = qr.make_image()

            image.save(img, format="PNG")
            img.seek(0)

            ws.add_image(Image(img), f"H{idx + 2}")
            images.append(img)

            ws.row_dimensions[idx + 2].height = 200

        wb.save(qr_codes)
        qr_codes.seek(0)

        adapted_date = pytz.utc.localize(datetime.datetime.utcnow()).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
        qr_codes.name = f"QR Codes from {adapted_date.strftime('%d.%m.%y %H-%M')}.xlsx"

        buttons = [[InlineKeyboardButton(_("Back"), callback_data="back_to_menu")]]
        send_or_edit(context, chat_id=user.chat_id, caption=_("This is file with products QR codes"), document=qr_codes, reply_markup=InlineKeyboardMarkup(buttons))

        qr_codes.close()
        for img in images:
            img.close()

        return self.States.ACTION

    def additional_states(self):
        add_edit_categories = CategoryAddEditMenu(self)
        products_menu = ProductsMenu(self)
        csv_import_categories_menu = CategoryCSVLoaderMenu(self)
        category_translations_menu = CategoryTranslationsMenu(self)
        return {self.States.ACTION: [add_edit_categories.handler,
                                     category_translations_menu.handler,
                                     products_menu.handler,
                                     csv_import_categories_menu.handler,
                                     CallbackQueryHandler(self.back_to_menu, pattern="^back_to_menu$"),
                                     CallbackQueryHandler(self.export_qrs, pattern="^export_qrs$")
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Category deleted")


class CategoryAddEditMenu(ArrowAddEditMenu):
    menu_name = 'category_add_edit_menu'
    model = dict
    reset_to_default = True

    @require_permission(Category.add_permission)
    def add_entry(self, update, context):
        return super(CategoryAddEditMenu, self).entry(update, context)

    @require_permission(Category.edit_permission)
    def edit_entry(self, update, context):
        return super(CategoryAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        if self.action(context) is self.Action.EDIT:
            return self.parent.selected_object(context)

    def load(self, context):
        self._load(context)
        user = context.user_data['user']

        category = self.query_object(context)
        if category and category.parent_category:
            context.user_data[self.menu_name]['parent_category_name'] = category.parent_category.get_translation(user.language_code).name

    def message_bottom_text(self, context):
        _ = context.user_data['user'].translator
        message_text = "〰〰〰〰〰〰〰〰〰〰"
        message_text += '\n'

        available_delivery_centers = DBSession.query(func.count(DeliveryCenter.id)).scalar()
        if available_delivery_centers <= 0:
            message_text += _("There is no delivery centers yet. Please add one in Admin menu/Delivery centers menu.")
        return message_text

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        delivery_center_translations = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.lang == user.language_code).all()
        delivery_centers_names = [dct.name for dct in delivery_center_translations]
        categorie_translations = DBSession.query(CategoryTranslation).filter(CategoryTranslation.lang == user.language_code).all()
        categories_names = [c.name for c in categorie_translations]

        if self.action(context) is self.Action.EDIT:
            fields = [self.Field('parent_category_name', _("Parent category"), validators.String(), required=False, variants=categories_names, default=None)]
        else:
            fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                      self.Field('code', _("*Code"), validators.String(), required=True),
                      self.Field('parent_category_name', _("Parent category"), validators.String(), required=False, variants=categories_names, default=None),
                      self.Field('delivery_center_name', _("*Delivery center"), validators.String(), variants=delivery_centers_names, required=True, switchable=bool(delivery_centers_names))]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.add_entry, pattern="^add_category$"),
                CallbackQueryHandler(self.edit_entry, pattern="^edit_category$")]

    def save_object(self, obj, context, session=None):
        if self.action(context) is self.Action.EDIT:
            category = self.query_object(context)
        else:
            category = Category()

        if 'name' in context.user_data[self.menu_name]:
            translation = CategoryTranslation(lang='en')
            translation.name = obj['name']
            category.translations.append(translation)

        if 'delivery_center_name' in context.user_data[self.menu_name]:
            delivery_center_translation = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.name == context.user_data[self.menu_name]['delivery_center_name']).first()
            category.delivery_center_id = delivery_center_translation.delivery_center_id

        if 'code' in context.user_data[self.menu_name]:
            category.code = context.user_data[self.menu_name]['code']

        parent_category_translation = DBSession.query(CategoryTranslation).filter(CategoryTranslation.name == context.user_data[self.menu_name]['parent_category_name']).first()
        category.parent_category = parent_category_translation.category if parent_category_translation else None

        if not add_to_db(category, session):
            return self.conv_fallback(context)


class CategoryTranslationsMenu(OneListMenu):
    menu_name = 'category_translations_menu'

    @require_permission(CategoryTranslation.view_permission)
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        category = self.parent.selected_object(context)
        if category:
            return DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == category.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^category_translations$')]

    def message_text(self, context, obj):
        _ = context.user_data['user'].translator

        if obj:
            message_text = f"____{_('Category Translations')}____\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
        else:
            message_text = _("There is no translations yet") + '\n'

        return message_text

    @require_permission(CategoryTranslation.delete_permission)
    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        return super(CategoryTranslationsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        user = context.user_data['user']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(CategoryTranslation.lang).filter(CategoryTranslation.category == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if len(exist_translations) <= 2 and user.has_permission(CategoryTranslation.add_permission):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_category_trans"))
        if user.has_permission(CategoryTranslation.edit_permission):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_category_trans"))
        if user.has_permission(CategoryTranslation.delete_permission) and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        category_translation_add_edit_menu = CategoryTranslationAddEditMenu(self)
        return {self.States.ACTION: [category_translation_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Translation deleted")


class CategoryTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = 'category_translation_add_edit_menu'
    model = CategoryTranslation

    @require_permission(CategoryTranslation.add_permission)
    def category_add(self, update, context):
        return super(CategoryTranslationAddEditMenu, self).entry(update, context)

    @require_permission(CategoryTranslation.edit_permission)
    def category_edit(self, update, context):
        return super(CategoryTranslationAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        ct_trans = self.parent.selected_object(context)
        if ct_trans and self.action(context) == self.Action.EDIT:
            return ct_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['user'].translator
        exist_translations = [trans[0].upper() for trans in DBSession.query(CategoryTranslation.lang).filter(CategoryTranslation.category == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True)]
        return fields

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.category = self.parent.parent.selected_object(context)
        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def entry_points(self):
        return [CallbackQueryHandler(self.category_add, pattern="^add_category_trans$"),
                CallbackQueryHandler(self.category_edit, pattern="^edit_category_trans$")]
