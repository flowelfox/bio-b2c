import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, prepare_user
from botmanlib.menus.ready_to_use.permissions import PermissionsMenu
from botmanlib.messages import send_or_edit, delete_interface, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler, PrefixHandler

from src.menus.admin.baskets import BasketsMenu
from src.menus.admin.categories import ProductCategoriesMenu
from src.menus.admin.delivery_centers import DeliveryCentersMenu
from src.menus.admin.distributions import DistributionsMenu
from src.menus.admin.formed_baskets import FormedBasketsMenu
from src.menus.admin.help import HelpMenu
from src.menus.admin.info import InfoMenu
from src.menus.admin.manufacturers import ManufacturersMenu
from src.menus.admin.ordered_products import OrderedProductsMenu
from src.models import User, Permission, DeliveryCenter, Basket, Manufacturer, Category


class AdminMenu(BaseMenu):
    menu_name = 'admin_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        _ = context.user_data['user'].translator

        if not user.has_permission('admin_menu_access'):
            delete_user_message(update)
            return ConversationHandler.END

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        delete_interface(context)
        self.send_message(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = []

        if user.has_permission('permissions_menu_access'):
            buttons.append([InlineKeyboardButton(_("Permissions"), callback_data='permissions')])
        if user.has_permission('distribution_menu_access'):
            buttons.append([InlineKeyboardButton(_("Distribution"), callback_data='distribution')])

        products_buttons = []
        if user.has_permission(Category.view_permission):
            products_buttons.append(InlineKeyboardButton(_("Categories & Products"), callback_data='categories'))
        if user.has_permission('ordered_products_menu_access'):
            products_buttons.append(InlineKeyboardButton(_("Ordered products"), callback_data='ordered_products'))

        if products_buttons:
            buttons.append(products_buttons)

        if user.has_permission('info_menu_access'):
            buttons.append([InlineKeyboardButton(_("Info"), callback_data='info')])
        if user.has_permission(DeliveryCenter.view_permission):
            buttons.append([InlineKeyboardButton(_("Delivery centers"), callback_data='delivery_centers')])

        basket_buttons = []
        if user.has_permission(Basket.view_permission):
            basket_buttons.append(InlineKeyboardButton(_("Baskets"), callback_data='baskets'))
        if user.has_permission('admin_formed_baskets_menu_access'):
            basket_buttons.append(InlineKeyboardButton(_("Formed baskets"), callback_data='formed_baskets'))

        if basket_buttons:
            buttons.append(basket_buttons)

        if user.has_permission(Manufacturer.view_permission):
            buttons.append([InlineKeyboardButton(_("Manufacturers"), callback_data='manufacturers')])
        if user.has_permission("change_help_menu_permission"):
            buttons.append([InlineKeyboardButton(_("Edit help"), callback_data='edit_help')])

        buttons.append([InlineKeyboardButton(_("Back to main menu"), callback_data='start')])

        send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(buttons))

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        permissions_menu = PermissionsMenu(User, Permission, parent=self)
        distribution_menu = DistributionsMenu(self)
        products_menu = ProductCategoriesMenu(self)
        delivery_centers = DeliveryCentersMenu(self)
        info_menu = InfoMenu(self)
        baskets_menu = BasketsMenu(self)
        manufacturers_menu = ManufacturersMenu(self)
        help_menu = HelpMenu(self)
        formed_baskets_menu = FormedBasketsMenu(self)
        ordered_products = OrderedProductsMenu(self)

        handler = ConversationHandler(entry_points=[PrefixHandler('/', "admin", self.entry)],
                                      states={
                                          self.States.ACTION: [distribution_menu.handler,
                                                               CallbackQueryHandler(self.entry, pattern='^back$'),
                                                               permissions_menu.handler,
                                                               products_menu.handler,
                                                               delivery_centers.handler,
                                                               info_menu.handler,
                                                               baskets_menu.handler,
                                                               manufacturers_menu.handler,
                                                               help_menu.handler,
                                                               formed_baskets_menu.handler,
                                                               ordered_products.handler,
                                                               ],

                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/start$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/operator$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
