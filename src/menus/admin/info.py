import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import group_buttons, inline_placeholder, require_permission
from botmanlib.messages import send_or_edit, delete_user_message
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler

from src.menus.admin.users_and_baskets import UsersInfoMenu


class InfoMenu(BaseMenu):
    menu_name = 'info_menu'

    class States(enum.Enum):
        ACTION = 1

    @require_permission("info_menu_access")
    def entry(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = []
        if user.has_permission("users_info_menu_access"):
            buttons.append(InlineKeyboardButton(_("Users info"), callback_data='users_info'))
        if user.has_permission("formed_baskets_info_menu_access"):
            buttons.append(InlineKeyboardButton(_("Formed baskets"), callback_data='formed_baskets_info'))
        if user.has_permission("ordered_products_info_menu_access"):
            buttons.append(InlineKeyboardButton(_("Ordered products"), callback_data='ordered_products_info'))
        buttons.append(InlineKeyboardButton(_("⏪ Back"), callback_data=f'back_{self.menu_name}'))

        send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(group_buttons(buttons, 1)))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        users_info_menu = UsersInfoMenu(self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern="^info$")],
                                      states={
                                          self.States.ACTION: [
                                              CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                              users_info_menu.handler,
                                              CallbackQueryHandler(inline_placeholder(self.States.ACTION, "Menu under development"), pattern='^formed_baskets_info$'),
                                              CallbackQueryHandler(inline_placeholder(self.States.ACTION, "Menu under development"), pattern='^ordered_products_info$'),
                                          ],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
