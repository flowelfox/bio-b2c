from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import require_permission, get_settings, write_settings
from botmanlib.messages import delete_interface
from botmanlib.models import MessageType
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler

from src.settings import SETTINGS_FILE


class HelpMenu(OneListMenu):
    menu_name = 'help_menu'
    model = dict

    @require_permission("change_help_menu_permission")
    def entry(self, update, context):
        self._load(context)
        _ = context.user_data['user'].translator
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        settings = get_settings(SETTINGS_FILE)

        return list(sorted(settings['help'], key=lambda x: x.get('priority', 0), reverse=True))

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^edit_help$')]

    def message_text(self, context, entry):
        user = context.user_data['user']
        _ = user.translator

        if entry:
            message_text = _("Question EN") + f": {entry['question']['en']}\n"
            message_text += _("Question RU") + f": {entry['question']['ru']}\n"
            message_text += _("Question UK") + f": {entry['question']['uk']}\n"

            message_text += _("Answer EN") + f": {entry['answer']['en']}\n"
            message_text += _("Answer RU") + f": {entry['answer']['ru']}\n"
            message_text += _("Answer UK") + f": {entry['answer']['uk']}\n"

            image = entry.get("image", None)

            message_text += _("Image EN") + f": {_('Loaded') if image and image.get('en', None) else _('Not loaded')}\n"
            message_text += _("Image RU") + f": {_('Loaded') if image and image.get('ru', None) else _('Not loaded')}\n"
            message_text += _("Image UK") + f": {_('Loaded') if image and image.get('uk', None) else _('Not loaded')}\n"

            message_text += _("Priority") + f": {entry.get('priority', 0)}\n"
        else:
            message_text = _("There is questions yet") + '\n'
        return message_text

    @require_permission('delete_help_entries_menu_permission')
    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        return super(HelpMenu, self).delete_ask(update, context)

    def delete(self, update, context):
        user_data = context.user_data
        data = update.callback_query.data

        if data == f'delete_yes_{self.menu_name}':
            settings = get_settings(SETTINGS_FILE)
            entry = self.selected_object(context)
            if entry in settings['help']:
                settings['help'].remove(entry)

            write_settings({"help": settings["help"]}, SETTINGS_FILE)

            update.callback_query.answer(text=self.after_delete_text(context), show_alert=True)
            delete_interface(context)
            self.update_objects(context)
            user_data[self.menu_name]['selected_object'] = 0
            return self.prev_object(update, context)

        elif data == f'delete_no_{self.menu_name}':
            self.bot.answer_callback_query(update.callback_query.id)
            user_data[self.menu_name]['delete_id'] = None

        self.send_message(context)
        return self.States.ACTION

    def center_buttons(self, context, o=None):
        _ = context.user_data['user'].translator
        buttons = []
        user = context.user_data['user']
        if user.has_permission("add_help_entries_menu_permission"):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_question"))
        if o and user.has_permission("edit_help_entries_menu_permission"):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_question"))
        if o and user.has_permission("delete_help_entries_menu_permission"):
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        return buttons

    def additional_states(self):
        question_add_edit_menu = HelpAddEditMenu(self)
        return {self.States.ACTION: [question_add_edit_menu.handler, ]}

    def after_delete_text(self, context):
        _ = context.user_data['user'].translator
        return _("Question deleted")


class HelpAddEditMenu(ArrowAddEditMenu):
    menu_name = 'help_add_edit_menu'
    model = dict

    @require_permission("add_help_entries_menu_permission")
    def add_entry(self, update, context):
        return super(HelpAddEditMenu, self).entry(update, context)

    @require_permission("edit_help_entries_menu_permission")
    def edit_entry(self, update, context):
        return super(HelpAddEditMenu, self).entry(update, context)

    def query_object(self, context):
        return None

    def load(self, context):
        super(HelpAddEditMenu, self).load(context)
        if self.action(context) is self.Action.EDIT:
            entry = self.parent.selected_object(context)
            context.user_data[self.menu_name]['question_en'] = entry['question']['en']
            context.user_data[self.menu_name]['question_ru'] = entry['question']['ru']
            context.user_data[self.menu_name]['question_uk'] = entry['question']['uk']

            context.user_data[self.menu_name]['answer_en'] = entry['answer']['en']
            context.user_data[self.menu_name]['answer_ru'] = entry['answer']['ru']
            context.user_data[self.menu_name]['answer_uk'] = entry['answer']['uk']

            context.user_data[self.menu_name]['priority'] = entry.get('priority', 0)
            if entry.get("image", None):
                context.user_data[self.menu_name]['image_en'] = entry['image']['en']
                context.user_data[self.menu_name]['image_ru'] = entry['image']['ru']
                context.user_data[self.menu_name]['image_uk'] = entry['image']['uk']

    def fields(self, context):
        user = context.user_data['user']
        _ = user.translator

        fields = [self.Field('question_en', _("*Question EN"), validators.String(), required=True),
                  self.Field('question_ru', _("*Question RU"), validators.String(), required=True),
                  self.Field('question_uk', _("*Question UK"), validators.String(), required=True),
                  self.Field('answer_en', _("*Answer EN"), validators.String(), required=True),
                  self.Field('answer_ru', _("*Answer RU"), validators.String(), required=True),
                  self.Field('answer_uk', _("*Answer UK"), validators.String(), required=True),
                  self.Field('image_en', _("Image EN"), validators.String(), message_type=MessageType.photo, required=False),
                  self.Field('image_ru', _("Image RU"), validators.String(), message_type=MessageType.photo, required=False),
                  self.Field('image_uk', _("Image UK"), validators.String(), message_type=MessageType.photo, required=False),
                  self.Field('priority', _("*Priority"), validators.Int(), required=True),
                  ]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.add_entry, pattern="^add_question$"),
                CallbackQueryHandler(self.edit_entry, pattern="^edit_question$")]

    def save_object(self, obj, context, session=None):
        new_entry = {"question": {'en': context.user_data[self.menu_name]['question_en'],
                                  'ru': context.user_data[self.menu_name]['question_ru'],
                                  'uk': context.user_data[self.menu_name]['question_uk'],
                                  },
                     "answer": {'en': context.user_data[self.menu_name]['answer_en'],
                                'ru': context.user_data[self.menu_name]['answer_ru'],
                                'uk': context.user_data[self.menu_name]['answer_uk'],
                                },
                     "image": {'en': context.user_data[self.menu_name]['image_en'],
                               'ru': context.user_data[self.menu_name]['image_ru'],
                               'uk': context.user_data[self.menu_name]['image_uk'],
                               },
                     "priority": context.user_data[self.menu_name]['priority']
                     }

        if self.action(context) is self.Action.EDIT:
            settings = get_settings(SETTINGS_FILE)
            entry = self.parent.selected_object(context)
            if entry in settings['help']:
                settings['help'].remove(entry)

            settings['help'].append(new_entry)
            write_settings({"help": settings["help"]}, SETTINGS_FILE)
        else:
            settings = get_settings(SETTINGS_FILE)
            settings['help'].append(new_entry)
            write_settings({"help": settings["help"]}, SETTINGS_FILE)
