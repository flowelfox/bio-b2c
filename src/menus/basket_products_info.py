import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission, to_state
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError
from telegram.ext import CallbackQueryHandler, ConversationHandler, MessageHandler, Filters

from src.models import DBSession, Product, Basket, ProductTranslation
from src.settings import WEBHOOK_ENABLE


class BasketProductsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2

    menu_name = "basket_products_menu"
    model = Product
    disable_web_page_preview = False
    auto_hide_arrows = True
    interface = "interface"

    @require_permission("product_menu_access")
    def entry(self, update, context):
        self._load(context)
        context.user_data[self.menu_name]['basket_id'] = int(update.callback_query.data.replace("more_", ""))

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        product = DBSession.query(Product).join(Product.baskets).filter(Basket.id == context.user_data[self.menu_name]['basket_id']).all()
        return product

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r"^more_\d+$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        context.user_data[self.menu_name]['basket_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        self.parent.send_message(context)
        return ConversationHandler.END

    def object_buttons(self, context, objects):
        _ = context.user_data['user'].translator
        buttons = []
        if objects:
            buttons.append([InlineKeyboardButton(_("👀 Peek under the wrap"), callback_data='product_more')])
        return buttons

    def more(self, update, context):
        _ = context.user_data['user'].translator
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_product')]]
        translation = self.selected_object(context).get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = ProductTranslation(name=_("Unknown"), description=_("Unknown"), short_description=_("Unknown"))
        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")
        send_or_edit(context, self.interface, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if obj:
            translation = obj.get_translation(user.language_code)
            manufacturer_translation = obj.manufacturer.get_translation(user.language_code) if obj.manufacturer else None
            if translation.image is not None:
                message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += f"<b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            if translation.short_description:
                message_text += translation.short_description + "\n\n"
            elif translation.description:
                message_text += translation.description[:100] + "...\n\n"

            if manufacturer_translation:
                message_text += _("Producer") + f": {manufacturer_translation.name}\n"
            if obj.packaging:
                message_text += _("Package") + f": {obj.packaging} {translation.packaging_unit if translation.packaging_unit else ''}\n"
            if obj.price:
                message_text += _("Price") + f": {obj.price:.2f} {_('UAH')}\n"

        else:
            message_text = _("There is nothing to list") + '\n'
        return message_text

    def page_text(self, current_page, max_page, context):
        user = context.user_data['user']
        _ = user.translator

        obj = self.selected_object(context)

        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += obj.category.get_translation(user.language_code).name + " (" + _("Product") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def back_to_product(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.more, pattern='^product_more$')],
                self.States.MORE: [CallbackQueryHandler(self.back_to_product, pattern='^back_to_product$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))]}

    def send_message(self, context):
        user_data = context.user_data
        user = user_data['user']

        back_button = self.back_button(context)

        if user_data[self.menu_name]['filtered_objects']:
            back_button = self.search_back_button(context)

        objects = self._get_objects(context)

        buttons = []
        # add user defined top buttons
        top_buttons = self.top_buttons(context)
        if top_buttons:
            buttons.extend(top_buttons)

        if not objects:
            # define message text for empty menu
            message_text = None
            if user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, None)

            if message_text is None:
                message_text = self.message_text(context, None)

            # define list controls
            controls = self.controls(context)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, None)
            if object_buttons:
                buttons.extend(object_buttons)
        else:
            # get current selected object
            selected_object = objects[user_data[self.menu_name]['selected_object']]

            # define message text when objects exists
            message_text = None
            if user_data[self.menu_name]['filtered_objects']:
                message_text = self.search_message_text(context, selected_object)

            if message_text is None:
                message_text = self.message_text(context, selected_object)

            # define list controls
            controls = self.controls(context, o=selected_object)
            if controls:
                buttons.append(controls)

            # add additional user buttons
            object_buttons = self.object_buttons(context, selected_object)
            if object_buttons:
                buttons.extend(object_buttons)

            # add delete button if needed
            delete_button = self.delete_button(context)
            if self.add_delete_button and delete_button:
                buttons.append([delete_button])

            # add page text for message text
            message_text += self.page_text(user_data[self.menu_name]['selected_object'] + 1, user_data[self.menu_name]['max_page'], context)

        # add back button
        if back_button:
            buttons.append([back_button])

        # add user defined bottom buttons
        bottom_buttons = self.bottom_buttons(context)
        if bottom_buttons:
            buttons.extend(bottom_buttons)

        markup = InlineKeyboardMarkup(buttons)

        # try to send message
        try:
            return send_or_edit(context, self.interface, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        except TelegramError as e:
            if e.message.startswith("Can't parse entities"):
                message_text = message_text.replace('\\', "")
                return send_or_edit(context, self.interface, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=None, disable_web_page_preview=self.disable_web_page_preview)
            else:
                raise e
