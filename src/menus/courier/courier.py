import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import add_to_db, prepare_user, require_permission
from botmanlib.messages import send_or_edit, delete_user_message, delete_interface
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, Filters, MessageHandler, PrefixHandler

from src.menus.courier.baskets_for_delivery import BasketsToDeliveryMenu
from src.menus.mixins.welcome_menu import WelcomeMenuMixin
from src.menus.start.profile import ProfileMenu
from src.menus.take_basket import TakeBasketMenu
from src.models import User


class CourierMenu(BaseMenu, WelcomeMenuMixin):
    menu_name = 'courier_menu'

    class States(enum.Enum):
        ACTION = 1
        LANGUAGE = 2
        CITY = 3
        ASK_PHONE = 4

    def entry(self, update, context):
        user = prepare_user(User, update, context)
        _ = context.user_data['user'].translator

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        if not user.has_permission('courier_menu_access') or not user.courier:
            delete_user_message(update)
            return ConversationHandler.END

        if update.effective_message.text and update.effective_message.text.startswith("/"):
            delete_interface(context)

        if user.city is None:
            return self.ask_city(update, context)

        elif user.phone is None:
            return self.ask_phone(update, context)

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("👩‍💻 Operator Center") + '</b>\n'
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Select the required section:")

        buttons = [[InlineKeyboardButton(_("🚚 Baskets for delivery"), callback_data='baskets_to_delivery')],
                   [InlineKeyboardButton(_("🔄 Get a cart"), callback_data='courier_take_basket')],
                   [InlineKeyboardButton(_("👤 My profile"), callback_data='profile')],
                   [InlineKeyboardButton("🇺🇦🇷🇺🇬🇧", callback_data='language')]]

        send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.ACTION

    @require_permission("change_language_menu_access")
    def ask_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton("🇬🇧ENG", callback_data='language_en')],
                   [InlineKeyboardButton("🇺🇦UKR", callback_data='language_uk')],
                   [InlineKeyboardButton("🇷🇺RUS", callback_data='language_ru')]]
        send_or_edit(context, chat_id=user.chat_id, text=_("Select a language:"),
                     reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.LANGUAGE

    def set_language(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        value = update.callback_query.data.replace("language_", "")

        if value == 'en':
            user.language_code = 'en'
        elif value == 'ru':
            user.language_code = 'ru'
        elif value == 'uk':
            user.language_code = 'uk'
        else:
            user.language_code = 'en'

        add_to_db(user)

        self.send_message(context)

        return self.States.ACTION

    def back_to_menu(self, update, context):
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def goto_next_menu(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        profile_menu = ProfileMenu(self)
        take_basket_menu = TakeBasketMenu(self)
        baskets_to_delivery = BasketsToDeliveryMenu(self)

        handler = ConversationHandler(entry_points=[PrefixHandler('/', 'courier', self.entry),
                                                    CallbackQueryHandler(self.entry, pattern="^operator$")],
                                      states={
                                          self.States.ACTION: [take_basket_menu.handler,
                                                               baskets_to_delivery.handler,
                                                               profile_menu.handler,
                                                               CallbackQueryHandler(self.ask_language, pattern="^language$")],
                                          self.States.LANGUAGE: [CallbackQueryHandler(self.set_language, pattern=r"^language_\w\w$")],
                                          self.States.CITY: [MessageHandler(Filters.text, self.find_city),
                                                             CallbackQueryHandler(self.set_city)],
                                          self.States.ASK_PHONE: [MessageHandler(Filters.contact, self.set_phone_contact),
                                                                  MessageHandler(Filters.text, self.set_phone)]
                                      },
                                      fallbacks=[
                                          MessageHandler(Filters.regex('^/admin$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/start$'), self.goto_next_menu),
                                          MessageHandler(Filters.regex('^/operator$'), self.goto_next_menu),
                                          MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler
