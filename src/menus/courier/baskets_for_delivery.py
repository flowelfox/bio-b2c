import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import require_permission, add_to_db
from botmanlib.messages import send_or_edit
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler

from src.menus.basket_products_info import BasketProductsMenu
from src.menus.transfer_menu import TransferMenu
from src.models import DBSession, FormedBasket, FormedBasketStatus


class BasketsToDeliveryMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = "courier_baskets_to_delivery_menu"
    disable_web_page_preview = False

    @require_permission("courier_baskets_to_delivery_menu_access")
    def entry(self, update, context):
        return super(BasketsToDeliveryMenu, self).entry(update, context)

    def query_objects(self, context):
        user = context.user_data['user']

        formed_baskets = DBSession.query(FormedBasket) \
            .filter(FormedBasket.holder_id == user.id) \
            .filter(FormedBasket.status == FormedBasketStatus.filled) \
            .all()

        return formed_baskets

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^baskets_to_delivery$")]

    def back_button(self, context):
        _ = context.user_data['user'].translator
        return InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, formed_basket):
        _ = context.user_data['user'].translator
        buttons = []
        if formed_basket:
            buttons.append([InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'more_{formed_basket.basket_order.basket.id}')])
            buttons.append([InlineKeyboardButton(_("ℹ Cart details"), callback_data=f'details_{formed_basket.id}')])
            if formed_basket.basket_order.user.username:
                buttons.append([InlineKeyboardButton(_("💭 Chat with a customer"), url=f"t.me/{formed_basket.basket_order.user.username}")])

            buttons.append([InlineKeyboardButton(_("✅ Transfer cart"), callback_data='courier_transfer_basket')])
            if not formed_basket.delivery_failed:
                buttons.append([InlineKeyboardButton(_("😒 Nobody"), callback_data='nobody')])

        return buttons

    def message_text(self, context, formed_basket):
        user = context.user_data['user']
        _ = user.translator

        message_text = ""
        if formed_basket:
            translation = formed_basket.basket_order.basket.get_translation(user.language_code)
            message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += "<b>" + _("ID:") + f" {formed_basket.id}</b>\n\n"

            message_text += "<b>" + _("Delivery Details:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("📍 Address") + f": {formed_basket.address if formed_basket.address else _('Unknown')}\n"
            if formed_basket.delivery_start_time and formed_basket.delivery_end_time:
                message_text += f"🕐 {formed_basket.delivery_start_time.strftime('%H:%M')}-{formed_basket.delivery_end_time.strftime('%H:%M')}\n"
            else:
                message_text += f"🕐 {_('Unknown')}\n"
            if formed_basket.delivery_failed:
                message_text += _("⚠ Delivery for this basket was failed. Return it to operator.") + "\n"
            message_text += "\n"

            message_text += "<b>" + _("👤 Customer:") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("ID:") + f" {formed_basket.basket_order.user.chat_id}\n"
            message_text += _("Name:") + f" <a href=\"{formed_basket.basket_order.user.mention_url}\">{formed_basket.basket_order.user.get_name()}</a>\n"
            message_text += _("Phone:") + f" {formed_basket.basket_order.user.phone if formed_basket.basket_order.user.phone else _('Unknown')}\n"
            city = formed_basket.basket_order.user.city.get_translation(user.language_code).name if formed_basket.basket_order.user.city else _('Unknown')
            if formed_basket.basket_order.user.city.get_translation(user.language_code).name == _("💬 Another City"):
                city += f" ({formed_basket.basket_order.user.other_city})"
            message_text += _("City:") + f" {city}\n\n"

        else:
            message_text = "<b>" + _("🚚 Carts for Delivery") + '</b>\n'
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("You do not have delivery request yet") + "\n\n"
            message_text += _('Start with <b>🔄 Get a Cart</b>')
        return message_text

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("details_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_menu')]]
        send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def nobody(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket = self.selected_object(context)
        formed_basket.delivery_failed = True
        add_to_db(formed_basket)
        update.callback_query.answer(text=_("Basket delivery failed. Please return this basket to delivery center."), show_alert=True)

        # send message to client
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)
        client_message_text = "<b>" + _("🔕 Cart wasn't delivered") + "</b>\n"
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        client_message_text += _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}\n"
        client_message_text += _("ID:") + f" {formed_basket.id}\n\n"

        client_message_text += "<b>" + _("👤 Courier:") + '</b>\n'
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        client_message_text += _("Name:") + f" {user.desired_name}\n"
        client_message_text += _("Phone:") + f" {user.phone}"

        client_buttons = []
        if user.username:
            client_buttons.append([InlineKeyboardButton(_("💭 Chat with a courier"), url=f"t.me/{user.username}")])
        client_buttons.append([InlineKeyboardButton(_("Close"), callback_data='close_message')])
        context.bot.send_message(chat_id=formed_basket.basket_order.user.chat_id, text=client_message_text,
                                 reply_markup=InlineKeyboardMarkup(client_buttons), parse_mode="HTML")

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['user'].translator
        if max_page > 1:
            message_text = "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("🚚 Carts for delivery") + ' (' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            message_text = ""

        return message_text

    def additional_states(self):
        transfer_menu = TransferMenu(self)
        basket_more = BasketProductsMenu(self)
        return {self.States.ACTION: [transfer_menu.handler,
                                     basket_more.handler,
                                     CallbackQueryHandler(self.nobody, pattern=r"^nobody$"),
                                     CallbackQueryHandler(self.back_to_menu, pattern=r"^back_to_menu$"),
                                     CallbackQueryHandler(self.details, pattern=r"^details_\d+$")]}
