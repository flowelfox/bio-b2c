import enum
from io import BytesIO

import qrcode
from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import require_permission, add_to_db, inline_placeholder
from botmanlib.messages import delete_user_message, send_or_edit, delete_interface
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler, Filters, MessageHandler

from src.menus.basket_products_info import BasketProductsMenu
from src.models import DBSession, FormedBasket
from src.settings import WEBHOOK_ENABLE


class TakeBasketMenu(BaseMenu):
    menu_name = "take_basket_menu"

    class States(enum.Enum):
        ACTION = 1
        FINISH = 2

    @require_permission("allow_user_take_basket")
    def entry_user(self, update, context):
        user = context.user_data['user']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['from'] = "user"

        user.waiting_for_transfer = True
        add_to_db(user)

        self.send_message(context)
        return self.States.ACTION

    @require_permission("allow_operator_take_basket")
    def entry_operator(self, update, context):
        user = context.user_data['user']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['from'] = "operator"

        user.waiting_for_transfer = True
        add_to_db(user)

        self.send_message(context)
        return self.States.ACTION

    @require_permission("allow_courier_take_basket")
    def entry_courier(self, update, context):
        user = context.user_data['user']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['from'] = "courier"

        user.waiting_for_transfer = True
        add_to_db(user)

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        delete_interface(context)
        if context.user_data[self.menu_name]['from'] == "user":
            message_text = "<b>" + _("🛍️ Get a cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("Please show this QR code to the operator at the issuing center or to the courier, if you ordered delivery")
        elif context.user_data[self.menu_name]['from'] == "operator":
            message_text = "<b>" + _("🔄 Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("If the cart is returned to the Issuing Center, show this QR code to the courier and add it again to the warehouse")
        elif context.user_data[self.menu_name]['from']:
            message_text = "<b>" + _("🔄 Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            message_text += _("If the cart is returned to the Issuing Center, show this QR code to the courier and add it again to the warehouse")
        else:
            message_text = "<b>" + _("🔄 Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]

        with BytesIO() as output:
            data = f"{user.language_code}{user.phone.replace('-', '').replace('+', '')}00000{user.id}"
            image = qrcode.make(data)
            image.save(output, format="PNG")
            output.seek(0)
            send_or_edit(context, "transfer_confirmation", chat_id=user.chat_id, caption=message_text, photo=output, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def back(self, update, context):
        user = context.user_data['user']

        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        if update.callback_query:
            update.callback_query.answer()

        user.waiting_for_transfer = False
        add_to_db(user)

        delete_interface(context, "transfer_confirmation")
        if 'declined_basket_id' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['declined_basket_id']
        if 'transfer_interface_message' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['transfer_interface_message']
        self.parent.send_message(context)
        return ConversationHandler.END

    def confirm_transfer(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("transfer_confirm_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)

        # send message to operator/courier
        _ = formed_basket.holder.translator
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        operator_message_text = "<b>" + _("⏏️ Give out a Cart") + "</b>\n"
        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"

        if context.user_data[self.menu_name]['from'] == 'operator':
            role = _("Operator")
        elif context.user_data[self.menu_name]['from'] == 'courier':
            role = _("Courier")
        elif context.user_data[self.menu_name]['from'] == 'user':
            role = _("User")
        else:
            role = _("Unknown")

        operator_message_text += "<b>" + _("{role} {name} confirmed receipt of the cart").format(role=role, name=user.get_name()) + "</b>\n"
        operator_message_text += _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}\n"
        operator_message_text += _("ID:") + f" {formed_basket.id}\n\n"
        operator_message_text += "<i>" + _("Do not forget to smile and wish the client \"Bon appetit!\" 😉") + "</i>"

        operator_buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'to_baskets')]]
        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=formed_basket.holder.chat_id,
                     chat_id=formed_basket.holder.chat_id,
                     interface_name='transfer_basket',
                     text=operator_message_text,
                     reply_markup=InlineKeyboardMarkup(operator_buttons),
                     parse_mode="HTML")

        # pass basket
        formed_basket.pass_to(user, DBSession)

        # send notification to client if carrier take basket
        if formed_basket.holder.courier:
            _ = formed_basket.basket_order.user.translator
            client_message_text = "<b>" + _("🔔 Wait for the delivery!") + '</b>\n'
            client_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            client_message_text += _("Your cart already at courier - please, wait for your delivery:") + "\n"
            client_message_text += f"📍 {formed_basket.address}\n\n"

            client_message_text += "<b>" + _("👤 Courier:") + '</b>\n'
            client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
            client_message_text += _("Name:") + f" {formed_basket.holder.desired_name}\n"
            client_message_text += _("Phone:") + f" {formed_basket.holder.phone}"

            client_buttons = []
            if formed_basket.holder.username:
                client_buttons.append([InlineKeyboardButton(_("💭 Chat with a courier"), url=f"t.me/{formed_basket.holder.username}")])
            client_buttons.append([InlineKeyboardButton(_("👌 Great"), callback_data='close_message')])
            context.bot.send_message(chat_id=formed_basket.basket_order.user.chat_id, text=client_message_text,
                                     reply_markup=InlineKeyboardMarkup(client_buttons), parse_mode="HTML")

            message_text = "<b>" + _("🔄 Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += "<b>" + _("Congratulations! Cart successfully transferred.🥳") + "</b>\n\n"
        elif formed_basket.holder.operator and formed_basket.delivery_failed:
            _ = formed_basket.basket_order.user.translator
            delivery_center = formed_basket.basket_order.basket.delivery_center
            client_message_text = "<b>" + _("🔔 The cart was returned to the Issuing Center") + '</b>\n'
            client_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            client_message_text += _("Your organic cart is waiting at the Issue Center repeatedly:") + "\n"
            client_message_text += _("📍 Address") + f": {delivery_center.get_translation(formed_basket.basket_order.user.language_code).address}\n\n"
            if delivery_center.open_time and delivery_center.close_time:
                client_message_text += _("🕰️ Work time:") + f":️ {delivery_center.open_time.strftime('%H:%M')}-{delivery_center.close_time.strftime('%H:%M')}\n\n"
            client_message_text += "<b>" + _("Do not have time to get to the issuing center - order delivery repeatedly! 🚚") + "</b>"

            client_buttons = [[InlineKeyboardButton(_("🚚 Delivery (99 UAH)"), callback_data=f'order_delivery_{formed_basket.id}')],
                              [InlineKeyboardButton(_("👌 I manage on my own (0 UAH)"), callback_data=f'order_take_{formed_basket.id}')]]

            context.bot.send_message(chat_id=formed_basket.basket_order.user.chat_id,
                                     interface_name='filled_basket_notification',
                                     text=client_message_text,
                                     reply_markup=InlineKeyboardMarkup(client_buttons),
                                     parse_mode="HTML")

            message_text = "<b>" + _("🔄 Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += "<b>" + _("Congratulations! Cart successfully transferred.🥳") + "</b>\n\n"
        else:
            message_text = "<b>" + _("🛍️ Get a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += "<b>" + _("Congratulations! The cart found its owner 🥳") + "</b>\n\n"
            message_text += _("Bon appetit and see you next week!")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        send_or_edit(context, "transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.FINISH

    def decline_transfer(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket_id = int(update.callback_query.data.replace("transfer_decline_", ""))
        context.user_data[self.menu_name]['declined_basket_id'] = formed_basket_id
        context.user_data[self.menu_name]['transfer_interface_message'] = context.user_data['interfaces']['transfer_confirmation'].message

        message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("✍️ Please write the reason for the refusal and send it to me")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_confirm")]]
        send_or_edit(context, "transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.FINISH

    def decline_confirm(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['decline_reason'] = update.effective_message.text
        context.user_data[self.menu_name]['decline_photo'] = None

        if "declined_basket_id" not in context.user_data[self.menu_name]:
            return self.States.FINISH

        self.decline_details(update, context)
        delete_user_message(update)

        return self.States.FINISH

    def decline_details(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        buttons = []

        message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("You canceled the receipt of the cart with the following comment:") + '\n'
        message_text += "💬 <b>" + context.user_data[self.menu_name]['decline_reason'] + "</b>\n\n"

        if context.user_data[self.menu_name]['decline_photo'] is None:
            message_text += _("If necessary, you can <b>📸 Add a photo</b> - take a picture and send it to me. This will help us to improve follow-up service.")
            buttons.append([InlineKeyboardButton(_("📸 Add a photo"), callback_data=f"add_a_photo")])

        buttons.append([InlineKeyboardButton(_("✅ Confirm decline"), callback_data=f"confirm_basket_decline")])
        buttons.append([InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_confirm")])

        send_or_edit(context, "transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
        return self.States.FINISH

    def confirm_basket_decline(self, update, context):
        user = context.user_data['user']
        formed_basket_id = context.user_data[self.menu_name]['declined_basket_id']
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)

        formed_basket.decline_pass(context.user_data[self.menu_name]['decline_reason'], context.user_data[self.menu_name]['decline_photo'], DBSession)

        # send message to operator/courier
        _ = formed_basket.holder.translator
        translation = formed_basket.basket_order.basket.get_translation(formed_basket.holder.language_code)
        operator_message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        operator_message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        if context.user_data[self.menu_name]['decline_photo'] is not None and WEBHOOK_ENABLE:
            operator_message_text += f'<a href="{self.bot.get_image_url(context.user_data[self.menu_name]["decline_photo"])}">\u200B</a>'

        if context.user_data[self.menu_name]['from'] == 'operator':
            role = _("Operator")
        elif context.user_data[self.menu_name]['from'] == 'courier':
            role = _("Courier")
        elif context.user_data[self.menu_name]['from'] == 'user':
            role = _("User")
        else:
            role = _("Unknown")
        operator_message_text += "<b>" + _("{role} {name} canceled the receipt of the cart:").format(role=role, name=user.get_name()) + "</b>\n"
        operator_message_text += _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}\n"
        operator_message_text += _("ID:") + f" {formed_basket.id}\n\n"
        operator_message_text += "<b>" + _("💬 Indicated cause:") + "</b>\n"
        operator_message_text += context.user_data[self.menu_name]['decline_reason']

        operator_buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f'to_decline_confirm')]]
        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=formed_basket.holder.chat_id,
                     chat_id=formed_basket.holder.chat_id,
                     interface_name='transfer_basket',
                     text=operator_message_text,
                     reply_markup=InlineKeyboardMarkup(operator_buttons),
                     parse_mode="HTML")

        return self.back(update, context)

    def ask_decline_photo(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("🚫 Cancel Receipt") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += _("Please send me photo")
        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        send_or_edit(context, "transfer_confirmation", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.FINISH

    def set_decline_photo(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        context.user_data[self.menu_name]['decline_photo'] = update.effective_message.photo[-1]
        delete_user_message(update)
        return self.decline_details(update, context)

    def back_to_confirm(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        transfer_interface_message = context.user_data[self.menu_name]['transfer_interface_message']
        send_or_edit(context, 'transfer_confirmation', chat_id=user.chat_id, text=transfer_interface_message.text, reply_markup=transfer_interface_message.reply_markup)
        if 'declined_basket_id' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['declined_basket_id']
        if 'transfer_interface_message' in context.user_data[self.menu_name]:
            del context.user_data[self.menu_name]['transfer_interface_message']
        context.user_data[self.menu_name]['decline_photo'] = None
        return self.States.ACTION

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        context.user_data[self.menu_name]['transfer_interface_message'] = context.user_data['interfaces']['transfer_confirmation'].message

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_confirm')]]

        formed_basket_id = int(update.callback_query.data.replace("transfer_details_", ""))
        formed_basket = DBSession.query(FormedBasket).get(formed_basket_id)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")
        send_or_edit(context, 'transfer_confirmation', chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.FINISH

    def to_take_basket(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def get_handler(self):
        basket_products_menu = TakeBasketProductsMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry_user, pattern='^user_take_basket$'),
                                                    CallbackQueryHandler(self.entry_operator, pattern='^operator_take_basket$'),
                                                    CallbackQueryHandler(self.entry_courier, pattern='^courier_take_basket$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                               CallbackQueryHandler(self.to_take_basket, pattern=f"^to_take_basket$"),
                                                               CallbackQueryHandler(self.confirm_transfer, pattern=r"^transfer_confirm_\d+$"),
                                                               CallbackQueryHandler(self.decline_transfer, pattern=r"^transfer_decline_\d+$"),
                                                               CallbackQueryHandler(self.details, pattern=r"^transfer_details_\d+$"),
                                                               basket_products_menu.handler,
                                                               ],
                                          self.States.FINISH: [MessageHandler(Filters.text, self.decline_confirm),
                                                               CallbackQueryHandler(self.back_to_confirm, pattern="^back_to_confirm$"),
                                                               CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                               CallbackQueryHandler(self.to_take_basket, pattern=f"^to_take_basket$"),
                                                               CallbackQueryHandler(self.confirm_basket_decline, pattern="^confirm_basket_decline$"),
                                                               CallbackQueryHandler(self.decline_details, pattern="^to_decline_confirm$"),
                                                               CallbackQueryHandler(self.ask_decline_photo, pattern="^add_a_photo$"),
                                                               MessageHandler(Filters.photo, self.set_decline_photo)]
                                      },
                                      fallbacks={MessageHandler(Filters.all, lambda update, context: delete_user_message(update))},
                                      allow_reentry=True)

        return handler


class TakeBasketProductsMenu(BasketProductsMenu):
    interface = 'transfer_confirmation'

    def entry(self, update, context):
        context.user_data[self.parent.menu_name]['transfer_interface_message'] = context.user_data['interfaces']['transfer_confirmation'].message
        self._load(context)
        context.user_data[self.menu_name]['basket_id'] = int(update.callback_query.data.replace("transfer_more_", ""))

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def back(self, update, context):
        user = context.user_data['user']

        context.user_data[self.menu_name]['basket_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        transfer_interface_message = context.user_data[self.parent.menu_name]['transfer_interface_message']
        send_or_edit(context, 'transfer_confirmation', chat_id=user.chat_id, text=transfer_interface_message.text, reply_markup=transfer_interface_message.reply_markup)
        return ConversationHandler.END

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern=r"^transfer_more_\d+$")]
