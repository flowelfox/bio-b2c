import enum
import re
from io import BytesIO

import pytz
from PIL import Image
from botmanlib.menus import BaseMenu
from botmanlib.messages import delete_user_message, send_or_edit, delete_interface
from pyzbar.pyzbar import decode
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.menus.basket_products_info import BasketProductsMenu
from src.models import DBSession, User


class TransferMenu(BaseMenu):
    menu_name = "transfer_basket_menu"

    class States(enum.Enum):
        ACTION = 1
        WAIT_CONFIRM = 2

    def entry_operator(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['from'] = "operator"
        context.user_data[self.menu_name]['sure'] = False

        self.send_message(context)
        return self.States.ACTION

    def entry_courier(self, update, context):
        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        context.user_data[self.menu_name]['from'] = "courier"
        context.user_data[self.menu_name]['sure'] = False

        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = user.translator

        delete_interface(context)
        if context.user_data[self.menu_name]['from'] == "operator":
            message_text = "<b>" + _("⏏️ Give out a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("📸 Take a photo of a QR code of the customer or courier and send it to me")
        elif context.user_data[self.menu_name]['from'] == "courier":
            message_text = "<b>" + _("⏏️ Give out a Cart") + "</b>\n"
            message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
            message_text += _("📸 Take a photo of a QR code of the customer or courier and send it to me")
        else:
            message_text = _("Unknown transfer source")

        buttons = [[InlineKeyboardButton(_("⚠️ Where is a QR?"), callback_data=f"where_qr")],
                   [InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
        send_or_edit(context, "transfer_basket", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def send_transfer_message(self, context):
        user = context.user_data['user']
        _ = user.translator
        formed_basket = self.parent.selected_object(context)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)
        transfer_user = context.user_data[self.menu_name]['transfer_user']

        message_text = "<b>" + _("⏏️ Give out a Cart") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
        message_text += "<b>" + _("ID:") + f" {formed_basket.id}</b>\n"
        adapted_date = pytz.utc.localize(formed_basket.delivery_date).astimezone(pytz.timezone("Europe/Kiev")).replace(tzinfo=None)
        message_text += _("🗓️ Date of issue:") + " " + adapted_date.strftime("%d.%m.%Y") + "\n\n"

        message_text += "<b>" + _("👤 Customer:") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        message_text += _("ID:") + f" {transfer_user.chat_id}\n"
        message_text += _("Name:") + f" <a href=\"{transfer_user.mention_url}\">{transfer_user.get_name()}</a>\n"
        message_text += _("Phone:") + f" {transfer_user.phone if transfer_user.phone else _('Unknown')}\n"
        city = transfer_user.city.get_translation(user.language_code).name if transfer_user.city else _('Unknown')
        if transfer_user.city.get_translation(user.language_code).name == _("💬 Another City"):
            city += f" ({transfer_user.other_city})"
        message_text += _("City:") + f" {city}\n\n"
        if transfer_user.courier is None and transfer_user.id != formed_basket.basket_order.user.id:
            message_text += _("⚠️ WARNING You transferring basket to wrong client.") + '\n'
            message_text += _("This basket is owned by {user}").format(user=f"<a href=\"{formed_basket.basket_order.user.mention_url}\">{formed_basket.basket_order.user.get_name()}</a>\n") + "\n\n"

        if transfer_user.courier is None and transfer_user.id != formed_basket.basket_order.user.id and not context.user_data[self.menu_name]['sure']:
            message_text += _("Please wait until basket owner confirm transfer.") + "\n"
            buttons = [[InlineKeyboardButton(_("Cancel"), callback_data="cancel_transfer")]]
            # send basket owner confirmation
            self.send_owner_confirmation(context)

        else:
            message_text += "<b>" + _("⚠️ Wait for the customer or courier to confirm receipt") + "</b>"

            buttons = [[InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f"more_{formed_basket.basket_order.basket.id}")],
                       [InlineKeyboardButton(_("ℹ Cart details"), callback_data='transfer_details')],
                       [InlineKeyboardButton(_("Cancel"), callback_data="cancel_transfer")]]

        send_or_edit(context, "transfer_basket", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

    def send_owner_confirmation(self, context):
        user = context.user_data['user']
        transfer_user = context.user_data[self.menu_name]['transfer_user']
        formed_basket = self.parent.selected_object(context)
        owner_user = formed_basket.basket_order.user
        translation = formed_basket.basket_order.basket.get_translation(formed_basket.basket_order.user.language_code)

        _ = owner_user.translator
        client_message_text = "<b>" + _("🛍️ Get a Cart") + "</b>\n"
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        if context.user_data[self.menu_name]['from'] == 'operator':
            role = _("Operator")
        elif context.user_data[self.menu_name]['from'] == 'courier':
            role = _("Courier")
        else:
            role = _("Unknown")

        transfer_user_link = f"<a href=\"{transfer_user.mention_url}\">{transfer_user.get_name()}</a>"
        client_message_text += _("{role} {name} wants to give your cart to {user}").format(role=role, name=user.get_name(), user=transfer_user_link) + "\n\n"
        client_message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
        client_message_text += "<b>" + _("ID:") + f" {formed_basket.id}</b>\n"
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        client_message_text += _("Please confirm transfer of your cart to another customer") + "\n\n"

        client_buttons = [[InlineKeyboardButton(_("✅ Confirm"), callback_data=f'owner_transfer_confirm_{formed_basket.id}_tu{transfer_user.id}'),
                           InlineKeyboardButton(_("🚫 Cancel"), callback_data=f'owner_transfer_decline_{formed_basket.id}_tu{transfer_user.id}')]]

        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=owner_user.chat_id,
                     chat_id=owner_user.chat_id,
                     interface_name='owner_transfer_confirmation',
                     text=client_message_text,
                     reply_markup=InlineKeyboardMarkup(client_buttons),
                     parse_mode="HTML")

    def send_client_message(self, context):
        user = context.user_data['user']
        transfer_user = context.user_data[self.menu_name]['transfer_user']
        formed_basket = self.parent.selected_object(context)
        translation = formed_basket.basket_order.basket.get_translation(formed_basket.basket_order.user.language_code)

        _ = transfer_user.translator
        client_message_text = "<b>" + _("🛍️ Get a Cart") + "</b>\n"
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"

        if context.user_data[self.menu_name]['from'] == 'operator':
            role = _("Operator")
        elif context.user_data[self.menu_name]['from'] == 'courier':
            role = _("Courier")
        else:
            role = _("Unknown")

        client_message_text += _("{role} {name} wants to give you a cart").format(role=role, name=user.get_name()) + "\n\n"
        client_message_text += "<b>" + _("Cart Name:") + f" {(translation.name if translation.name else _('Unknown'))}</b>\n"
        client_message_text += "<b>" + _("ID:") + f" {formed_basket.id}</b>\n"
        client_message_text += "〰〰〰〰〰〰〰〰〰〰\n"
        client_message_text += _("To check the contents, you can <b>👀 Look into the cart</b>") + "\n\n"
        client_message_text += _("⚠️ Carefully check the contents and compliance with your order")

        client_buttons = [[InlineKeyboardButton(_("👀 Look into the cart"), callback_data=f'transfer_more_{formed_basket.basket_order.basket.id}')],
                          [InlineKeyboardButton(_("ℹ Cart details"), callback_data=f'transfer_details_{formed_basket.id}')],
                          [InlineKeyboardButton(_("✅ Get"), callback_data=f'transfer_confirm_{formed_basket.id}'),
                           InlineKeyboardButton(_("🚫 Cancel"), callback_data=f'transfer_decline_{formed_basket.id}')]]

        send_or_edit(context,
                     dispatcher=self.dispatcher,
                     user_id=transfer_user.chat_id,
                     chat_id=transfer_user.chat_id,
                     interface_name='transfer_confirmation',
                     text=client_message_text,
                     reply_markup=InlineKeyboardMarkup(client_buttons),
                     parse_mode="HTML")

    def confirm_as_owner_transfer(self, update, context):
        context.user_data[self.menu_name]['sure'] = True
        self.send_transfer_message(context)
        self.send_client_message(context)
        return self.States.WAIT_CONFIRM

    def where_qr(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        message_text = "<b>" + _("⚠️ Where is a QR?") + "</b>\n"
        message_text += "〰〰〰〰〰〰〰〰〰〰\n\n"
        message_text += "<b>" + _("🚚 Customer QR code:") + "</b>\n"
        message_text += _("1. The client should open the @OrganicisBot bot (if necessary, restart it with the /start command)\n"
                          "2. Go to <b>🛒 Organic Cart</b>\n"
                          "3. Select <b>🛍️ Get Cart</b>") + "\n\n"
        message_text += "<b>" + _("🚚 QR code of the courier:") + "</b>\n"
        message_text += _("1. The courier must log in to the bot by sending the /courier command\n"
                          "2. Go to <b>🔄 Get a Cart</b>")

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_to_menu")]]
        send_or_edit(context, "transfer_basket", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def back(self, update, context):
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        if update.callback_query:
            update.callback_query.answer()

        delete_interface(context, "transfer_basket")
        self.parent.send_message(context)
        return ConversationHandler.END

    def scan_qr_code(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        formed_basket = self.parent.selected_object(context)

        with BytesIO() as file:
            photo = update.effective_message.photo[-1]
            photo.get_file().download(out=file)
            file.seek(0)
            decoded_data = decode(Image.open(file))
            delete_user_message(update)
            if decoded_data:
                m = re.match("(\w\w)(380\d+)00000(\d+)", decoded_data[0].data.decode('utf-8'))
                lang, phone, user_id = m.groups()

                transfer_user = DBSession.query(User).get(user_id)
                context.user_data[self.menu_name]['transfer_user'] = transfer_user

                if transfer_user and transfer_user.waiting_for_transfer:
                    self.send_transfer_message(context)
                    if transfer_user.courier or transfer_user.id == formed_basket.basket_order.user.id or context.user_data[self.menu_name]['sure']:
                        # send message to user
                        self.send_client_message(context)

                    return self.States.WAIT_CONFIRM
                else:
                    message_text = _("User is not waiting for basket right now. Ask him to enter \"Take basket\" menu.")
                    buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
                    send_or_edit(context, "transfer_basket", chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
                    return self.States.ACTION
            else:
                _ = context.user_data['user'].translator
                message_text = _("Can't recognize QR code.")
                message_text += _("Please send photo with QR code")

                buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data=f"back_{self.menu_name}")]]
                send_or_edit(context, "transfer_basket", chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
                return self.States.ACTION

    def details(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        buttons = [[InlineKeyboardButton(_("⏪ Back"), callback_data='back_to_transfer')]]
        formed_basket = self.parent.selected_object(context)
        translation = formed_basket.basket_order.basket.get_translation(user.language_code)

        message_text = "<b>" + translation.name + "</b>\n"
        message_text += '〰〰〰〰〰〰〰〰〰〰\n'
        message_text += (translation.short_description if translation.short_description else "")
        message_text += "\n\n"
        message_text += (translation.description if translation.description else "")
        send_or_edit(context, 'transfer_basket', chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.WAIT_CONFIRM

    def back_to_transfer(self, update, context):
        self.send_transfer_message(context)
        return self.States.WAIT_CONFIRM

    def cancel_transfer(self, update, context):
        if 'transfer_user' in context.user_data[self.menu_name] and context.user_data[self.menu_name]['transfer_user']:
            transfer_user = context.user_data[self.menu_name]['transfer_user']
            formed_basket = self.parent.selected_object(context)
            if transfer_user.courier is None and transfer_user.id != formed_basket.basket_order.user.id and not context.user_data[self.menu_name]['sure']:
                delete_interface(context, 'owner_transfer_confirmation', formed_basket.basket_order.user.chat_id, dispatcher=self.dispatcher)

            delete_interface(context, 'transfer_confirmation', transfer_user.chat_id, dispatcher=self.dispatcher)
            self.fake_callback_update(transfer_user, "to_take_basket")

        self.send_message(context)
        return self.States.ACTION

    def get_handler(self):
        basket_products_menu = TransferBasketProductsMenu(self)
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry_operator, pattern='^operator_transfer_basket$'),
                                                    CallbackQueryHandler(self.entry_courier, pattern='^courier_transfer_basket$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                                               CallbackQueryHandler(self.back_to_menu, pattern=f"^back_to_menu$"),
                                                               CallbackQueryHandler(self.where_qr, pattern=f"^where_qr$"),
                                                               MessageHandler(Filters.photo, self.scan_qr_code)],
                                          self.States.WAIT_CONFIRM: [CallbackQueryHandler(self.cancel_transfer, pattern="^cancel_transfer$"),
                                                                     CallbackQueryHandler(self.confirm_as_owner_transfer, pattern="^confirm_as_owner_transfer$"),
                                                                     CallbackQueryHandler(self.details, pattern=f"^transfer_details$"),
                                                                     CallbackQueryHandler(self.back_to_transfer, pattern="^back_to_transfer$"),
                                                                     CallbackQueryHandler(self.back, pattern="^to_baskets$"),
                                                                     basket_products_menu.handler]
                                      },
                                      fallbacks=[MessageHandler(Filters.all, lambda update, context: delete_user_message(update))],
                                      allow_reentry=True)

        return handler


class TransferBasketProductsMenu(BasketProductsMenu):
    interface = 'transfer_basket'

    def back(self, update, context):
        context.user_data[self.menu_name]['basket_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        self.parent.send_transfer_message(context)
        return ConversationHandler.END
