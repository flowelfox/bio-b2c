import enum
from abc import ABC

from botmanlib.menus.helpers import group_buttons, add_to_db
from botmanlib.messages import remove_interface, send_or_edit, delete_interface, remove_interface_markup
from botmanlib.validators import PhoneNumber
from formencode import validators, Invalid
from telegram import InlineKeyboardButton, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup

from src.models import DBSession, City


class WelcomeMenuMixin(ABC):
    class States(enum.Enum):
        CITY = 1
        ASK_PHONE = 2

    def ask_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        remove_interface(context)
        message_text = _("Hello, you here first time, you must register to proceed. This will take a few seconds.\n\n")
        message_text += _("Enter your city name: ")

        send_or_edit(context, chat_id=user.chat_id, text=message_text)

        return self.States.CITY

    def find_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        all_cities = DBSession.query(City).all()

        try:
            city_buttons = []
            val = validators.String(min=1)
            value = val.to_python(update.message.text)

            found_cities = []
            for city in all_cities:
                for city_translation in city.translations:
                    if value.lower() in city_translation.name.lower():
                        found_cities.append(city)
                        break

            if found_cities:
                message_text = _("Please select your city from provided variants:") + '\n'
                for city in found_cities[:6]:
                    city_translation = city.get_translation(user.language_code)
                    if city_translation:
                        city_buttons.append(InlineKeyboardButton(city_translation.name, callback_data=f"city_{city.id}"))
                buttons = group_buttons(city_buttons, 2)
            else:
                raise Invalid("Can't find any city", None, None)
            remove_interface(context)
            send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

            return self.States.CITY
        except Invalid:
            delete_interface(context)
            message_text = _("Can't find your city 😔") + "\n\n"
            message_text += _("Our bot available only in these cities:") + "\n"
            for idx, city in enumerate(all_cities):
                message_text += str(idx + 1) + ". " + getattr(city.get_translation(user.language_code), 'name', _("Unknown")) + "\n"
            message_text += _("Enter your city name again:")
            send_or_edit(context, chat_id=user.chat_id, text=message_text)
            return self.States.CITY

    def set_city(self, update, context):
        user = context.user_data['user']
        _ = user.translator

        user.city_id = int(update.callback_query.data.replace("city_", ""))
        add_to_db(user)
        remove_interface_markup(context)

        if user.phone is None:
            return self.ask_phone(update, context)
        else:
            self.send_message(context)
            return self.States.ACTION

    def ask_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        buttons = [[KeyboardButton(_("Send phone number"), request_contact=True)]]
        send_or_edit(context, 'ask_phone', chat_id=user.chat_id, text=_("Please send you phone number"),
                     reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True))
        return self.States.ASK_PHONE

    def set_phone_contact(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        phone = update.message.contact.phone_number
        user.phone = phone
        add_to_db(user)
        send_or_edit(context, chat_id=user.chat_id, text=_("Your phone number is saved"), reply_markup=ReplyKeyboardRemove())
        remove_interface(context)
        return self.entry(update, context)

    def set_phone(self, update, context):
        user = context.user_data['user']
        _ = user.translator
        text = update.effective_message.text
        try:
            validator = PhoneNumber()
            phone = validator.to_python(text)
            user.phone = phone
            add_to_db(user)
            send_or_edit(context, chat_id=user.chat_id, text=_("Your phone number is saved"), reply_markup=ReplyKeyboardRemove())
            remove_interface(context)
            return self.entry(update, context)
        except Invalid:
            remove_interface(context, 'phone_invalid')
            send_or_edit(context, 'phone_invalid', chat_id=user.chat_id, text=_("Please enter phone number in +XXYYYZZZZZZZ format"))
            return self.ask_phone(update, context)


    def send_message(self, context):
        raise NotImplementedError
