import datetime
import logging

from botmanlib.messages import send_or_edit
from sqlalchemy import Date
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from src.models import DeliveryCenterSession, DeliveryCenter, DBSession, FormedBasket, BasketOrder, Basket
from src.settings import SERVER_TIME_CORRECTION


def start_close_delivery_center_job(job_queue):
    stop_close_delivery_center_job(job_queue)
    delivery_centres = DBSession.query(DeliveryCenter).filter(DeliveryCenter.close_time != None).all()

    for delivery_center in delivery_centres:
        run_datetime = datetime.datetime.combine(datetime.date.today(), delivery_center.close_time) + datetime.timedelta(hours=SERVER_TIME_CORRECTION) - datetime.timedelta(hours=2)
        run_time = datetime.time(run_datetime.hour, run_datetime.minute)
        job_context = {"dc_id": delivery_center.id}
        job_queue.run_daily(close_delivery_center_job, run_time, name=f"close_delivery_center_job_{delivery_center.id}", context=job_context)
        logging.info(f"Delivery center close notification job started for {delivery_center.get_translation('en').name} at {run_time}")


def stop_close_delivery_center_job(job_queue):
    for job in job_queue.get_jobs_by_name("close_delivery_center_job"):
        job.schedule_removal()


def close_delivery_center_job(context):
    dc_id = context.job.context['dc_id']
    delivery_center = DeliveryCenterSession.query(DeliveryCenter).get(dc_id)
    formed_baskets = DeliveryCenterSession.query(FormedBasket) \
        .join(FormedBasket.basket_order) \
        .join(BasketOrder.basket) \
        .join(Basket.delivery_center) \
        .filter(DeliveryCenter.id == dc_id) \
        .filter(FormedBasket.has_delivery == False) \
        .filter(FormedBasket.delivery_date.cast(Date) == datetime.date.today()) \
        .all()

    for formed_basket in formed_baskets:
        user = formed_basket.basket_order.user
        _ = user.translator
        dc_translation = delivery_center.get_translation(user.language_code)
        message_text = ""
        message_text += _("We will close in 2 hours.") + "\n"
        message_text += _("Please take your basket at  our center") + "\n"
        message_text += _("Address:") + f" {dc_translation.address}\n"
        message_text += _("Work time:") + f" {delivery_center.open_time.strftime('%H:%M')}-{delivery_center.close_time.strftime('%H:%M')}\n"

        buttons = [[InlineKeyboardButton(_("Order delivery"), callback_data=f"close_order_delivery_{formed_basket.id}")],
                   [InlineKeyboardButton(_("Close"), callback_data="close_message")]]
        send_or_edit(context, f'close_center_notification_{formed_basket.id}', user_id=user.chat_id, dispatcher=context.job_queue._dispatcher,
                     chat_id=formed_basket.basket_order.user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
