
def strtokeycap(string):
    result_string = ""
    for ch in string:
        if ch == "0":
            result_string += "0️⃣"
        elif ch == "1":
            result_string += "1️⃣"
        elif ch == "2":
            result_string += "2️⃣"
        elif ch == "3":
            result_string += "3️⃣"
        elif ch == "4":
            result_string += "4️⃣"
        elif ch == "5":
            result_string += "5️⃣"
        elif ch == "6":
            result_string += "6️⃣"
        elif ch == "7":
            result_string += "7️⃣"
        elif ch == "8":
            result_string += "8️⃣"
        elif ch == "9":
            result_string += "9️⃣"
        elif ch == "#":
            result_string += "#️⃣"
        elif ch == "*":
            result_string += "*️⃣"
    return result_string
