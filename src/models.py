import datetime
import enum

from botmanlib.menus.helpers import translator
from botmanlib.models import Database, BaseUser, UserPermissionsMixin, BasePermission, BaseUserSession, ModelPermissionsBase, UserSessionsMixin
from sqlalchemy import Column, String, UniqueConstraint, ForeignKey, Integer, Float, Boolean, DateTime, Time
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, object_session

database = Database()
Base = database.Base


class ProductBasket(Base):
    __tablename__ = 'product_basket_asoc'

    product_id = Column(Integer, ForeignKey('products.id'), primary_key=True)
    product = relationship("Product")
    basket_id = Column(Integer, ForeignKey('baskets.id'), primary_key=True)
    basket = relationship("Basket")

    editable = Column(Boolean, default=False)


class ProductBasketOrder(Base):
    __tablename__ = 'product_basket_order_asoc'

    product_id = Column(Integer, ForeignKey('products.id'), primary_key=True)
    product = relationship("Product")
    basket_order_id = Column(Integer, ForeignKey('basket_orders.id'), primary_key=True)
    basket_order = relationship("BasketOrder")

    quantity = Column(Integer, default=1)


class ProductFormedBasket(Base):
    __tablename__ = 'product_formed_basket_asoc'

    product_id = Column(Integer, ForeignKey('products.id'), primary_key=True)
    product = relationship("Product")
    formed_basket_id = Column(Integer, ForeignKey('formed_baskets.id'), primary_key=True)
    formed_basket = relationship("FormedBasket")

    quantity = Column(Integer, default=1)


class ProductsOrder(Base):
    __tablename__ = 'products_order_asoc'

    product_id = Column(Integer, ForeignKey('products.id'), primary_key=True)
    product = relationship("Product")
    product_order_id = Column(Integer, ForeignKey('product_orders.id'), primary_key=True)
    product_order = relationship("ProductOrder")

    quantity = Column(Integer, default=1)


class User(Base, BaseUser, UserPermissionsMixin, UserSessionsMixin):
    __tablename__ = 'users'

    desired_name = Column(String)
    phone = Column(String)
    email = Column(String)
    other_city = Column(String)
    waiting_for_transfer = Column(Boolean, default=False)
    city_id = Column(Integer, ForeignKey('cities.id'))
    city = relationship('City', back_populates='users')
    basket_orders = relationship('BasketOrder', back_populates='user')
    operator = relationship('Operator', back_populates='user', uselist=False)
    courier = relationship('Courier', back_populates='user', uselist=False)
    formed_baskets = relationship("FormedBasket", back_populates='holder')
    product_orders = relationship("ProductOrder", back_populates='user')

    def init_permissions(self):
        session = object_session(self)
        if session is None:
            session = database.DBSession
        for permission in ['start_menu_access',
                           'profile_menu_access',
                           'change_language_menu_access',
                           'product_menu_access',
                           'scan_qr_menu_access',
                           'help_menu_access',
                           'user_baskets_menu_access',
                           'user_take_basket_menu_access',
                           'user_my_baskets_menu_access',
                           'user_buy_basket_menu_access',
                           'basket_order_menu_access',
                           'allow_buy_basket',
                           'allow_user_take_basket']:
            perm = session.query(Permission).get(permission)
            if perm not in self.permissions:
                self.permissions.append(perm)

    @property
    def registered(self):
        return self.desired_name and self.phone and self.city

    def get_name(self):
        if self.desired_name:
            return self.desired_name
        elif self.name:
            return self.name
        elif self.username:
            return self.username
        else:
            return f"User {self.chat_id}"


class Operator(Base):
    __tablename__ = 'operators'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship('User', back_populates='operator')
    join_date = Column(DateTime, default=datetime.datetime.utcnow)
    delivery_center_id = Column(Integer, ForeignKey('delivery_centers.id'), nullable=True)
    delivery_center = relationship("DeliveryCenter", back_populates='operators')


class Courier(Base):
    __tablename__ = 'couriers'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship('User', back_populates='courier')
    join_date = Column(DateTime, default=datetime.datetime.utcnow)

    delivery_center_id = Column(Integer, ForeignKey('delivery_centers.id'), nullable=True)
    delivery_center = relationship("DeliveryCenter", back_populates='couriers')


class Permission(BasePermission, Base):
    __tablename__ = 'permissions'


class UserSession(BaseUserSession, Base):
    __tablename__ = 'user_sessions'


class Category(Base, ModelPermissionsBase):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False)
    translations = relationship("CategoryTranslation", back_populates='category', cascade='all,delete')
    products = relationship("Product", back_populates='category', cascade='all, delete')

    delivery_center_id = Column(Integer, ForeignKey('delivery_centers.id'), nullable=False)
    delivery_center = relationship("DeliveryCenter", back_populates='categories')

    parent_category_id = Column(Integer, ForeignKey('categories.id'))
    parent_category = relationship("Category", back_populates='categories', remote_side=id, uselist=False)

    categories = relationship("Category", back_populates='parent_category', remote_side=parent_category_id, uselist=True)

    def get_translation(self, lang):
        trans = DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == self.id).filter(CategoryTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == self.id).first()
        return trans


class CategoryTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'category_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship('Category', back_populates='translations')
    name = Column(String, nullable=False)
    __table_args__ = (UniqueConstraint('category_id', 'lang', name='_category_lang_uc'),)


class Product(Base, ModelPermissionsBase):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    translations = relationship("ProductTranslation", back_populates='product', cascade='all,delete')
    price = Column(Integer)
    packaging = Column(String)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship("Category", back_populates="products")
    manufacturer_id = Column(Integer, ForeignKey('manufacturers.id'), nullable=True)
    manufacturer = relationship('Manufacturer', back_populates='products')

    baskets = relationship("Basket", secondary=ProductBasket.__table__, back_populates="products", lazy='joined')
    basket_orders = relationship("BasketOrder", secondary=ProductBasketOrder.__table__, back_populates="products")
    formed_baskets = relationship("FormedBasket", secondary=ProductFormedBasket.__table__, back_populates="products")
    products_orders = relationship("ProductOrder", secondary=ProductsOrder.__table__, back_populates="products")

    def get_translation(self, lang):
        trans = DBSession.query(ProductTranslation).filter(ProductTranslation.product_id == self.id).filter(ProductTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(ProductTranslation).filter(ProductTranslation.product_id == self.id).first()
        return trans


class ProductTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'product_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    product_id = Column(Integer, ForeignKey('products.id'), nullable=False)
    product = relationship("Product", back_populates="translations")
    name = Column(String, nullable=False)
    packaging_unit = Column(String)
    description = Column(String)
    short_description = Column(String)
    image = Column(String)

    __table_args__ = (UniqueConstraint('product_id', 'lang', name='_product_lang_uc'),)


class Manufacturer(Base, ModelPermissionsBase):
    __tablename__ = 'manufacturers'

    id = Column(Integer, primary_key=True)
    translations = relationship("ManufacturerTranslation", back_populates='manufacturer', cascade='all,delete')
    products = relationship("Product", back_populates='manufacturer')
    site = Column(String, nullable=True)
    tm = Column(String, nullable=True)

    def get_translation(self, lang):
        trans = DBSession.query(ManufacturerTranslation).filter(ManufacturerTranslation.manufacturer_id == self.id).filter(ManufacturerTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(ManufacturerTranslation).filter(ManufacturerTranslation.manufacturer_id == self.id).first()
        return trans


class ManufacturerTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'manufacturer_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    manufacturer_id = Column(Integer, ForeignKey('manufacturers.id'), nullable=False)
    manufacturer = relationship('Manufacturer', back_populates='translations')
    name = Column(String, nullable=False)

    __table_args__ = (UniqueConstraint('manufacturer_id', 'lang', name='_manufacturer_lang_uc'),)


class City(Base, ModelPermissionsBase):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True)
    translations = relationship("CityTranslation", back_populates='city', cascade='all,delete')
    delivery_centers = relationship("DeliveryCenter", back_populates='city', cascade='all,delete')
    users = relationship("User", back_populates='city')

    def get_translation(self, lang):
        trans = DBSession.query(CityTranslation).filter(CityTranslation.city_id == self.id).filter(CityTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(CityTranslation).filter(CityTranslation.city_id == self.id).first()
        return trans


class CityTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'city_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    city_id = Column(Integer, ForeignKey('cities.id'), nullable=False)
    city = relationship('City', back_populates='translations')
    name = Column(String, nullable=False)

    __table_args__ = (UniqueConstraint('city_id', 'lang', name='_city_lang_uc'),)


class DeliveryCenter(Base, ModelPermissionsBase):
    __tablename__ = 'delivery_centers'

    id = Column(Integer, primary_key=True)
    open_time = Column(Time)
    close_time = Column(Time)

    city_id = Column(Integer, ForeignKey('cities.id'), nullable=False)
    city = relationship('City', back_populates='delivery_centers')

    translations = relationship("DeliveryCenterTranslation", back_populates='delivery_center', cascade='all,delete')
    categories = relationship("Category", back_populates='delivery_center', cascade='all,delete')
    baskets = relationship("Basket", back_populates='delivery_center', cascade='all,delete')
    operators = relationship("Operator", back_populates='delivery_center')
    couriers = relationship("Courier", back_populates='delivery_center')

    def get_translation(self, lang):
        trans = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.delivery_center_id == self.id).filter(DeliveryCenterTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(DeliveryCenterTranslation).filter(DeliveryCenterTranslation.delivery_center_id == self.id).first()
        return trans


class DeliveryCenterTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'delivery_centers_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    name = Column(String, nullable=False)
    address = Column(String)
    delivery_center_id = Column(Integer, ForeignKey('delivery_centers.id'), nullable=False)
    delivery_center = relationship("DeliveryCenter", back_populates="translations")


class BasketOrder(Base):
    __tablename__ = 'basket_orders'

    id = Column(Integer, primary_key=True)
    payment_id = Column(String)
    deliveries = Column(Integer, nullable=False, default=0)
    number_of_baskets = Column(Integer, default=1, nullable=False)
    payed = Column(Boolean, default=False)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)

    basket_id = Column(Integer, ForeignKey('baskets.id'), nullable=False)
    basket = relationship("Basket")

    products = relationship("Product", secondary=ProductBasketOrder.__table__, lazy='joined')

    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='basket_orders')

    formed_baskets = relationship("FormedBasket", back_populates='basket_order')

    def get_price(self, weeks=None):
        session = object_session(self)
        price = self.basket.price
        for product in self.products:
            product_basket_order_asoc = session.query(ProductBasketOrder).get((product.id, self.id))
            if product_basket_order_asoc.quantity > 1:
                price += product.price if product.price else 0

        price *= self.number_of_baskets
        if weeks == 4:
            price *= 4
            price -= price * 0.05
        if weeks == 12:
            price *= 12
            price -= price * 0.1
        if weeks == 25:
            price *= 25
            price -= price * 0.15

        return price

    def delivery_dates(self):
        start_date = self.create_date + datetime.timedelta(days=7)
        while start_date.weekday() != 3:  # TODO: make week day dynamic and depend in delivery center
            start_date += datetime.timedelta(days=1)

        delivery_dates = [start_date]
        for i in range(1, self.deliveries):
            delivery_dates.append(start_date + datetime.timedelta(days=7 * i))  # if delivery day every one week

        return delivery_dates

    def active(self):
        if not self.payed:
            return False

        delivery_dates = self.delivery_dates()
        return delivery_dates[-1] > datetime.datetime.utcnow()

    def generate_formed_baskets(self, session=None):
        if session is None:
            session = object_session(self)

        if self.formed_baskets:
            return None

        operator = session.query(Operator).filter(Operator.delivery_center_id == self.basket.delivery_center_id).order_by(Operator.join_date.desc()).first()

        delivery_dates = self.delivery_dates()
        for delivery_date in delivery_dates:
            formed_basket = FormedBasket(status=FormedBasketStatus.new, delivery_date=delivery_date, basket_order_id=self.id, holder_id=operator.user_id)
            for product in self.products:
                product_basket_order_asoc = session.query(ProductBasketOrder).get((product.id, self.id))
                product_formed_basket_asoc = ProductFormedBasket(product_id=product.id, formed_basket=formed_basket, quantity=product_basket_order_asoc.quantity)
                session.add(product_formed_basket_asoc)
            formed_basket_history_entry = FormedBasketHistory(action=FormedBasketHistoryAction.create, previous_holder_id=None, new_holder_id=operator.user_id, formed_basket=formed_basket)
            session.add(formed_basket)
            session.add(formed_basket_history_entry)
        session.commit()


class Basket(Base, ModelPermissionsBase):
    __tablename__ = 'baskets'

    id = Column(Integer, primary_key=True)
    price = Column(Float, nullable=False)
    products = relationship("Product", secondary=ProductBasket.__table__, lazy='joined')

    delivery_center_id = Column(Integer, ForeignKey('delivery_centers.id'), nullable=False)
    delivery_center = relationship("DeliveryCenter", back_populates='baskets')

    translations = relationship("BasketTranslation", back_populates='basket', cascade='all,delete')

    def get_translation(self, lang):
        trans = DBSession.query(BasketTranslation).filter(BasketTranslation.basket_id == self.id).filter(BasketTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(BasketTranslation).filter(BasketTranslation.basket_id == self.id).first()
        return trans


class BasketTranslation(Base, ModelPermissionsBase):
    __tablename__ = 'baskets_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    basket_id = Column(Integer, ForeignKey('baskets.id'), nullable=False)
    basket = relationship('Basket', back_populates='translations')
    name = Column(String, nullable=False)
    short_description = Column(String, nullable=False)
    description = Column(String, nullable=False)
    image = Column(String, nullable=True)

    __table_args__ = (UniqueConstraint('basket_id', 'lang', name='_basket_lang_uc'),)


class FormedBasketStatus(enum.Enum):
    new = "new"
    filled = "filled"
    taken = "taken"


class FormedBasket(Base):
    __tablename__ = 'formed_baskets'

    id = Column(Integer, primary_key=True)
    status = Column(ENUM(FormedBasketStatus), default=FormedBasketStatus.new)
    delivery_date = Column(DateTime, nullable=False)
    address = Column(String, nullable=True)
    delivery_start_time = Column(Time, nullable=True)
    delivery_end_time = Column(Time, nullable=True)
    delivery_price = Column(Float, nullable=True)
    delivery_failed = Column(Boolean, nullable=False, default=False)

    holder_id = Column(Integer, ForeignKey('users.id'), nullable=True)  # if None then in is not exist in real world
    holder = relationship("User", back_populates='formed_baskets')

    basket_order_id = Column(Integer, ForeignKey('basket_orders.id'), nullable=False)
    basket_order = relationship("BasketOrder", back_populates='formed_baskets')

    products = relationship("Product", secondary=ProductFormedBasket.__table__, lazy='joined')

    history = relationship("FormedBasketHistory", back_populates='formed_basket')

    def status_str(self, language_code):
        _ = translator(language_code)
        if self.status is FormedBasketStatus.new:
            return _("New")
        elif self.status is FormedBasketStatus.filled:
            return _("Filled")
        elif self.status is FormedBasketStatus.taken:
            return _("Taken")
        else:
            return _("Unknown")

    def pass_to(self, new_holder, session=None):
        if session is None:
            session = object_session(self)
        formed_basket_history_entry = FormedBasketHistory(action=FormedBasketHistoryAction.passing, previous_holder_id=self.holder_id, new_holder_id=new_holder.id, formed_basket=self)
        session.add(formed_basket_history_entry)
        self.holder_id = new_holder.id
        if self.basket_order.user.id == new_holder.id:
            self.status = FormedBasketStatus.taken
        session.add(self)
        session.commit()

    def decline_pass(self, comment, photo,  session=None):
        if session is None:
            session = object_session(self)

        formed_basket_history_entry = FormedBasketHistory(action=FormedBasketHistoryAction.passing, previous_holder_id=self.holder_id, new_holder_id=self.holder_id, formed_basket=self, comment=comment)
        session.add(formed_basket_history_entry)
        session.commit()

    def fill(self, session=None):
        if session is None:
            session = object_session(self)

        self.status = FormedBasketStatus.filled
        formed_basket_history_entry = FormedBasketHistory(action=FormedBasketHistoryAction.fill, previous_holder_id=self.holder_id, new_holder_id=self.holder_id, formed_basket=self)
        session.add(formed_basket_history_entry)
        session.add(self)
        session.commit()

    @hybrid_property
    def has_delivery(self):
        return self.address is not None and self.delivery_start_time is not None and self.delivery_end_time is not None

    @has_delivery.expression
    def has_delivery(cls):
        return ((cls.address != None) & (cls.delivery_start_time != None) & (cls.delivery_end_time != None))


class FormedBasketHistoryAction(enum.Enum):
    create = "create"
    fill = "fill"
    passing = "passing"
    decline_passing = "decline_passing"


class FormedBasketHistory(Base):
    __tablename__ = 'formed_basket_history'

    id = Column(Integer, primary_key=True)
    action = Column(ENUM(FormedBasketHistoryAction), nullable=False)
    action_date = Column(DateTime, default=datetime.datetime.utcnow)

    previous_holder_id = Column(Integer, ForeignKey('users.id'), nullable=True)
    previous_holder = relationship("User", foreign_keys=[previous_holder_id])

    new_holder_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    new_holder = relationship("User", foreign_keys=[new_holder_id])

    formed_basket_id = Column(Integer, ForeignKey('formed_baskets.id'), nullable=False)
    formed_basket = relationship("FormedBasket", back_populates='history')

    comment = Column(String, nullable=True)

    image = Column(String)


class ProductOrder(Base):
    __tablename__ = 'product_orders'

    id = Column(Integer, primary_key=True)
    payment_id = Column(String)
    payed = Column(Boolean, default=False)
    price = Column(Float, nullable=False)
    create_date = Column(DateTime, default=datetime.datetime.utcnow)
    confirmed = Column(Boolean, default=False, nullable=False)

    products = relationship("Product", secondary=ProductsOrder.__table__, lazy='joined')

    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='product_orders')


DBSession = database.create_session("DBSession")
ImportSession = database.create_session("ImportSession")
DeliveryCenterSession = database.create_session("DeliveryCenterSession")
