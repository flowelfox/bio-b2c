FROM registry.gitlab.com/botman_bots/botmanlib:1.8.1-stretch
MAINTAINER Flowelcat <flowelcat@gmail.com>

# Copying sources to image and installing bot dependencies
RUN mkdir /bot
COPY . /bot
WORKDIR /bot
RUN apt-get update
RUN apt-get install -y libzbar0
RUN apt-get install -y python-imaging
RUN pip install -r requirements.txt
RUN pip install -e .

RUN mv /bot/resources /bot/bot_resources

# Initializing botmanlib
RUN botmanlib init

# Creating command for starting container
CMD /bin/bash -c "cp -r /bot/bot_resources/* /bot/resources;botmanlib startbot"
